package com.example.designer_mobile.Utils;

import com.example.designer_mobile.Network.Client;
import com.example.designer_mobile.Network.Service;

public class UtilApi {
    public static String BASE_URL_API = "https://www.interio.id/api/";

    public static Service getApiService() {
        return Client.getClient(BASE_URL_API).create(Service.class);
    }
}
