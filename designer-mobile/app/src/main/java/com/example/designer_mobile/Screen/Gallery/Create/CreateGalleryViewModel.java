package com.example.designer_mobile.Screen.Gallery.Create;

import com.example.designer_mobile.Base.BaseErrorResponse;
import com.example.designer_mobile.Base.BaseMasterCode;
import com.example.designer_mobile.Base.BaseResponse;
import com.example.designer_mobile.Model.Gallery;
import com.example.designer_mobile.Model.MasterCode;
import com.example.designer_mobile.Network.Service;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.example.designer_mobile.Utils.ErrorUtil;
import com.example.designer_mobile.Utils.StateViewModel;
import com.example.designer_mobile.Utils.UtilApi;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateGalleryViewModel extends StateViewModel<CreateGalleryViewModel.State> {
    private static final String TAG = "CreateGalleryViewModel";

    Service mApiService = UtilApi.getApiService();

    AppState appState = AppState.getInstance();

    BaseMasterCode masterCode = new BaseMasterCode();

    @Override
    protected CreateGalleryViewModel.State initState() {
        return new CreateGalleryViewModel.State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message = "";
        Gallery gallery = new Gallery();
        String[] propertyTypeCodeList;
        String[] propertyTypeNameList;
        String[] designStyleCodeList;
        String[] designStyleNameList;
        String[] unitDurationCodeList;
        String[] unitDurationNameList;
    }

    void getPropertyTypeList() {
        state.isLoading = true;
        state.status = CreateGalleryViewModel.Status.INIT;
        updateState();

        String parent_code = "PRT";
        mApiService.getMasterCodeByParent(parent_code).enqueue(new Callback<List<MasterCode>>() {
            @Override
            public void onResponse(Call<List<MasterCode>> call, Response<List<MasterCode>> response) {
                state.isLoading = false;
                List<MasterCode> data = response.body();

                state.propertyTypeCodeList = masterCode.getCodeList(data);
                state.propertyTypeNameList = masterCode.getNameList(data);

                updateState();
            }

            @Override
            public void onFailure(Call<List<MasterCode>> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    void getDesignStyleList() {
        state.isLoading = true;
        state.status = CreateGalleryViewModel.Status.INIT;
        updateState();

        String parent_code = "DSY";
        mApiService.getMasterCodeByParent(parent_code).enqueue(new Callback<List<MasterCode>>() {
            @Override
            public void onResponse(Call<List<MasterCode>> call, Response<List<MasterCode>> response) {
                state.isLoading = false;
                List<MasterCode> data = response.body();

                state.designStyleCodeList = masterCode.getCodeList(data);
                state.designStyleNameList = masterCode.getNameList(data);
                updateState();
            }

            @Override
            public void onFailure(Call<List<MasterCode>> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    void storeGallery(Map<String, RequestBody> parameters, MultipartBody.Part[] arrayParameters) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();

        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), appState.getDesigner().getId());

        mApiService.storeGallery(
                userId,
                parameters.get("title"),
                parameters.get("property_type"),
                parameters.get("room_length"),
                parameters.get("room_width"),
                parameters.get("room_height"),
                parameters.get("design_style"),
                parameters.get("description"),
                arrayParameters
        ).enqueue(new Callback<BaseResponse<Gallery>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Gallery>> call, @NotNull Response<BaseResponse<Gallery>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = Status.SUCCESS;
                    state.message = response.body().getMessage();
                    state.gallery = response.body().getData();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Gallery>> call, Throwable t) {
                state.isLoading = false;
                state.status = Status.ERROR;
                state.message = t.getMessage();
            }
        });
    }
}
