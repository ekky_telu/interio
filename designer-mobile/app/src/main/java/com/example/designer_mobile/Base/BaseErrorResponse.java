package com.example.designer_mobile.Base;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class BaseErrorResponse {
    private String message;
    private JsonArray errors;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JsonArray getErrors() {
        return errors;
    }

    public void setErrors(JsonArray errors) {
        this.errors = errors;
    }
}
