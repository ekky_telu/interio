package com.example.designer_mobile.Screen.SettingProfile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SetPrice extends AppCompatActivity {
    private static final String TAG = "SetPriceActivity";

    /* Define ViewModel */
    private SettingProfileViewModel viewModel;

    @BindView(R.id.masukkan_harga)
    EditText mPrice;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_price);

        ButterKnife.bind(this);
        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(SettingProfileViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderResponse(state.status, state.message);
            renderPrice(state.designer);
        });

        viewModel.getProfile();
    }

    void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    void renderPrice(Designer designer) {
        mPrice.setText(designer.getPrice());
    }

    void renderResponse(SettingProfileViewModel.Status status, String message) {
        if (message != null) {
            if (status == SettingProfileViewModel.Status.SUCCESS) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

                onBackPressed();
                finish();
            } else if (status == SettingProfileViewModel.Status.ERROR) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void intentSP(View view) {
        onBackPressed();
        finish();
    }

    public void savePrice(View view) {
        if (!validate()) {
            Toast.makeText(this, "Harga Harus Diisi", Toast.LENGTH_SHORT).show();
            return;
        }

        String price = mPrice.getText().toString();
        viewModel.updatePrice(price);
    }

    boolean validate() {
        return !mPrice.getText().toString().equals("");
    }
}
