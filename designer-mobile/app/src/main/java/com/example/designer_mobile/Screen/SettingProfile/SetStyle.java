package com.example.designer_mobile.Screen.SettingProfile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.designer_mobile.Adapter.DesignStyleRegisterAdapter;
import com.example.designer_mobile.Base.BottomSheetListView;
import com.example.designer_mobile.Base.SpacesItemDecoration;
import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.R;
import com.example.designer_mobile.Screen.Login.Login;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SetStyle extends AppCompatActivity {

    private static final String TAG = "SetStyleActivity";

    /* Define ViewModel */
    private SettingProfileViewModel viewModel;

    @BindView(R.id.design_style)
    RecyclerView mDesignStyle;
    @BindView(R.id.gaya_desain)
    EditText mDesignStyleToShow;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    DesignStyleRegisterAdapter adapter;
    String mDesignStyleCodeToShow;
    String[] designStyleCodeList;
    String[] designStyleNameList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_style);

        ButterKnife.bind(this);
        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(SettingProfileViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderDesignStyle(state.status, state.designer);
            renderResponse(state.status, state.message);
        });

        viewModel.getProfile();
        viewModel.getDesignStyleList();
        mDesignStyleToShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDesignStyle(v);
            }
        });
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderDesignStyle(SettingProfileViewModel.Status status, Designer designer) {
        if (status == SettingProfileViewModel.Status.SUCCESS) {
            String[] designStyleCodeList = viewModel.getState().getValue().designStyleCodeList;
            String[] designStyleNameList = viewModel.getState().getValue().designStyleNameList;

            mDesignStyle.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            });
            Log.d(TAG, String.valueOf(designer.getDesignStyle().size()));
            adapter = new DesignStyleRegisterAdapter(designStyleCodeList, designStyleNameList, true, designer.getDesignStyle());
            mDesignStyle.setAdapter(adapter);

            SpacesItemDecoration decoration = new SpacesItemDecoration(25);
            mDesignStyle.addItemDecoration(decoration);

            if (designer.getMainDesignStyle() != null && !designer.getMainDesignStyle().isEmpty() && !designer.getMainDesignStyle().equals("null")) {
                mDesignStyleCodeToShow = designer.getMainDesignStyle();
                mDesignStyleToShow.setText(designer.getMainDesignStyleDetail().getDescription());
            }

        }
    }

    public void showDesignStyle(View view) {
        String title = "Gaya Desain";
        String[] codeList = adapter.getDesignStyleCodeToShow().toArray(new String[0]);
        String[] nameList = adapter.getDesignStyleNameToShow().toArray(new String[0]);

        if (codeList.length == 0) {
            Toast.makeText(this, "Silakan Pilih Desain di Bawah Terlebih Dahulu", Toast.LENGTH_SHORT).show();
            return;
        }

        mDesignStyleToShow.setText("");
        mDesignStyleCodeToShow = "";

        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(R.layout.bottom_sheet_view);

        BottomSheetListView listViewName = dialog.findViewById(R.id.list_item);
        TextView titleView = dialog.findViewById(R.id.list_title);

        titleView.setText(title);

        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.item_bottom_sheet, nameList);
        listViewName.setAdapter(adapter);

        listViewName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDesignStyleCodeToShow = codeList[position];
                mDesignStyleToShow.setText(parent.getItemAtPosition(position).toString());

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    void renderResponse(SettingProfileViewModel.Status status, String message) {
        if (message != null) {
            if (status == SettingProfileViewModel.Status.SUCCESS) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

                onBackPressed();
                finish();
            } else if (status == SettingProfileViewModel.Status.ERROR) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void intentSP(View view) {
        onBackPressed();
        finish();
    }

    public void saveDesignStyle(View view) {
        if (validate()) {
            String[] designStyles = new String[adapter.getItems().size()];
            adapter.getItems().toArray(designStyles);

            viewModel.updateDesignStyle(mDesignStyleCodeToShow, designStyles);
        }
    }

    boolean validate() {
        if (adapter.getItems().size() == 0) {
            Toast.makeText(this, "Pilih Gaya Desain yang Sesuai Denganmu", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (mDesignStyleToShow.getText().toString().equals("")) {
            Toast.makeText(this, "Pilih Gaya Desain yang Ingin Kamu Tampilkan di Profil", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
}
