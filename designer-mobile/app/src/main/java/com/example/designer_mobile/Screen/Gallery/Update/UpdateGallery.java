package com.example.designer_mobile.Screen.Gallery.Update;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.designer_mobile.Adapter.UpdatePortfolioPhotoAdapter;
import com.example.designer_mobile.Base.BottomSheetListView;
import com.example.designer_mobile.Model.PortfolioPhoto;
import com.example.designer_mobile.Model.Gallery;
import com.example.designer_mobile.Network.Client;
import com.example.designer_mobile.R;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UpdateGallery extends AppCompatActivity {
    private static final String TAG = "UpdateGalleryActivity";

    /* Define ViewModel */
    private UpdateGalleryViewModel viewModel;

    /* Define Code as API Parameters */
    String mPropertyTypeCode;
    String mDesignStyleCode;
    String mUnitDurationCode;

    /* Define Context */
    private Context mContext;

    @BindView(R.id.toolbar)
    Toolbar mToolBar;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.title)
    EditText mTitle;
    @BindView(R.id.description)
    EditText mDescription;
    @BindView(R.id.room_length)
    EditText mRoomLength;
    @BindView(R.id.room_width)
    EditText mRoomWidth;
    @BindView(R.id.room_height)
    EditText mRoomHeight;
    @BindView(R.id.property_type)
    EditText mPropertyType;
    @BindView(R.id.design_style)
    EditText mDesignStyle;
    @BindView(R.id.photos)
    RecyclerView mPhotos;

    String galleryId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_gallery);

        ButterKnife.bind(this);
        mContext = this;

        galleryId = getIntent().getStringExtra("gallery_id");

        setToolbar(mToolBar);
        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(UpdateGalleryViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderResponse(state.status);
            renderGallery(state.gallery);
        });

        viewModel.getGallery(galleryId);

        viewModel.getPropertyTypeList();
        viewModel.getDesignStyleList();

        mPropertyType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPropertyType(v);
            }
        });

        mDesignStyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDesignStyle(v);
            }
        });
    }

    void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    void renderResponse(UpdateGalleryViewModel.Status status) {
        if (status == UpdateGalleryViewModel.Status.SUCCESS) {
            Toast.makeText(mContext, viewModel.getState().getValue().message,
                    Toast.LENGTH_SHORT).show();

            onBackPressed();
            finish();
        } else if (status == UpdateGalleryViewModel.Status.ERROR) {
            Toast.makeText(mContext, viewModel.getState().getValue().message,
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void renderGallery(Gallery gallery) {
        if (gallery != null) {
            Log.d(TAG, gallery.getDescription());
            mTitle.setText(gallery.getTitle());
            mRoomLength.setText(gallery.getRoomLength());
            mRoomWidth.setText(gallery.getRoomWidth());
            mRoomHeight.setText(gallery.getRoomHeight());
            mDescription.setText(gallery.getDescription());
            mPropertyType.setText(gallery.getPropertyTypeDetail().getDescription());
            mDesignStyle.setText(gallery.getDesignStyleDetail().getDescription());

            mPropertyTypeCode = gallery.getPropertyType();
            mDesignStyleCode = gallery.getDesignStyle();

            showPhotosToAdapter(gallery.getPhotos());
        }
    }

    private void showPhotosToAdapter(List<PortfolioPhoto> photo) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        UpdatePortfolioPhotoAdapter adapter = new UpdatePortfolioPhotoAdapter(mContext, photo, Client.GALLERY_IMAGE_URL);

        mPhotos.setLayoutManager(layoutManager);
        mPhotos.setAdapter(adapter);
    }

    private void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_dark);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void updateGallery(View view) {
        if (!validateParameters()) {
            Toast.makeText(mContext, "Lengkapi Data", Toast.LENGTH_SHORT).show();
            return;
        }

        Map<String, String> parameters = new HashMap<>();

        parameters.put("gallery_id", galleryId);
        parameters.put("title", mTitle.getText().toString());
        parameters.put("room_length", mRoomLength.getText().toString());
        parameters.put("room_width", mRoomWidth.getText().toString());
        parameters.put("room_height", mRoomHeight.getText().toString());
        parameters.put("description", mDescription.getText().toString());

        parameters.put("property_type", mPropertyTypeCode);
        parameters.put("design_style", mDesignStyleCode);

        viewModel.updateGallery(parameters);
    }

    private boolean validateParameters() {
        return !mTitle.getText().toString().equals("") && !mRoomLength.getText().toString().equals("")
                && !mRoomWidth.getText().toString().equals("") && !mRoomHeight.getText().toString().equals("")
                && !mDescription.getText().toString().equals("") && !mPropertyType.getText().toString().equals("")
                && !mDesignStyle.getText().toString().equals("");
    }

    public void showPropertyType(View view) {
        String title = "Tipe Properti";
        String[] codeList = viewModel.getState().getValue().propertyTypeCodeList;
        String[] nameList = viewModel.getState().getValue().propertyTypeNameList;

        BottomSheetDialog dialog = new BottomSheetDialog(mContext);
        dialog.setContentView(R.layout.bottom_sheet_view);

        BottomSheetListView listViewName = dialog.findViewById(R.id.list_item);
        TextView titleView = dialog.findViewById(R.id.list_title);

        titleView.setText(title);

        ArrayAdapter adapter = new ArrayAdapter(mContext, R.layout.item_bottom_sheet, nameList);
        listViewName.setAdapter(adapter);

        listViewName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mPropertyTypeCode = codeList[position];
                mPropertyType.setText(parent.getItemAtPosition(position).toString());

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void showDesignStyle(View view) {
        String title = "Gaya Desain";
        String[] codeList = viewModel.getState().getValue().designStyleCodeList;
        String[] nameList = viewModel.getState().getValue().designStyleNameList;

        BottomSheetDialog dialog = new BottomSheetDialog(mContext);
        dialog.setContentView(R.layout.bottom_sheet_view);

        BottomSheetListView listViewName = dialog.findViewById(R.id.list_item);
        TextView titleView = dialog.findViewById(R.id.list_title);

        titleView.setText(title);

        ArrayAdapter adapter = new ArrayAdapter(mContext, R.layout.item_bottom_sheet, nameList);
        listViewName.setAdapter(adapter);

        listViewName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDesignStyleCode = codeList[position];
                mDesignStyle.setText(parent.getItemAtPosition(position).toString());

                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
