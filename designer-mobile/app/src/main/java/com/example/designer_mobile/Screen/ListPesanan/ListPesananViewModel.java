package com.example.designer_mobile.Screen.ListPesanan;

import android.util.Log;

import com.example.designer_mobile.Base.BaseErrorResponse;
import com.example.designer_mobile.Base.BaseResponse;
import com.example.designer_mobile.Model.Transaction;
import com.example.designer_mobile.Network.Service;
import com.example.designer_mobile.Screen.Gallery.Detail.DetailGalleryViewModel;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.example.designer_mobile.Utils.ErrorUtil;
import com.example.designer_mobile.Utils.StateViewModel;
import com.example.designer_mobile.Utils.UtilApi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListPesananViewModel extends StateViewModel<ListPesananViewModel.State> {
    private static final String TAG = "ListPesananViewModel";
    Service mApiService = UtilApi.getApiService();
    AppState appState = AppState.getInstance();

    @Override
    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message = "";
        List<Transaction> listPesanan = new ArrayList<>();
    }

    void getListPesanan(String designerId) {
        state.isLoading = true;
        state.status = ListPesananViewModel.Status.INIT;
        updateState();

        mApiService.listPesanan(designerId).enqueue(new Callback<BaseResponse<List<Transaction>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Transaction>>> call, Response<BaseResponse<List<Transaction>>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = ListPesananViewModel.Status.SUCCESS;
                    state.listPesanan = response.body().getData();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = ListPesananViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Transaction>>> call, Throwable t) {
                state.isLoading = false;
                state.status = ListPesananViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }
}
