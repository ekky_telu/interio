package com.example.designer_mobile.Screen.Project.Detail;

import com.example.designer_mobile.Base.BaseErrorResponse;
import com.example.designer_mobile.Base.BaseResponse;
import com.example.designer_mobile.Model.PortfolioPhoto;
import com.example.designer_mobile.Model.Project;
import com.example.designer_mobile.Network.Service;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.example.designer_mobile.Utils.ErrorUtil;
import com.example.designer_mobile.Utils.StateViewModel;
import com.example.designer_mobile.Utils.UtilApi;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailProjectViewModel extends StateViewModel<DetailProjectViewModel.State> {
    private static final String TAG = "DetailProjectViewModel";

    Service mApiService = UtilApi.getApiService();

    AppState appState = AppState.getInstance();

    @Override
    protected DetailProjectViewModel.State initState() {
        return new DetailProjectViewModel.State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        DetailProjectViewModel.Status status = DetailProjectViewModel.Status.INIT;
        String message = null;
        Project project = new Project();
    }

    void getData(String id) {
        state.isLoading = true;
        state.status = DetailProjectViewModel.Status.INIT;
        updateState();

        mApiService.getProjectById(id).enqueue(new Callback<BaseResponse<Project>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Project>> call, @NotNull Response<BaseResponse<Project>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = DetailProjectViewModel.Status.SUCCESS;
                    state.project = response.body().getData();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = DetailProjectViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<Project>> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = DetailProjectViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }
}
