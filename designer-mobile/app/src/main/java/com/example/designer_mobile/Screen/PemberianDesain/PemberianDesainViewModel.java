package com.example.designer_mobile.Screen.PemberianDesain;

import com.example.designer_mobile.Base.BaseErrorResponse;
import com.example.designer_mobile.Base.BaseResponse;
import com.example.designer_mobile.Model.Transaction;
import com.example.designer_mobile.Network.Service;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.example.designer_mobile.Utils.ErrorUtil;
import com.example.designer_mobile.Utils.StateViewModel;
import com.example.designer_mobile.Utils.UtilApi;

import org.jetbrains.annotations.NotNull;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PemberianDesainViewModel extends StateViewModel<PemberianDesainViewModel.State> {
    private static final String TAG = "PemberianDesainViewModel";
    private Service mApiService = UtilApi.getApiService();

    @Override
    protected PemberianDesainViewModel.State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    static class State {
        boolean isLoading = false;
        PemberianDesainViewModel.Status status = PemberianDesainViewModel.Status.INIT;
        String message = "";
        Transaction transaction;
    }

    void getTransaction(String transactionId) {
        state.isLoading = true;
        state.status = PemberianDesainViewModel.Status.INIT;
        updateState();

        mApiService.getTransactionById(transactionId).enqueue(new Callback<BaseResponse<Transaction>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Transaction>> call, @NotNull Response<BaseResponse<Transaction>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.transaction = response.body().getData();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = PemberianDesainViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<Transaction>> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = PemberianDesainViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void sendDesign(RequestBody transactionId, RequestBody description, MultipartBody.Part[] photos) {
        state.isLoading = true;
        state.status = PemberianDesainViewModel.Status.INIT;
        updateState();

        mApiService.sendDesign(transactionId, description, photos).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse> call, @NotNull Response<BaseResponse> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = Status.SUCCESS;
                    state.message = response.body().getMessage();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }
}
