package com.example.designer_mobile.Screen.Gallery.Detail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.designer_mobile.Adapter.PortfolioPhotoAdapter;
import com.example.designer_mobile.Model.Gallery;
import com.example.designer_mobile.Network.Client;
import com.example.designer_mobile.R;
import com.example.designer_mobile.Base.SpacesItemDecoration;
import com.example.designer_mobile.Screen.Gallery.Update.UpdateGallery;
import com.example.designer_mobile.SharedPreferences.AppState;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailGallery extends AppCompatActivity {

    private static final String TAG = "DetailGalleryActivity";

    /* Define ViewModel */
    private DetailGalleryViewModel viewModel;

    /* Define Context */
    private Context mContext;

    /* Define ID Gallery */
    private String id;

    /* Define Component */
    @BindView(R.id.name)
    TextView mName;
    @BindView(R.id.username)
    TextView mUsername;
    @BindView(R.id.designer_type)
    TextView mDesignerType;
    @BindView(R.id.design_style)
    TextView mDesignStyle;
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.description)
    TextView mDescription;
    @BindView(R.id.property_type)
    TextView mPropertyType;
    @BindView(R.id.room_size)
    TextView mRoomSize;
    @BindView(R.id.toolbar)
    Toolbar mToolBar;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.photos)
    RecyclerView mPhotos;
    @BindView(R.id.btnEdit)
    RelativeLayout mBtnEdit;

    Gallery gallery;
    SpacesItemDecoration decoration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_gallery);

        ButterKnife.bind(this);
        mContext = this;

        decoration = new SpacesItemDecoration(25);
        mPhotos.addItemDecoration(decoration);

        setToolbar(mToolBar);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        id = getIntent().getStringExtra("gallery_id");
        viewModel = new ViewModelProvider(this).get(DetailGalleryViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderData(state.gallery, state.status);
        });

        viewModel.getData(id);
    }

    void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderData(Gallery gallery, DetailGalleryViewModel.Status status) {
        if (gallery != null && status == DetailGalleryViewModel.Status.SUCCESS) {
            double roomSize = (Integer.parseInt(gallery.getRoomLength())) *
                    (Integer.parseInt(gallery.getRoomWidth())) *
                    (Integer.parseInt(gallery.getRoomHeight()));

            if (!gallery.getDesignerId().equals(AppState.getInstance().getDesigner().getId())) {
                mBtnEdit.setVisibility(View.GONE);
            }

            mName.setText(gallery.getDesigner().getName());
            mUsername.setText("@" + gallery.getDesigner().getUsername());
            mDesignerType.setText("Designer (Individual)");
            mDesignStyle.setText(gallery.getDesignStyleDetail().getDescription());
            mTitle.setText(gallery.getTitle());
            mDescription.setText(gallery.getDescription());
            mPropertyType.setText(gallery.getPropertyTypeDetail().getDescription());
            mRoomSize.setText(String.valueOf(roomSize));

            mPhotos.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            });

            PortfolioPhotoAdapter adapter = new PortfolioPhotoAdapter(this, gallery.getPhotos(), Client.GALLERY_IMAGE_URL);
            mPhotos.setAdapter(adapter);

            this.gallery = gallery;
        }
    }

    private void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void updateGallery(View view) {
        Intent update = new Intent(this, UpdateGallery.class);
        update.putExtra("gallery_id", gallery.getId());
        startActivity(update);
    }
}
