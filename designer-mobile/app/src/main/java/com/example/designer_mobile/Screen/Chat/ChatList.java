package com.example.designer_mobile.Screen.Chat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.example.designer_mobile.Adapter.ChatAdapter;
import com.example.designer_mobile.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatList extends AppCompatActivity {

    @BindView(R.id.chat_list)
    RecyclerView mChatList;

    FirebaseDatabase firebase = FirebaseDatabase.getInstance();
    DatabaseReference dbRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);

        dbRef = firebase.getReference();

        ButterKnife.bind(this);


    }
}
