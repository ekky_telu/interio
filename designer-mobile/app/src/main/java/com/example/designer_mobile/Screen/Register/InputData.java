package com.example.designer_mobile.Screen.Register;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.designer_mobile.R;
import com.example.designer_mobile.Screen.Login.Login;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InputData extends AppCompatActivity {
    String mUsername, mPassword, mPasswordConfirmation;
    private RegisterViewModel viewModel;

    @BindView(R.id.fname)
    EditText mFName;
    @BindView(R.id.lname)
    EditText mLName;
    @BindView(R.id.email)
    EditText mEmail;
    @BindView(R.id.nohp)
    EditText mPhone;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register3);
        ButterKnife.bind(this);

        mUsername = getIntent().getStringExtra("username");
        mPassword = getIntent().getStringExtra("password");
        mPasswordConfirmation = getIntent().getStringExtra("password_confirmation");

        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(RegisterViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderResponse(state.isError);
        });
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void renderResponse(boolean isError) {
        if (isError) {
            if (!viewModel.getState().getValue().message.equals("") && viewModel.getState().getValue().status == RegisterViewModel.Status.ERROR) {
                Toast.makeText(this, viewModel.getState().getValue().message, Toast.LENGTH_SHORT).show();
            }
            return;
        }

        Intent intent = new Intent(this, SetDesignStyle.class);
        intent.putExtra("username", mUsername);
        intent.putExtra("password", mPassword);
        intent.putExtra("password_confirmation", mPasswordConfirmation);
        intent.putExtra("first_name", mFName.getText().toString());
        intent.putExtra("last_name", mLName.getText().toString());
        intent.putExtra("email", mEmail.getText().toString());
        intent.putExtra("phone", mPhone.getText().toString());
        startActivity(intent);
    }

    public void showLogin(View view) {
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
        finish();
    }

    public void next(View view) {
        if (mFName.getText().toString().equals("")) {
            Toast.makeText(this, "Nama Depan Belum Diisi", Toast.LENGTH_SHORT).show();
            return;
        }

        if (mLName.getText().toString().equals("")) {
            Toast.makeText(this, "Nama Belakang Belum Diisi", Toast.LENGTH_SHORT).show();
            return;
        }

        if (mEmail.getText().toString().equals("")) {
            Toast.makeText(this, "Email Belum Diisi", Toast.LENGTH_SHORT).show();
            return;
        }

        if (mPhone.getText().toString().equals("")) {
            Toast.makeText(this, "No Telepon Belum Diisi", Toast.LENGTH_SHORT).show();
            return;
        }

        viewModel.checkEmail(mEmail.getText().toString(), "designer");
    }
}
