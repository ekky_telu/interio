package com.example.designer_mobile.Model;

import android.net.Uri;

import com.google.gson.annotations.SerializedName;

public class PortfolioPhoto {

    @SerializedName("id")
    private String id;

    @SerializedName("project_id")
    private String project_id;

    @SerializedName("title")
    private String title;

    @SerializedName("image")
    private String image;

    @SerializedName("description")
    private String description;

    @SerializedName("main_image")
    private String main_image;

    @SerializedName("image_uri")
    private Uri image_uri;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectId() {
        return project_id;
    }

    public void setProjectId(String project_id) {
        this.project_id = project_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMainImage() {
        return main_image;
    }

    public void setMainImage(String main_image) {
        this.main_image = main_image;
    }

    public Uri getImageUri() {
        return image_uri;
    }

    public void setImageUri(Uri image_uri) {
        this.image_uri = image_uri;
    }
}
