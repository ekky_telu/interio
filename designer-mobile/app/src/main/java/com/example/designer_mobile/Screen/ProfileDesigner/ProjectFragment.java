package com.example.designer_mobile.Screen.ProfileDesigner;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.designer_mobile.Adapter.ProjectAdapter;
import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class ProjectFragment extends Fragment {
    @BindView(R.id.projectList)
    RecyclerView projectList;
    @BindView(R.id.noData)
    TextView noData;

    private Designer designer;

    public ProjectFragment(Designer designer) {
        this.designer = designer;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_project, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (designer.getProjectsCount().equals("0")) {
            noData.setVisibility(View.VISIBLE);
        } else {
            ProjectAdapter adapter = new ProjectAdapter(getContext(), designer.getProjects(), designer);
            projectList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
            projectList.setAdapter(adapter);
        }
    }
}
