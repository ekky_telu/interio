package com.example.designer_mobile.Screen.Register;

import android.util.Log;

import androidx.lifecycle.ViewModel;

import com.example.designer_mobile.Base.BaseErrorResponse;
import com.example.designer_mobile.Base.BaseMasterCode;
import com.example.designer_mobile.Base.BaseResponse;
import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.Model.MasterCode;
import com.example.designer_mobile.Model.Project;
import com.example.designer_mobile.Network.Service;
import com.example.designer_mobile.Screen.Project.Create.CreateProject;
import com.example.designer_mobile.Screen.Project.Create.CreateProjectViewModel;
import com.example.designer_mobile.Screen.Project.Detail.DetailProjectViewModel;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.example.designer_mobile.Utils.ErrorUtil;
import com.example.designer_mobile.Utils.StateViewModel;
import com.example.designer_mobile.Utils.UtilApi;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterViewModel extends StateViewModel<RegisterViewModel.State> {
    private static final String TAG = "RegisterViewModel";
    Service mApiService = UtilApi.getApiService();

    private FirebaseDatabase firebase = FirebaseDatabase.getInstance();

    BaseMasterCode masterCode = new BaseMasterCode();

    @Override
    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        RegisterViewModel.Status status = RegisterViewModel.Status.INIT;
        String message = "";
        JsonArray errors;
        boolean isError = true;
        String[] designStyleCodeList;
        String[] designStyleNameList;
    }

    void register(String username, String email, String password, String password_confirmation, String first_name, String last_name, String phone, String role, String[] design_style) {
        state.isLoading = true;
        state.status = RegisterViewModel.Status.INIT;
        updateState();

        mApiService.register(username, email, password, password_confirmation, first_name, last_name, phone, role, design_style).enqueue(new Callback<BaseResponse<Designer>>() {
            @Override
            public void onResponse(Call<BaseResponse<Designer>> call, Response<BaseResponse<Designer>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    BaseResponse baseResponse = response.body();
                    state.status = RegisterViewModel.Status.SUCCESS;
                    state.message = baseResponse.getMessage();

                    Designer designer = response.body().getData();
                    DatabaseReference dbRef = firebase.getReference();
                    dbRef.child("designers").push().setValue(designer);
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = RegisterViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }
                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Designer>> call, Throwable t) {
                state.isLoading = false;
                state.status = RegisterViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void getDesignStyleList() {
        state.isLoading = true;
        state.status = RegisterViewModel.Status.INIT;
        updateState();

        String parent_code = "DSY";
        mApiService.getMasterCodeByParent(parent_code).enqueue(new Callback<List<MasterCode>>() {
            @Override
            public void onResponse(Call<List<MasterCode>> call, Response<List<MasterCode>> response) {
                state.isLoading = false;
                state.status = Status.SUCCESS;
                List<MasterCode> data = response.body();

                state.designStyleCodeList = masterCode.getCodeList(data);
                state.designStyleNameList = masterCode.getNameList(data);
                updateState();
            }

            @Override
            public void onFailure(Call<List<MasterCode>> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    void checkUsername(String username, String role) {
        state.isLoading = true;
        state.status = RegisterViewModel.Status.INIT;
        updateState();

        mApiService.checkUsername(role, username).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                state.isLoading = false;
                if (!response.isSuccessful()) {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = RegisterViewModel.Status.ERROR;
                    state.errors = errorResponse.getErrors();
                    state.isError = true;

                    JsonObject errorObj = (JsonObject) state.errors.get(0);
                    state.message = errorObj.get("message").getAsString();
                } else {
                    state.status = Status.SUCCESS;
                    state.isError = false;
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    void checkEmail(String email, String role) {
        state.isLoading = true;
        state.status = RegisterViewModel.Status.INIT;
        updateState();

        mApiService.checkEmail(role, email).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                state.isLoading = false;
                if (!response.isSuccessful()) {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = RegisterViewModel.Status.ERROR;
                    state.errors = errorResponse.getErrors();
                    state.isError = true;

                    JsonObject errorObj = (JsonObject) state.errors.get(0);
                    state.message = errorObj.get("message").getAsString();
                } else {
                    state.status = Status.SUCCESS;
                    state.isError = false;
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }
}
