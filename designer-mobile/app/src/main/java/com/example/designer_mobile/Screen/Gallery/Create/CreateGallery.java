package com.example.designer_mobile.Screen.Gallery.Create;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.example.designer_mobile.Adapter.CreatePortfolioPhotoAdapter;
import com.example.designer_mobile.Base.BottomSheetListView;
import com.example.designer_mobile.R;
import com.example.designer_mobile.Screen.Gallery.Detail.DetailGallery;
import com.example.designer_mobile.Screen.ProfileDesigner.ProfileDesigner;
import com.example.designer_mobile.Screen.Project.Detail.DetailProject;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateGallery extends AppCompatActivity {
    private static final String TAG = "CreateGalleryActivity";

    /* Define ViewModel */
    private CreateGalleryViewModel viewModel;

    /* Define Code as API Parameters */
    String mPropertyTypeCode;
    String mDesignStyleCode;

    /* Define Context */
    private Context mContext;

    List<MultipartBody.Part> photos = new ArrayList<>();
    List<Image> images;

    @BindView(R.id.toolbar)
    Toolbar mToolBar;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.title)
    EditText mTitle;
    @BindView(R.id.description)
    EditText mDescription;
    @BindView(R.id.room_length)
    EditText mRoomLength;
    @BindView(R.id.room_width)
    EditText mRoomWidth;
    @BindView(R.id.room_height)
    EditText mRoomHeight;
    @BindView(R.id.property_type)
    EditText mPropertyType;
    @BindView(R.id.design_style)
    EditText mDesignStyle;
    @BindView(R.id.photos)
    RecyclerView mPhotos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_gallery);

        ButterKnife.bind(this);
        mContext = this;

        setToolbar(mToolBar);
        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(CreateGalleryViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderResponse(state.status);
        });

        viewModel.getPropertyTypeList();
        viewModel.getDesignStyleList();

        mPropertyType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPropertyType(v);
            }
        });

        mDesignStyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDesignStyle(v);
            }
        });
    }

    void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    void renderResponse(CreateGalleryViewModel.Status status) {
        if (status == CreateGalleryViewModel.Status.SUCCESS) {
            Toast.makeText(mContext, viewModel.getState().getValue().message,
                    Toast.LENGTH_SHORT).show();

            onBackPressed();
            finish();
        } else if (status == CreateGalleryViewModel.Status.ERROR) {
            Toast.makeText(mContext, viewModel.getState().getValue().message,
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_dark);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void storeGallery(View view) {
        if (!validateParameters()) {
            Toast.makeText(mContext, "Lengkapi Data!", Toast.LENGTH_SHORT).show();
            return;
        }

        Map<String, RequestBody> parameters = new HashMap<>();

        parameters.put("title", RequestBody.create(MediaType.parse("text/plain"), mTitle.getText().toString()));
        parameters.put("room_length", RequestBody.create(MediaType.parse("integer"), mRoomLength.getText().toString()));
        parameters.put("room_width", RequestBody.create(MediaType.parse("integer"), mRoomWidth.getText().toString()));
        parameters.put("room_height", RequestBody.create(MediaType.parse("integer"), mRoomHeight.getText().toString()));
        parameters.put("description", RequestBody.create(MediaType.parse("text/plain"), mDescription.getText().toString()));

        parameters.put("property_type", RequestBody.create(MediaType.parse("text/plain"), mPropertyTypeCode));
        parameters.put("design_style", RequestBody.create(MediaType.parse("text/plain"), mDesignStyleCode));

        MultipartBody.Part[] photosParameter = new MultipartBody.Part[photos.size()];
        for (int i = 0; i < photos.size(); i++) {
            photosParameter[i] = photos.get(i);
        }

        viewModel.storeGallery(parameters, photosParameter);
    }

    private boolean validateParameters() {
        if (mTitle.getText().toString().equals("") || mRoomLength.getText().toString().equals("")
                || mRoomWidth.getText().toString().equals("") || mRoomHeight.getText().toString().equals("")
                || mDescription.getText().toString().equals("") || mPropertyType.getText().toString().equals("")
                || mDesignStyle.getText().toString().equals("") || photos.size() < 1) {
            return false;
        }
        return true;
    }

    public void showPropertyType(View view) {
        String title = "Tipe Properti";
        String[] codeList = viewModel.getState().getValue().propertyTypeCodeList;
        String[] nameList = viewModel.getState().getValue().propertyTypeNameList;

        BottomSheetDialog dialog = new BottomSheetDialog(mContext);
        dialog.setContentView(R.layout.bottom_sheet_view);

        BottomSheetListView listViewName = dialog.findViewById(R.id.list_item);
        TextView titleView = dialog.findViewById(R.id.list_title);

        titleView.setText(title);

        ArrayAdapter adapter = new ArrayAdapter(mContext, R.layout.item_bottom_sheet, nameList);
        listViewName.setAdapter(adapter);

        listViewName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mPropertyTypeCode = codeList[position];
                mPropertyType.setText(parent.getItemAtPosition(position).toString());

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void showDesignStyle(View view) {
        String title = "Gaya Desain";
        String[] codeList = viewModel.getState().getValue().designStyleCodeList;
        String[] nameList = viewModel.getState().getValue().designStyleNameList;

        BottomSheetDialog dialog = new BottomSheetDialog(mContext);
        dialog.setContentView(R.layout.bottom_sheet_view);

        BottomSheetListView listViewName = dialog.findViewById(R.id.list_item);
        TextView titleView = dialog.findViewById(R.id.list_title);

        titleView.setText(title);

        ArrayAdapter adapter = new ArrayAdapter(mContext, R.layout.item_bottom_sheet, nameList);
        listViewName.setAdapter(adapter);

        listViewName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDesignStyleCode = codeList[position];
                mDesignStyle.setText(parent.getItemAtPosition(position).toString());

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void addPhoto(View view) {
        ImagePicker.create(this).start();
    }

    private void showPhotosToAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        CreatePortfolioPhotoAdapter adapter = new CreatePortfolioPhotoAdapter(mContext, images);

        mPhotos.setLayoutManager(layoutManager);
        mPhotos.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            images = ImagePicker.getImages(data);

            for (Image image : images) {
                File photoEncoded = new File(image.getPath());

                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), photoEncoded);
                photos.add(MultipartBody.Part.createFormData("photos[]", image.getName(), requestFile));
            }

            showPhotosToAdapter();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
