package com.example.designer_mobile.Screen.Gallery.Update;

import com.example.designer_mobile.Base.BaseErrorResponse;
import com.example.designer_mobile.Base.BaseMasterCode;
import com.example.designer_mobile.Base.BaseResponse;
import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.Model.MasterCode;
import com.example.designer_mobile.Model.Gallery;
import com.example.designer_mobile.Network.Service;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.example.designer_mobile.Utils.ErrorUtil;
import com.example.designer_mobile.Utils.StateViewModel;
import com.example.designer_mobile.Utils.UtilApi;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateGalleryViewModel extends StateViewModel<UpdateGalleryViewModel.State> {
    private static final String TAG = "UpdateGalleryViewModel";

    Service mApiService = UtilApi.getApiService();

    AppState appState = AppState.getInstance();

    BaseMasterCode masterCode = new BaseMasterCode();

    @Override
    protected UpdateGalleryViewModel.State initState() {
        return new UpdateGalleryViewModel.State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        UpdateGalleryViewModel.Status status = UpdateGalleryViewModel.Status.INIT;
        String message = "";
        Gallery gallery;
        String[] propertyTypeCodeList;
        String[] propertyTypeNameList;
        String[] designStyleCodeList;
        String[] designStyleNameList;
    }

    void getPropertyTypeList() {
        state.isLoading = true;
        state.status = UpdateGalleryViewModel.Status.INIT;
        updateState();

        String parent_code = "PRT";
        mApiService.getMasterCodeByParent(parent_code).enqueue(new Callback<List<MasterCode>>() {
            @Override
            public void onResponse(Call<List<MasterCode>> call, Response<List<MasterCode>> response) {
                state.isLoading = false;
                List<MasterCode> data = response.body();

                state.propertyTypeCodeList = masterCode.getCodeList(data);
                state.propertyTypeNameList = masterCode.getNameList(data);

                updateState();
            }

            @Override
            public void onFailure(Call<List<MasterCode>> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    void getDesignStyleList() {
        state.isLoading = true;
        state.status = UpdateGalleryViewModel.Status.INIT;
        updateState();

        String parent_code = "DSY";
        mApiService.getMasterCodeByParent(parent_code).enqueue(new Callback<List<MasterCode>>() {
            @Override
            public void onResponse(Call<List<MasterCode>> call, Response<List<MasterCode>> response) {
                state.isLoading = false;
                List<MasterCode> data = response.body();

                state.designStyleCodeList = masterCode.getCodeList(data);
                state.designStyleNameList = masterCode.getNameList(data);
                updateState();
            }

            @Override
            public void onFailure(Call<List<MasterCode>> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    void getGallery(String galleryId) {
        state.isLoading = true;
        state.status = UpdateGalleryViewModel.Status.INIT;
        updateState();

        mApiService.getGalleryById(galleryId).enqueue(new Callback<BaseResponse<Gallery>>() {
            @Override
            public void onResponse(Call<BaseResponse<Gallery>> call, Response<BaseResponse<Gallery>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.gallery = response.body().getData();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Gallery>> call, Throwable t) {

            }
        });

    }

    void updateGallery(Map<String, String> parameters) {
        state.isLoading = true;
        state.status = UpdateGalleryViewModel.Status.INIT;
        updateState();

        mApiService.updateGalleryById(
                parameters.get("gallery_id"),
                appState.getDesigner().getId(),
                parameters.get("title"),
                parameters.get("property_type"),
                parameters.get("room_length"),
                parameters.get("room_width"),
                parameters.get("room_height"),
                parameters.get("design_style"),
                parameters.get("description")
        ).enqueue(new Callback<BaseResponse<Gallery>>() {
            @Override
            public void onResponse(Call<BaseResponse<Gallery>> call, Response<BaseResponse<Gallery>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = Status.SUCCESS;
                    state.message = response.body().getMessage();
                    state.gallery = response.body().getData();

                    getDataUser();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Gallery>> call, Throwable t) {

            }
        });

    }

    private void getDataUser() {
        mApiService.getProfile().enqueue(new Callback<BaseResponse<Designer>>() {
            @Override
            public void onResponse(Call<BaseResponse<Designer>> call, Response<BaseResponse<Designer>> response) {
                Designer designer = response.body().getData();
                appState.setDesigner(designer);
            }

            @Override
            public void onFailure(Call<BaseResponse<Designer>> call, Throwable t) {
                state.isLoading = false;
                state.status = UpdateGalleryViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }
}
