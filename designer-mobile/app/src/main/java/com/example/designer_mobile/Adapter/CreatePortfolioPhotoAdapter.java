package com.example.designer_mobile.Adapter;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.model.Image;
import com.example.designer_mobile.Model.PortfolioPhoto;
import com.example.designer_mobile.R;

import java.io.File;
import java.util.List;

public class CreatePortfolioPhotoAdapter extends RecyclerView.Adapter<CreatePortfolioPhotoAdapter.ViewHolder> {
    static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mPhoto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mPhoto = itemView.findViewById(R.id.photo);
        }
    }

    private List<Image> photos;
    private Context context;

    public CreatePortfolioPhotoAdapter(Context context, List<Image> photos) {
        this.photos = photos;
        this.context = context;
    }

    @NonNull
    @Override
    public CreatePortfolioPhotoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_create_portfolio_photo, parent, false);
        CreatePortfolioPhotoAdapter.ViewHolder holder = new CreatePortfolioPhotoAdapter.ViewHolder(layoutView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Image photo = photos.get(position);
        if (photo != null) {
            Glide.with(context)
                    .load(photo.getPath())
                    .placeholder(R.mipmap.ic_interio_logo)
                    .into(holder.mPhoto);
        }
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }
}
