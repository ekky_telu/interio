package com.example.designer_mobile.Screen.PemberianDesain;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.example.designer_mobile.Adapter.CreatePortfolioPhotoAdapter;
import com.example.designer_mobile.Model.Transaction;
import com.example.designer_mobile.R;
import com.example.designer_mobile.Screen.DetailPesanan.DetailPesanan;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PemberianDesain extends AppCompatActivity {

    private static final String TAG = "DetailPesananActivity";

    private PemberianDesainViewModel viewModel;

    /* Define Component */
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.status)
    TextView mStatus;

    @BindView(R.id.isideskriptif)
    TextView mDescription;

    @BindView(R.id.isitipe)
    TextView mPropertyType;

    @BindView(R.id.panjangruangan)
    TextView mRoomLength;

    @BindView(R.id.lebarruangan)
    TextView mRoomWidth;

    @BindView(R.id.tinggiruangan)
    TextView mRoomHeight;

    @BindView(R.id.photos)
    RecyclerView mPhotos;

    @BindView(R.id.deskripsi_desainer)
    EditText mResultDescription;

    String transactionId;

    List<MultipartBody.Part> photos = new ArrayList<>();
    List<Image> images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemberian_desain);

        ButterKnife.bind(this);
        transactionId = getIntent().getStringExtra("transaction_id");

        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(PemberianDesainViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderTransaction(state.transaction);
            renderResponse(state.status);
        });

        viewModel.getTransaction(transactionId);
    }

    private void renderTransaction(Transaction transaction) {
        if (transaction != null) {
            mDescription.setText(transaction.getDescription());
            mPropertyType.setText(transaction.getDetail().get(0).getPropertyTypeDetail().getDescription());
            mRoomLength.setText(transaction.getDetail().get(0).getRoomLength() + "m");
            mRoomWidth.setText(transaction.getDetail().get(0).getRoomWidth() + "m");
            mRoomHeight.setText(transaction.getDetail().get(0).getRoomHeight() + "m");
            mStatus.setText(transaction.getStatusDetail().getDescription());
        }
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderResponse(PemberianDesainViewModel.Status status) {
        if (status == PemberianDesainViewModel.Status.SUCCESS) {
            onBackPressed();
            finish();
        } else if (status == PemberianDesainViewModel.Status.ERROR) {
            Toast.makeText(this, viewModel.getState().getValue().message, Toast.LENGTH_SHORT).show();
        }
    }

    public void addPhoto(View view) {
        ImagePicker.create(this).start();
    }

    private void showPhotosToAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        CreatePortfolioPhotoAdapter adapter = new CreatePortfolioPhotoAdapter(this, images);

        mPhotos.setLayoutManager(layoutManager);
        mPhotos.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            images = ImagePicker.getImages(data);

            for (Image image : images) {
                File photoEncoded = new File(image.getPath());

                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), photoEncoded);
                photos.add(MultipartBody.Part.createFormData("photos[]", image.getName(), requestFile));
            }

            showPhotosToAdapter();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void sendDesign(View view) {
        if (!validateParameters()) {
            Toast.makeText(this, "Lengkapi Data!", Toast.LENGTH_SHORT).show();
            return;
        }

        RequestBody transactionId = RequestBody.create(MediaType.parse("text/plain"), this.transactionId);
        RequestBody resultDescription = RequestBody.create(MediaType.parse("text/plain"), mResultDescription.getText().toString());
        MultipartBody.Part[] photosParameter = new MultipartBody.Part[photos.size()];
        for (int i = 0; i < photos.size(); i++) {
            photosParameter[i] = photos.get(i);
        }

        viewModel.sendDesign(transactionId, resultDescription, photosParameter);
    }

    private boolean validateParameters() {
        return !mResultDescription.getText().toString().equals("") && photos.size() >= 1;
    }
}
