package com.example.designer_mobile.Screen.Project.Detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.example.designer_mobile.Adapter.PortfolioPhotoAdapter;
import com.example.designer_mobile.Model.PortfolioPhoto;
import com.example.designer_mobile.Model.Project;
import com.example.designer_mobile.Network.Client;
import com.example.designer_mobile.R;
import com.example.designer_mobile.Base.SpacesItemDecoration;
import com.example.designer_mobile.Screen.Project.Update.UpdateProject;
import com.example.designer_mobile.SharedPreferences.AppState;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailProject extends AppCompatActivity {
    private static final String TAG = "DetailProjectActivity";

    /* Define ViewModel */
    private DetailProjectViewModel viewModel;

    /* Define Context */
    private Context mContext;

    /* Define ID Gallery */
    private String id;

    /* Define Component */
    @BindView(R.id.name)
    TextView mName;
    @BindView(R.id.username)
    TextView mUsername;
    @BindView(R.id.designer_type)
    TextView mDesignerType;
    @BindView(R.id.design_style)
    TextView mDesignStyle;
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.description)
    TextView mDescription;
    @BindView(R.id.property_type)
    TextView mPropertyType;
    @BindView(R.id.room_size)
    TextView mRoomSize;
    @BindView(R.id.duration)
    TextView mDuration;
    @BindView(R.id.location)
    TextView mLocation;
    @BindView(R.id.toolbar)
    Toolbar mToolBar;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.photos)
    RecyclerView mPhotos;
    @BindView(R.id.btnEdit)
    RelativeLayout mBtnEdit;

    Project project;
    SpacesItemDecoration decoration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_project);

        ButterKnife.bind(this);
        mContext = this;

        decoration = new SpacesItemDecoration(25);
        mPhotos.addItemDecoration(decoration);
        setToolbar(mToolBar);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        id = getIntent().getStringExtra("project_id");
        viewModel = new ViewModelProvider(this).get(DetailProjectViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderData(state.project, state.status);
        });

        viewModel.getData(id);
    }

    void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderData(Project project, DetailProjectViewModel.Status status) {
        if (project != null && status == DetailProjectViewModel.Status.SUCCESS) {
            double roomSize = (Integer.parseInt(project.getRoomLength())) *
                    (Integer.parseInt(project.getRoomWidth())) *
                    (Integer.parseInt(project.getRoomHeight()));

            if (!project.getDesignerId().equals(AppState.getInstance().getDesigner().getId())) {
                mBtnEdit.setVisibility(View.GONE);
            }

            mName.setText(project.getDesigner().getName());
            mUsername.setText("@" + project.getDesigner().getUsername());
            mDesignerType.setText("Designer (Individual)");
            mDesignStyle.setText(project.getDesignStyleDetail().getDescription());
            mTitle.setText(project.getTitle());
            mDescription.setText(project.getDescription());
            mPropertyType.setText(project.getPropertyTypeDetail().getDescription());
            mRoomSize.setText(String.valueOf(roomSize));
            mLocation.setText(project.getLocation());
            mDuration.setText(project.getDuration());

            mPhotos.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            });

            PortfolioPhotoAdapter adapter = new PortfolioPhotoAdapter(this, project.getPhotos(), Client.PROJECT_IMAGE_URL);
            mPhotos.setAdapter(adapter);

            this.project = project;
        }
    }

    private void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void updateProject(View view) {
        Intent update = new Intent(this, UpdateProject.class);
        update.putExtra("project_id", project.getId());
        startActivity(update);
    }
}
