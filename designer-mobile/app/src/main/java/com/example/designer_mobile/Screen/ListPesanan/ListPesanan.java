package com.example.designer_mobile.Screen.ListPesanan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.designer_mobile.Adapter.ListPesananAdapter;
import com.example.designer_mobile.R;
import com.example.designer_mobile.Screen.Login.LoginViewModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListPesanan extends AppCompatActivity {
    private static final String TAG = "ListPesananActivity";

    //vars
    private ArrayList<String> mOrder = new ArrayList<>();
    private ArrayList<String> mStatus = new ArrayList<>();

    private String user_id;
    private Context mContext;
    private ListPesananViewModel viewModel;

    /* Define Component */

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_pesanan);

        ButterKnife.bind(this);
        init();

        initTextView();
    }

    private void init() {
        user_id = getIntent().getStringExtra("user_id");
        viewModel = new ViewModelProvider(this).get(ListPesananViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
        });
        viewModel.getListPesanan(user_id);
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void initTextView() {
        mOrder.add("Order223 ");
        mStatus.add("Sedang di Proses");

        mOrder.add("Order 321");
        mStatus.add("Diterima");

        mOrder.add("Order 097");
        mStatus.add("Dibatalkan");

        mOrder.add("Option");
        mStatus.add("Status");

        mOrder.add("Option");
        mStatus.add("Status");

//        initRecyclerView();
    }

//    private void initRecyclerView() {
//        RecyclerView recyclerView = findViewById(R.id.recyclerViewPesanan);
//        ListPesananAdapter adapter = new ListPesananAdapter(this, mOrder, mStatus);
//        recyclerView.setAdapter(adapter);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//    }
}
