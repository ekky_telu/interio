package com.example.designer_mobile.Screen.Project.Create;

import android.util.Log;
import android.widget.Toast;

import com.example.designer_mobile.Base.BaseErrorResponse;
import com.example.designer_mobile.Base.BaseMasterCode;
import com.example.designer_mobile.Base.BaseResponse;
import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.Model.MasterCode;
import com.example.designer_mobile.Model.Project;
import com.example.designer_mobile.Network.Service;
import com.example.designer_mobile.Screen.Project.Detail.DetailProjectViewModel;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.example.designer_mobile.Utils.ErrorUtil;
import com.example.designer_mobile.Utils.StateViewModel;
import com.example.designer_mobile.Utils.UtilApi;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateProjectViewModel extends StateViewModel<CreateProjectViewModel.State> {
    private static final String TAG = "CreateProjectViewModel";

    Service mApiService = UtilApi.getApiService();

    AppState appState = AppState.getInstance();

    BaseMasterCode masterCode = new BaseMasterCode();

    @Override
    protected CreateProjectViewModel.State initState() {
        return new CreateProjectViewModel.State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message = "";
        Project project = new Project();
        String[] propertyTypeCodeList;
        String[] propertyTypeNameList;
        String[] designStyleCodeList;
        String[] designStyleNameList;
        String[] unitDurationCodeList;
        String[] unitDurationNameList;
    }

    void getPropertyTypeList() {
        state.isLoading = true;
        state.status = CreateProjectViewModel.Status.INIT;
        updateState();

        String parent_code = "PRT";
        mApiService.getMasterCodeByParent(parent_code).enqueue(new Callback<List<MasterCode>>() {
            @Override
            public void onResponse(Call<List<MasterCode>> call, Response<List<MasterCode>> response) {
                state.isLoading = false;
                List<MasterCode> data = response.body();

                state.propertyTypeCodeList = masterCode.getCodeList(data);
                state.propertyTypeNameList = masterCode.getNameList(data);

                updateState();
            }

            @Override
            public void onFailure(Call<List<MasterCode>> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    void getDesignStyleList() {
        state.isLoading = true;
        state.status = CreateProjectViewModel.Status.INIT;
        updateState();

        String parent_code = "DSY";
        mApiService.getMasterCodeByParent(parent_code).enqueue(new Callback<List<MasterCode>>() {
            @Override
            public void onResponse(Call<List<MasterCode>> call, Response<List<MasterCode>> response) {
                state.isLoading = false;
                List<MasterCode> data = response.body();

                state.designStyleCodeList = masterCode.getCodeList(data);
                state.designStyleNameList = masterCode.getNameList(data);
                updateState();
            }

            @Override
            public void onFailure(Call<List<MasterCode>> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    void getUnitDurationList() {
        state.isLoading = true;
        state.status = CreateProjectViewModel.Status.INIT;
        updateState();

        String parent_code = "UDT";
        mApiService.getMasterCodeByParent(parent_code).enqueue(new Callback<List<MasterCode>>() {
            @Override
            public void onResponse(Call<List<MasterCode>> call, Response<List<MasterCode>> response) {
                state.isLoading = false;
                List<MasterCode> data = response.body();

                state.unitDurationCodeList = masterCode.getCodeList(data);
                state.unitDurationNameList = masterCode.getNameList(data);
                updateState();
            }

            @Override
            public void onFailure(Call<List<MasterCode>> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    void storeProject(Map<String, RequestBody> parameters, MultipartBody.Part[] arrayParameters) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();

        RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), appState.getDesigner().getId());

        mApiService.storeProject(
                userId,
                parameters.get("title"),
                parameters.get("location"),
                parameters.get("property_type"),
                parameters.get("room_length"),
                parameters.get("room_width"),
                parameters.get("room_height"),
                parameters.get("design_style"),
                parameters.get("duration"),
                parameters.get("description"),
                arrayParameters
        ).enqueue(new Callback<BaseResponse<Project>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Project>> call, @NotNull Response<BaseResponse<Project>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = Status.SUCCESS;
                    state.message = response.body().getMessage();
                    state.project = response.body().getData();

                    getDataUser();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Project>> call, Throwable t) {
                state.isLoading = false;
                state.status = Status.ERROR;
                state.message = t.getMessage();
            }
        });
    }

    private void getDataUser() {
        mApiService.getProfile().enqueue(new Callback<BaseResponse<Designer>>() {
            @Override
            public void onResponse(Call<BaseResponse<Designer>> call, Response<BaseResponse<Designer>> response) {
                Designer designer = response.body().getData();
                appState.setDesigner(designer);
            }

            @Override
            public void onFailure(Call<BaseResponse<Designer>> call, Throwable t) {
                state.isLoading = false;
                state.status = CreateProjectViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }
}
