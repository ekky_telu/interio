package com.example.designer_mobile.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TransactionDetail {
    @SerializedName("id")
    private String id;
    @SerializedName("transaction_id")
    private String transaction_id;
    @SerializedName("transaction")
    private Transaction transaction;
    @SerializedName("property_type")
    private String property_type;
    @SerializedName("property_type_detail")
    private MasterCode property_type_detail;
    @SerializedName("room_length")
    private String room_length;
    @SerializedName("room_width")
    private String room_width;
    @SerializedName("room_height")
    private String room_height;
    @SerializedName("design_style")
    private String design_style;
    @SerializedName("design_style_detail")
    private MasterCode design_style_detail;
    @SerializedName("result_description")
    private String result_description;
    @SerializedName("status")
    private String status;
    @SerializedName("status_detail")
    private MasterCode status_detail;
    @SerializedName("result_photo")
    private List<PortfolioPhoto> result_photos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTransactionId() {
        return transaction_id;
    }

    public void setTransactionId(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public String getPropertyType() {
        return property_type;
    }

    public void setPropertyType(String property_type) {
        this.property_type = property_type;
    }

    public String getRoomLength() {
        return room_length;
    }

    public void setRoomLength(String room_length) {
        this.room_length = room_length;
    }

    public String getRoomWidth() {
        return room_width;
    }

    public void setRoomWidth(String room_width) {
        this.room_width = room_width;
    }

    public String getRoomHeight() {
        return room_height;
    }

    public void setRoomHeight(String room_height) {
        this.room_height = room_height;
    }

    public String getDesignStyle() {
        return design_style;
    }

    public void setDesignStyle(String design_style) {
        this.design_style = design_style;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResultDescription() {
        return result_description;
    }

    public void setResultDescription(String result_description) {
        this.result_description = result_description;
    }

    public MasterCode getStatusDetail() {
        return status_detail;
    }

    public void setStatusDetail(MasterCode status_detail) {
        this.status_detail = status_detail;
    }

    public MasterCode getPropertyTypeDetail() {
        return property_type_detail;
    }

    public void setPropertyTypeDetail(MasterCode property_type_detail) {
        this.property_type_detail = property_type_detail;
    }

    public MasterCode getDesignStyleDetail() {
        return design_style_detail;
    }

    public void setDesignStyleDetail(MasterCode design_style_detail) {
        this.design_style_detail = design_style_detail;
    }

    public List<PortfolioPhoto> getResultPhotos() {
        return result_photos;
    }

    public void setResultPhotos(List<PortfolioPhoto> result_photos) {
        this.result_photos = result_photos;
    }
}
