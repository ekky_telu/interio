package com.example.designer_mobile.Screen.DetailPesanan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.designer_mobile.Model.Transaction;
import com.example.designer_mobile.R;
import com.example.designer_mobile.Screen.Home.Home;
import com.example.designer_mobile.Screen.ListPesanan.CartFragment;
import com.example.designer_mobile.Screen.ListPesanan.ListPesanan;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailPesanan extends AppCompatActivity {

    private static final String TAG = "DetailPesananActivity";

    private DetailPesananViewModel viewModel;

    /* Define Component */
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.status)
    TextView mStatus;

    @BindView(R.id.isideskriptif)
    TextView mDescription;

    @BindView(R.id.isitipe)
    TextView mPropertyType;

    @BindView(R.id.panjangruangan)
    TextView mRoomLength;

    @BindView(R.id.lebarruangan)
    TextView mRoomWidth;

    @BindView(R.id.tinggiruangan)
    TextView mRoomHeight;

    @BindView(R.id.action_layout)
    RelativeLayout mActionLayout;

    String transactionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pesanan);

        ButterKnife.bind(this);
        transactionId = getIntent().getStringExtra("transaction_id");

        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(DetailPesananViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderTransaction(state.transaction);
            renderResponse(state.status);
        });

        viewModel.getTransaction(transactionId);
    }

    private void renderTransaction(Transaction transaction) {
        if (transaction != null) {
            if (transaction.getStatusDetail().getDescription().equals("Dibatalkan")) {
                mActionLayout.setVisibility(View.GONE);
            }

            mDescription.setText(transaction.getDescription());
            mPropertyType.setText(transaction.getDetail().get(0).getPropertyTypeDetail().getDescription());
            mRoomLength.setText(transaction.getDetail().get(0).getRoomLength() + "m");
            mRoomWidth.setText(transaction.getDetail().get(0).getRoomWidth() + "m");
            mRoomHeight.setText(transaction.getDetail().get(0).getRoomHeight() + "m");
            mStatus.setText(transaction.getStatusDetail().getDescription());
        }
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderResponse(DetailPesananViewModel.Status status) {
        if (status == DetailPesananViewModel.Status.SUCCESS) {
//            Toast.makeText(this, viewModel.getState().getValue().message, Toast.LENGTH_SHORT).show();

//            Intent list = new Intent(this, DetailPesanan.class);
//            list.putExtra("transaction_id", transactionId);
//            startActivity(list);
            onBackPressed();
            finish();
        } else if (status == DetailPesananViewModel.Status.ERROR) {
            Toast.makeText(this, viewModel.getState().getValue().message, Toast.LENGTH_SHORT).show();
        }
    }

    public void accept(View view) {
        viewModel.acceptTransaction(transactionId);
    }

    public void cancel(View view) {
        viewModel.cancelTransaction(transactionId);
    }

    public void back(View view) {
        onBackPressed();
    }
}
