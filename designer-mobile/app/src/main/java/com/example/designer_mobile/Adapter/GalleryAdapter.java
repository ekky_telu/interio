package com.example.designer_mobile.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.Model.Gallery;
import com.example.designer_mobile.Network.Client;
import com.example.designer_mobile.R;
import com.example.designer_mobile.Screen.Gallery.Detail.DetailGallery;

import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {
    private List<Gallery> galleries;
    private Context context;

    public GalleryAdapter(Context context, List<Gallery> galleries) {
        this.context = context;
        this.galleries = galleries;
    }

    @NonNull
    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gallery, parent, false);
        return new GalleryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryAdapter.ViewHolder holder, int position) {
        Gallery gallery = galleries.get(position);

        holder.mTitle.setText(gallery.getTitle());
        Glide.with(context)
                .load(Client.GALLERY_IMAGE_URL + gallery.getPhotos().get(0).getImage())
                .into(holder.mPhoto);

        holder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detailGallery = new Intent(v.getContext(), DetailGallery.class);
                detailGallery.putExtra("gallery_id", gallery.getId());
                v.getContext().startActivity(detailGallery);
            }
        });
    }

    @Override
    public int getItemCount() {
        return galleries.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mPhoto;
        TextView mTitle;
        LinearLayout mLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            mPhoto = itemView.findViewById(R.id.photo);
            mTitle = itemView.findViewById(R.id.title);
            mLayout = itemView.findViewById(R.id.layout);
        }
    }
}
