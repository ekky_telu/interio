package com.example.designer_mobile.Screen.SettingProfile;

import android.util.Log;

import com.example.designer_mobile.Base.BaseErrorResponse;
import com.example.designer_mobile.Base.BaseMasterCode;
import com.example.designer_mobile.Base.BaseResponse;
import com.example.designer_mobile.Model.DesignStyle;
import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.Model.MasterCode;
import com.example.designer_mobile.Network.Service;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.example.designer_mobile.Utils.ErrorUtil;
import com.example.designer_mobile.Utils.StateViewModel;
import com.example.designer_mobile.Utils.UtilApi;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingProfileViewModel extends StateViewModel<SettingProfileViewModel.State> {
    private static final String TAG = "DetailProjectViewModel";
    Service mApiService = UtilApi.getApiService();
    AppState appState = AppState.getInstance();
    BaseMasterCode masterCode = new BaseMasterCode();

    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message;
        Designer designer;
        JsonArray errors;
        String[] designStyleCodeList;
        String[] designStyleNameList;
    }

    void updateProfile(String username, String email, String first_name, String last_name, String phone, String province, String city, String address, String role) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();

        mApiService.updateProfile(username, email, first_name, last_name, phone, role, province, city, address).enqueue(new Callback<BaseResponse<Designer>>() {
            @Override
            public void onResponse(Call<BaseResponse<Designer>> call, Response<BaseResponse<Designer>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = SettingProfileViewModel.Status.SUCCESS;
                    state.message = response.body().getMessage();
                    getDataUser();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = SettingProfileViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Designer>> call, Throwable t) {
                state.isLoading = false;
                state.status = SettingProfileViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void updatepassword(String old_password, String password, String password_confirmation, String role) {
        state.isLoading = true;
        state.status = SettingProfileViewModel.Status.INIT;
        updateState();

        mApiService.updatepassword(old_password,password,password_confirmation,role).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = SettingProfileViewModel.Status.SUCCESS;
                    state.message = response.body().getMessage();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = SettingProfileViewModel.Status.ERROR;
                    state.errors = errorResponse.getErrors();

                    JsonObject errorObj = (JsonObject) state.errors.get(0);
                    state.message = errorObj.get("message").getAsString();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                state.isLoading = false;
                state.status = SettingProfileViewModel.Status.ERROR;
                state.message = t.getMessage();
                Log.d(TAG, "ON FAILURE " + state.message);

                updateState();
            }
        });
    }

    void updatePrice(String price) {
        state.isLoading = true;
        state.status = SettingProfileViewModel.Status.INIT;
        updateState();

        mApiService.updatePrice(price).enqueue(new Callback<BaseResponse<Designer>>() {
            @Override
            public void onResponse(Call<BaseResponse<Designer>> call, Response<BaseResponse<Designer>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = SettingProfileViewModel.Status.SUCCESS;
                    state.message = response.body().getMessage();
                    getDataUser();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = SettingProfileViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Designer>> call, Throwable t) {
                state.isLoading = false;
                state.status = SettingProfileViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void updateDesignStyle(String mainDesignStyle, String[] designStyles) {
        state.isLoading = true;
        state.status = SettingProfileViewModel.Status.INIT;
        updateState();

        mApiService.updateDesignStyle(mainDesignStyle, designStyles).enqueue(new Callback<BaseResponse<Designer>>() {
            @Override
            public void onResponse(Call<BaseResponse<Designer>> call, Response<BaseResponse<Designer>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = SettingProfileViewModel.Status.SUCCESS;
                    state.message = response.body().getMessage();
                    getDataUser();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = SettingProfileViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Designer>> call, Throwable t) {
                state.isLoading = false;
                state.status = SettingProfileViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void getDesignStyleList() {
        state.isLoading = true;
        state.status = SettingProfileViewModel.Status.INIT;
        updateState();

        String parent_code = "DSY";
        mApiService.getMasterCodeByParent(parent_code).enqueue(new Callback<List<MasterCode>>() {
            @Override
            public void onResponse(Call<List<MasterCode>> call, Response<List<MasterCode>> response) {
                state.isLoading = false;
                state.status = Status.SUCCESS;
                List<MasterCode> data = response.body();

                state.designStyleCodeList = masterCode.getCodeList(data);
                state.designStyleNameList = masterCode.getNameList(data);
                updateState();
            }

            @Override
            public void onFailure(Call<List<MasterCode>> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    private void getDataUser() {
        mApiService.getProfile().enqueue(new Callback<BaseResponse<Designer>>() {
            @Override
            public void onResponse(Call<BaseResponse<Designer>> call, Response<BaseResponse<Designer>> response) {
                Designer designer = response.body().getData();
                appState.setDesigner(designer);
            }

            @Override
            public void onFailure(Call<BaseResponse<Designer>> call, Throwable t) {
                state.isLoading = false;
                state.status = SettingProfileViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void logout() {
        state.isLoading = true;
        state.status = SettingProfileViewModel.Status.INIT;
        updateState();

        mApiService.logout().enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = SettingProfileViewModel.Status.SUCCESS;
                    state.message = response.body().getMessage();
                    appState.setIsLoggedIn(false);
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = SettingProfileViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                state.isLoading = false;
                state.status = SettingProfileViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void getProfile() {
        state.designer = appState.getDesigner();
    }
}
