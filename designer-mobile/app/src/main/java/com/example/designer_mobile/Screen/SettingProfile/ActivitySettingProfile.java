package com.example.designer_mobile.Screen.SettingProfile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.designer_mobile.R;
import com.example.designer_mobile.Screen.Login.Login;
import com.example.designer_mobile.Screen.ProfileDesigner.ProfileDesigner;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySettingProfile extends AppCompatActivity {
    private static final String TAG = "ActivitySettingProfile";

    /* Define ViewModel */
    private SettingProfileViewModel viewModel;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_profile);

        ButterKnife.bind(this);
        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(SettingProfileViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderResponse(state.status, state.message);
        });
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    void renderResponse(SettingProfileViewModel.Status status, String message) {
        if (message != null) {
            if (status == SettingProfileViewModel.Status.SUCCESS) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

                Intent login = new Intent(this, Login.class);
                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(login);
                finish();
            } else if (status == SettingProfileViewModel.Status.ERROR) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void intentUDD(View view) {
        Intent i = new Intent(this, ChangeProfile.class);
        startActivity(i);
    }

    public void logout(View view) {
        viewModel.logout();
    }

    public void intenUKS(View view) {
        Intent i = new Intent(this, UbahKataSandi.class);
        startActivity(i);
    }

    public void intentSH(View view) {
        Intent i = new Intent(this, SetPrice.class);
        startActivity(i);
    }

    public void intentAGD(View view) {
        Intent i = new Intent(this, SetStyle.class);
        startActivity(i);
    }

    public void intentPD(View view) {
        onBackPressed();
        finish();
    }
}
