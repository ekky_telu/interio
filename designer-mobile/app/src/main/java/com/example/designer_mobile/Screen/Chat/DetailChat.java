package com.example.designer_mobile.Screen.Chat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.designer_mobile.Adapter.DetailChatAdapter;
import com.example.designer_mobile.Model.ChatMessage;
import com.example.designer_mobile.R;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailChat extends AppCompatActivity {

    String TAG = "DetailChatActivity";

    @BindView(R.id.toolbar)
    Toolbar mToolBar;
    @BindView(R.id.messages)
    RecyclerView mMessages;
    @BindView(R.id.name)
    TextView mName;
    @BindView(R.id.username)
    TextView mUsername;
    @BindView(R.id.message)
    TextView mMessage;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    FirebaseDatabase firebase = FirebaseDatabase.getInstance();
    DatabaseReference dbRef;
    DetailChatAdapter adapter;

    String chatUid;
    String userUid;
    String designerUid;
    List<ChatMessage> chatMessages = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_chat);

        dbRef = firebase.getReference();

        ButterKnife.bind(this);
        setToolbar(mToolBar);

        chatUid = getIntent().getStringExtra("chat_uid");
        designerUid = AppState.getInstance().getDesignerUid();

        showChat(chatUid);
    }

    private void showChat(String chatUid) {
        setVisibilityProgressBar(true);
        dbRef.child("chatMembers").child(chatUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String userUid = null;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (!snapshot.getKey().equals(designerUid)) {
                        userUid = snapshot.getKey();
                        break;
                    }
                }

                setUserInfo(userUid);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        dbRef.child("messages").child(chatUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String sentBy = snapshot.child("sentBy").getValue().toString();
                    String message = snapshot.child("message").getValue().toString();
                    String date = snapshot.child("date").getValue().toString();
                    String time = snapshot.child("time").getValue().toString();

                    ChatMessage chatMessage = new ChatMessage(sentBy, message, time, date);
                    chatMessages.add(chatMessage);

                    showMessages();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setUserInfo(String userUid) {
        dbRef.child("users").child(userUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String firstName = dataSnapshot.child("firstName").getValue().toString();
                String username = dataSnapshot.child("username").getValue().toString();

                mName.setText(firstName);
                mUsername.setText("@" + username);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void showMessages() {
        adapter = new DetailChatAdapter(chatMessages, designerUid);

        mMessages.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mMessages.setAdapter(adapter);

        setVisibilityProgressBar(false);
    }

    public void sendMessage(View view) {
        String message = mMessage.getText().toString();
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String date = dateFormat.format(c);
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        String time = timeFormat.format(c);

        ChatMessage chatMessage = new ChatMessage(designerUid, message, time, date);
        dbRef.child("messages").child(chatUid).push().setValue(chatMessage);

        chatMessages.add(chatMessage);
        adapter.notifyItemInserted(chatMessages.size() - 1);
        mMessage.setText("");

        Map<String, Object> chatData = new HashMap<>();
        chatData.put("lastDate", date);
        chatData.put("lastTime", time);
        chatData.put("lastMessage", message);
        chatData.put("lastSentBy", designerUid);
        chatData.put("lastSentRole", "designers");

        dbRef.child("chats").child(chatUid).updateChildren(chatData);
    }

    private void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_dark);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
//            startActivity(new Intent(this, ChatList.class));
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setVisibilityProgressBar(boolean visibility) {
        mProgressBar.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
