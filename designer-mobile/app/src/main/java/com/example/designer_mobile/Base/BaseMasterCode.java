package com.example.designer_mobile.Base;

import com.example.designer_mobile.Model.MasterCode;

import java.util.List;

public class BaseMasterCode {

    public String[] getCodeList(List<MasterCode> data) {
        String[] code = new String[data.size()];
        for (int i = 0; i < data.size(); i++) {
            code[i] = data.get(i).getCode();
        }

        return code;
    }

    public String[] getNameList(List<MasterCode> data) {
        String[] name = new String[data.size()];
        for (int i = 0; i < data.size(); i++) {
            name[i] = data.get(i).getDescription();
        }

        return name;
    }

}
