package com.example.designer_mobile.Screen.DetailPesanan;

import com.example.designer_mobile.Base.BaseErrorResponse;
import com.example.designer_mobile.Base.BaseResponse;
import com.example.designer_mobile.Model.Transaction;
import com.example.designer_mobile.Network.Service;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.example.designer_mobile.Utils.ErrorUtil;
import com.example.designer_mobile.Utils.StateViewModel;
import com.example.designer_mobile.Utils.UtilApi;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPesananViewModel extends StateViewModel<DetailPesananViewModel.State> {
    private static final String TAG = "RegisterActivity";
    Service mApiService = UtilApi.getApiService();

    AppState appState = AppState.getInstance();

    @Override
    protected DetailPesananViewModel.State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        DetailPesananViewModel.Status status = DetailPesananViewModel.Status.INIT;
        String message = "";
        Transaction transaction;
    }

    void getTransaction(String transactionId) {
        state.isLoading = true;
        state.status = DetailPesananViewModel.Status.INIT;
        updateState();

        mApiService.getTransactionById(transactionId).enqueue(new Callback<BaseResponse<Transaction>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Transaction>> call, @NotNull Response<BaseResponse<Transaction>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.transaction = response.body().getData();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = DetailPesananViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<Transaction>> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = DetailPesananViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void acceptTransaction(String transactionId) {
        state.isLoading = true;
        state.status = DetailPesananViewModel.Status.INIT;
        updateState();

        mApiService.acceptTransaction(transactionId).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse> call, @NotNull Response<BaseResponse> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = DetailPesananViewModel.Status.SUCCESS;
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = DetailPesananViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = DetailPesananViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void cancelTransaction(String transactionId) {
        state.isLoading = true;
        state.status = DetailPesananViewModel.Status.INIT;
        updateState();

        mApiService.cancelTransaction(transactionId).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse> call, @NotNull Response<BaseResponse> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = DetailPesananViewModel.Status.SUCCESS;
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = DetailPesananViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = DetailPesananViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }
}
