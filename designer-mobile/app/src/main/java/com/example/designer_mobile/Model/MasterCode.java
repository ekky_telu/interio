package com.example.designer_mobile.Model;

import com.google.gson.annotations.SerializedName;

public class MasterCode {
    @SerializedName("id")
    private String id;

    @SerializedName("parent_code")
    private String parent_code;

    @SerializedName("code")
    private String code;

    @SerializedName("description")
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentCode() {
        return parent_code;
    }

    public void setParentCode(String parent_code) {
        this.parent_code = parent_code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
