package com.example.designer_mobile.Screen.Register;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.designer_mobile.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SetPassword extends AppCompatActivity {
    String mUsername;

    @BindView(R.id.password)
    EditText mPassword;
    @BindView(R.id.confirm_password)
    EditText mConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register2);
        ButterKnife.bind(this);

        mUsername = getIntent().getStringExtra("username");
    }

    public void next(View view) {
        if (mPassword.getText().toString().equals("")) {
            Toast.makeText(this, "Password Belum Diisi", Toast.LENGTH_SHORT).show();
            return;
        }

        if (mConfirmPassword.getText().toString().equals("")) {
            Toast.makeText(this, "Konfirmasi Password Belum Diisi", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!mConfirmPassword.getText().toString().equals(mPassword.getText().toString())) {
            Toast.makeText(this, "Password Tidak Sama dengan Konfirmasi Password", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(this, InputData.class);
        intent.putExtra("username", mUsername);
        intent.putExtra("password", mPassword.getText().toString());
        intent.putExtra("password_confirmation", mConfirmPassword.getText().toString());
        startActivity(intent);
    }
}
