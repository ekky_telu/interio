package com.example.designer_mobile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.designer_mobile.SharedPreferences.AppState;

public class MainActivity extends AppCompatActivity {

    AppState appState = new AppState();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
