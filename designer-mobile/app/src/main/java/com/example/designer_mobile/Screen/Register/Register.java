package com.example.designer_mobile.Screen.Register;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.designer_mobile.R;
import com.example.designer_mobile.Screen.Login.Login;
import com.example.designer_mobile.Screen.Project.Detail.DetailProjectViewModel;
import com.google.gson.JsonArray;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Register extends AppCompatActivity {
    private static final String TAG = "RegisterActivity";
    private RegisterViewModel viewModel;

    @BindView(R.id.username)
    TextView mUsername;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(RegisterViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderResponse(state.isError);
        });
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void next(View view) {
        if (mUsername.getText().toString().equals("")) {
            Toast.makeText(this, "Username Belum Diisi", Toast.LENGTH_SHORT).show();
            return;
        }

        viewModel.checkUsername(mUsername.getText().toString(), "designer");
    }

    public void renderResponse(boolean isError) {
        if (isError) {
            if (!viewModel.getState().getValue().message.equals("") && viewModel.getState().getValue().status == RegisterViewModel.Status.ERROR) {
                Toast.makeText(this, viewModel.getState().getValue().message, Toast.LENGTH_SHORT).show();
            }
            return;
        }

        Intent intent = new Intent(this, SetPassword.class);
        intent.putExtra("username", mUsername.getText().toString());
        startActivity(intent);
    }

    public void showLogin(View view) {
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
        finish();
    }
}
