package com.example.designer_mobile.Screen.Login;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.designer_mobile.Base.BaseErrorResponse;
import com.example.designer_mobile.Base.BaseResponse;
import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.Model.Login;
import com.example.designer_mobile.Network.Service;
import com.example.designer_mobile.Screen.Project.Create.CreateProjectViewModel;
import com.example.designer_mobile.Screen.Project.Detail.DetailProjectViewModel;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.example.designer_mobile.Utils.ErrorUtil;
import com.example.designer_mobile.Utils.StateViewModel;
import com.example.designer_mobile.Utils.UtilApi;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends StateViewModel<LoginViewModel.State> {
    private static final String TAG = "LoginViewModel";
    Service mApiService = UtilApi.getApiService();
    AppState appState = AppState.getInstance();
    FirebaseDatabase firebase = FirebaseDatabase.getInstance();

    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message = "";
        JsonArray errors;
        Login login = new Login();
    }

    void login(String username, String password, String role) {
        state.isLoading = true;
        state.status = LoginViewModel.Status.INIT;
        updateState();

        mApiService.login(username, password, role).enqueue(new Callback<BaseResponse<Login>>() {
            @Override
            public void onResponse(Call<BaseResponse<Login>> call, Response<BaseResponse<Login>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.login = response.body().getData();
                    state.message = response.body().getMessage();
                    appState.setToken(state.login.getAccessToken());
                    getDataUser();
                    appState.setIsLoggedIn(true);
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = LoginViewModel.Status.ERROR;
                    state.errors = errorResponse.getErrors();

                    JsonObject errorObj = (JsonObject) state.errors.get(0);
                    state.message = errorObj.get("message").getAsString();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Login>> call, Throwable t) {
                state.isLoading = false;
                state.status = LoginViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    private void getDataUser() {
        mApiService.getProfile().enqueue(new Callback<BaseResponse<Designer>>() {
            @Override
            public void onResponse(Call<BaseResponse<Designer>> call, Response<BaseResponse<Designer>> response) {
                Designer designer = response.body().getData();
                appState.setDesigner(designer);
                state.status = Status.SUCCESS;

                updateDesignerUid();

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Designer>> call, Throwable t) {
                state.isLoading = false;
                state.status = LoginViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    private void updateDesignerUid() {
        DatabaseReference dbRef = firebase.getReference();
        dbRef.child("designers").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (snapshot.child("username").getValue().toString().equals(appState.getDesigner().getUsername())) {
                        appState.setDesignerUid(snapshot.getKey());
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
