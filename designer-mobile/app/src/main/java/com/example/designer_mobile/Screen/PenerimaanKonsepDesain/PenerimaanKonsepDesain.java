package com.example.designer_mobile.Screen.PenerimaanKonsepDesain;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.designer_mobile.Adapter.CreatePortfolioPhotoAdapter;
import com.example.designer_mobile.Adapter.ResultPhotoAdapter;
import com.example.designer_mobile.Model.PortfolioPhoto;
import com.example.designer_mobile.Model.Transaction;
import com.example.designer_mobile.R;
import com.example.designer_mobile.Screen.DetailPesanan.DetailPesanan;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PenerimaanKonsepDesain extends AppCompatActivity {

    private static final String TAG = "PenerimaanKonsepDesainActivity";

    private PenerimaanKonsepDesainViewModel viewModel;

    /* Define Component */
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.status)
    TextView mStatus;

    @BindView(R.id.isideskriptif)
    TextView mDescription;

    @BindView(R.id.isitipe)
    TextView mPropertyType;

    @BindView(R.id.panjangruangan)
    TextView mRoomLength;

    @BindView(R.id.lebarruangan)
    TextView mRoomWidth;

    @BindView(R.id.tinggiruangan)
    TextView mRoomHeight;

    @BindView(R.id.isi_deskripsi)
    TextView mResultDescription;

    @BindView(R.id.photos)
    RecyclerView mPhotos;

    String transactionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_penerimaan_konsep_desain);

        transactionId = getIntent().getStringExtra("transaction_id");

        ButterKnife.bind(this);
        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(PenerimaanKonsepDesainViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderTransaction(state.transaction);
            renderResponse(state.status);
        });

        viewModel.getTransaction(transactionId);
    }

    private void renderTransaction(Transaction transaction) {
        if (transaction != null) {
            mDescription.setText(transaction.getDescription());
            mPropertyType.setText(transaction.getDetail().get(0).getPropertyTypeDetail().getDescription());
            mRoomLength.setText(transaction.getDetail().get(0).getRoomLength() + "m");
            mRoomWidth.setText(transaction.getDetail().get(0).getRoomWidth() + "m");
            mRoomHeight.setText(transaction.getDetail().get(0).getRoomHeight() + "m");
            mStatus.setText(transaction.getStatusDetail().getDescription());
            mResultDescription.setText(transaction.getDetail().get(0).getResultDescription());

            showPhotosToAdapter(transaction.getDetail().get(0).getResultPhotos());
        }
    }

    private void showPhotosToAdapter(List<PortfolioPhoto> photos) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        ResultPhotoAdapter adapter = new ResultPhotoAdapter(this, photos);

        mPhotos.setLayoutManager(layoutManager);
        mPhotos.setAdapter(adapter);
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderResponse(PenerimaanKonsepDesainViewModel.Status status) {
        if (status == PenerimaanKonsepDesainViewModel.Status.SUCCESS) {
            Toast.makeText(this, viewModel.getState().getValue().message, Toast.LENGTH_SHORT).show();

            Intent list = new Intent(this, DetailPesanan.class);
            list.putExtra("transaction_id", transactionId);
            startActivity(list);
            finish();
        } else if (status == PenerimaanKonsepDesainViewModel.Status.ERROR) {
            Toast.makeText(this, viewModel.getState().getValue().message, Toast.LENGTH_SHORT).show();
        }
    }

    public void back(View view) {
        onBackPressed();
        finish();
    }
}
