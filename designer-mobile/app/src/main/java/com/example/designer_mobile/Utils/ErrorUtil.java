package com.example.designer_mobile.Utils;

import android.util.Log;

import com.example.designer_mobile.Base.BaseErrorResponse;
import com.example.designer_mobile.Network.Client;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtil {

    public static BaseErrorResponse parseError(Response<?> response) {
        Converter<ResponseBody, BaseErrorResponse> converter =
                Client.getRetrofit().responseBodyConverter(BaseErrorResponse.class, new Annotation[0]);

        BaseErrorResponse error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new BaseErrorResponse();
        }

        return error;
    }

}
