package com.example.designer_mobile.Screen.Register;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.example.designer_mobile.Adapter.DesignStyleRegisterAdapter;
import com.example.designer_mobile.Base.SpacesItemDecoration;
import com.example.designer_mobile.MainActivity;
import com.example.designer_mobile.R;
import com.example.designer_mobile.Screen.Login.Login;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SetDesignStyle extends AppCompatActivity {
    private static final String TAG = "SetDesignStyleActivity";

    /* Define ViewModel */
    private RegisterViewModel viewModel;

    String mUsername, mPassword, mPasswordConfirmation, mFName, mLName, mEmail, mPhone;
    String mRole = "designer";

    /* Define Component */
    @BindView(R.id.design_style)
    RecyclerView mDesignStyle;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    DesignStyleRegisterAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register4);

        ButterKnife.bind(this);

        mUsername = getIntent().getStringExtra("username");
        mPassword = getIntent().getStringExtra("password");
        mPasswordConfirmation = getIntent().getStringExtra("password_confirmation");
        mFName = getIntent().getStringExtra("first_name");
        mLName = getIntent().getStringExtra("last_name");
        mEmail = getIntent().getStringExtra("email");
        mPhone = getIntent().getStringExtra("phone");

        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(RegisterViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderDesignStyle(state.status);
            renderResponse(state.status, state.message);
        });

        viewModel.getDesignStyleList();
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderDesignStyle(RegisterViewModel.Status status) {
        if (status == RegisterViewModel.Status.SUCCESS) {
            String[] designStyleCodeList = viewModel.getState().getValue().designStyleCodeList;
            String[] designStyleNameList = viewModel.getState().getValue().designStyleNameList;

            mDesignStyle.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            });

            adapter = new DesignStyleRegisterAdapter(designStyleCodeList, designStyleNameList);
            mDesignStyle.setAdapter(adapter);

            SpacesItemDecoration decoration = new SpacesItemDecoration(25);
            mDesignStyle.addItemDecoration(decoration);
        }
    }

    void renderResponse(RegisterViewModel.Status status, String message) {
        if (!message.equals("")) {
            if (status == RegisterViewModel.Status.SUCCESS) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

                Intent login = new Intent(this, Login.class);
                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(login);
                finish();
            } else if (status == RegisterViewModel.Status.ERROR) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void register(View view) {
        String[] designStyle = new String[adapter.getItemCount()];
        adapter.getItems().toArray(designStyle);

        viewModel.register(mUsername, mEmail, mPassword, mPasswordConfirmation, mFName, mLName, mPhone, mRole, designStyle);
    }
}
