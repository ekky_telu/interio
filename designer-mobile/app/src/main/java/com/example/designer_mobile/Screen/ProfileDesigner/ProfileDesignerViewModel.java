package com.example.designer_mobile.Screen.ProfileDesigner;

import android.util.Log;

import com.example.designer_mobile.Base.BaseErrorResponse;
import com.example.designer_mobile.Base.BaseResponse;
import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.Network.Service;
import com.example.designer_mobile.Screen.Login.LoginViewModel;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.example.designer_mobile.Utils.ErrorUtil;
import com.example.designer_mobile.Utils.StateViewModel;
import com.example.designer_mobile.Utils.UtilApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileDesignerViewModel extends StateViewModel<ProfileDesignerViewModel.State> {

    private static final String TAG = "ProfileDesignerVM";

    private Service mApiService = UtilApi.getApiService();
    private AppState appState = AppState.getInstance();

    @Override
    protected ProfileDesignerViewModel.State initState() {
        return new State();
    }

    public enum Status {
        INIT, SUCCESS, ERROR
    }

    public static class State {
        public boolean isLoading = false;
        ProfileDesignerViewModel.Status status = ProfileDesignerViewModel.Status.INIT;
        public String message = null;
        public Designer designer;
    }

    void getProfile() {
        state.isLoading = true;
        updateState();

        mApiService.getProfile().enqueue(new Callback<BaseResponse<Designer>>() {
            @Override
            public void onResponse(Call<BaseResponse<Designer>> call, Response<BaseResponse<Designer>> response) {
                state.isLoading = false;

                Designer designer = response.body().getData();
                appState.setDesigner(designer);
                state.status = Status.SUCCESS;
                state.designer = appState.getDesigner();

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Designer>> call, Throwable t) {
                state.isLoading = false;
                state.status = ProfileDesignerViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void getDesigner(String designerId) {
        state.isLoading = true;
        updateState();

        mApiService.getDesignerById(designerId).enqueue(new Callback<BaseResponse<Designer>>() {
            @Override
            public void onResponse(Call<BaseResponse<Designer>> call, Response<BaseResponse<Designer>> response) {
                state.isLoading = false;

                Designer designer = response.body().getData();
                state.status = Status.SUCCESS;
                state.designer = designer;

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Designer>> call, Throwable t) {
                state.isLoading = false;
                state.status = ProfileDesignerViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }
}
