package com.example.designer_mobile.Screen.Project.Update;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.designer_mobile.Adapter.CreatePortfolioPhotoAdapter;
import com.example.designer_mobile.Adapter.UpdatePortfolioPhotoAdapter;
import com.example.designer_mobile.Base.BottomSheetListView;
import com.example.designer_mobile.Model.PortfolioPhoto;
import com.example.designer_mobile.Model.Project;
import com.example.designer_mobile.Network.Client;
import com.example.designer_mobile.R;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;

public class UpdateProject extends AppCompatActivity {
    private static final String TAG = "UpdateProjectActivity";

    /* Define ViewModel */
    private UpdateProjectViewModel viewModel;

    /* Define Code as API Parameters */
    String mPropertyTypeCode;
    String mDesignStyleCode;
    String mUnitDurationCode;

    /* Define Context */
    private Context mContext;

    @BindView(R.id.toolbar)
    Toolbar mToolBar;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.title)
    EditText mTitle;
    @BindView(R.id.description)
    EditText mDescription;
    @BindView(R.id.location)
    EditText mLocation;
    @BindView(R.id.room_length)
    EditText mRoomLength;
    @BindView(R.id.room_width)
    EditText mRoomWidth;
    @BindView(R.id.room_height)
    EditText mRoomHeight;
    @BindView(R.id.duration)
    EditText mDuration;
    @BindView(R.id.property_type)
    EditText mPropertyType;
    @BindView(R.id.design_style)
    EditText mDesignStyle;
    @BindView(R.id.unit_duration)
    EditText mUnitDuration;
    @BindView(R.id.photos)
    RecyclerView mPhotos;

    String projectId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_project);

        ButterKnife.bind(this);
        mContext = this;

        projectId = getIntent().getStringExtra("project_id");

        setToolbar(mToolBar);
        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(UpdateProjectViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderResponse(state.status);
            renderProject(state.project);
        });

        viewModel.getProject(projectId);

        viewModel.getPropertyTypeList();
        viewModel.getDesignStyleList();
        viewModel.getUnitDurationList();

        mPropertyType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPropertyType(v);
            }
        });

        mDesignStyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDesignStyle(v);
            }
        });

        mUnitDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUnitDuration(v);
            }
        });
    }

    void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    void renderResponse(UpdateProjectViewModel.Status status) {
        if (status == UpdateProjectViewModel.Status.SUCCESS) {
            Toast.makeText(mContext, viewModel.getState().getValue().message,
                    Toast.LENGTH_SHORT).show();

            onBackPressed();
            finish();
        } else if (status == UpdateProjectViewModel.Status.ERROR) {
            Toast.makeText(mContext, viewModel.getState().getValue().message,
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void renderProject(Project project) {
        if (project != null) {
            mTitle.setText(project.getTitle());
            mLocation.setText(project.getLocation());
            mRoomLength.setText(project.getRoomLength());
            mRoomWidth.setText(project.getRoomWidth());
            mRoomHeight.setText(project.getRoomHeight());
            mDescription.setText(project.getDescription());
            mPropertyType.setText(project.getPropertyTypeDetail().getDescription());
            mDesignStyle.setText(project.getDesignStyleDetail().getDescription());

            mPropertyTypeCode = project.getPropertyType();
            mDesignStyleCode = project.getDesignStyle();
            Log.d(TAG, project.getDuration());
            Log.d(TAG, String.valueOf(Integer.parseInt(project.getDuration()) / 30));

            if (Integer.parseInt(project.getDuration()) / 365 >= 1) {
                mUnitDuration.setText("Tahun");
                mDuration.setText(String.valueOf(Integer.parseInt(project.getDuration()) / 365));
            } else if (Integer.parseInt(project.getDuration()) / 30 >= 1) {
                mUnitDuration.setText("Bulan");
                mDuration.setText(String.valueOf(Integer.parseInt(project.getDuration()) / 30));
            } else {
                mUnitDuration.setText("Hari");
                mDuration.setText(project.getDuration());
            }

            showPhotosToAdapter(project.getPhotos());
        }
    }

    private void showPhotosToAdapter(List<PortfolioPhoto> photo) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        UpdatePortfolioPhotoAdapter adapter = new UpdatePortfolioPhotoAdapter(mContext, photo, Client.PROJECT_IMAGE_URL);

        mPhotos.setLayoutManager(layoutManager);
        mPhotos.setAdapter(adapter);
    }

    private void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_dark);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void updateProject(View view) {
        if (!validateParameters()) {
            Toast.makeText(mContext, "Lengkapi Data", Toast.LENGTH_SHORT).show();
            return;
        }

        Map<String, String> parameters = new HashMap<>();

        String duration;
        if (mUnitDuration.getText().toString().equals("Tahun")) {
            duration = String.valueOf(Integer.parseInt(mDuration.getText().toString()) * 365);
        } else if (mUnitDuration.getText().toString().equals("Bulan")) {
            duration = String.valueOf(Integer.parseInt(mDuration.getText().toString()) * 30);
        } else {
            duration = mDuration.getText().toString();
        }

        parameters.put("project_id", projectId);
        parameters.put("title", mTitle.getText().toString());
        parameters.put("location", mLocation.getText().toString());
        parameters.put("room_length", mRoomLength.getText().toString());
        parameters.put("room_width", mRoomWidth.getText().toString());
        parameters.put("room_height", mRoomHeight.getText().toString());
        parameters.put("duration", duration);
        parameters.put("description", mDescription.getText().toString());

        parameters.put("property_type", mPropertyTypeCode);
        parameters.put("design_style", mDesignStyleCode);

        viewModel.updateProject(parameters);
    }

    private boolean validateParameters() {
        return !mTitle.getText().toString().equals("") && !mLocation.getText().toString().equals("") && !mRoomLength.getText().toString().equals("")
                && !mRoomWidth.getText().toString().equals("") && !mRoomHeight.getText().toString().equals("") && !mDuration.getText().toString().equals("")
                && !mUnitDuration.getText().toString().equals("") && !mDescription.getText().toString().equals("") && !mPropertyType.getText().toString().equals("")
                && !mDesignStyle.getText().toString().equals("");
    }

    public void showPropertyType(View view) {
        String title = "Tipe Properti";
        String[] codeList = viewModel.getState().getValue().propertyTypeCodeList;
        String[] nameList = viewModel.getState().getValue().propertyTypeNameList;

        BottomSheetDialog dialog = new BottomSheetDialog(mContext);
        dialog.setContentView(R.layout.bottom_sheet_view);

        BottomSheetListView listViewName = dialog.findViewById(R.id.list_item);
        TextView titleView = dialog.findViewById(R.id.list_title);

        titleView.setText(title);

        ArrayAdapter adapter = new ArrayAdapter(mContext, R.layout.item_bottom_sheet, nameList);
        listViewName.setAdapter(adapter);

        listViewName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mPropertyTypeCode = codeList[position];
                mPropertyType.setText(parent.getItemAtPosition(position).toString());

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void showDesignStyle(View view) {
        String title = "Gaya Desain";
        String[] codeList = viewModel.getState().getValue().designStyleCodeList;
        String[] nameList = viewModel.getState().getValue().designStyleNameList;

        BottomSheetDialog dialog = new BottomSheetDialog(mContext);
        dialog.setContentView(R.layout.bottom_sheet_view);

        BottomSheetListView listViewName = dialog.findViewById(R.id.list_item);
        TextView titleView = dialog.findViewById(R.id.list_title);

        titleView.setText(title);

        ArrayAdapter adapter = new ArrayAdapter(mContext, R.layout.item_bottom_sheet, nameList);
        listViewName.setAdapter(adapter);

        listViewName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDesignStyleCode = codeList[position];
                mDesignStyle.setText(parent.getItemAtPosition(position).toString());

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void showUnitDuration(View view) {
        String title = "Satuan Durasi";
        String[] codeList = viewModel.getState().getValue().unitDurationCodeList;
        String[] nameList = viewModel.getState().getValue().unitDurationNameList;

        BottomSheetDialog dialog = new BottomSheetDialog(mContext);
        dialog.setContentView(R.layout.bottom_sheet_view);

        BottomSheetListView listViewName = dialog.findViewById(R.id.list_item);
        TextView titleView = dialog.findViewById(R.id.list_title);

        titleView.setText(title);

        ArrayAdapter adapter = new ArrayAdapter(mContext, R.layout.item_bottom_sheet, nameList);
        listViewName.setAdapter(adapter);

        listViewName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mUnitDurationCode = codeList[position];
                mUnitDuration.setText(parent.getItemAtPosition(position).toString());

                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
