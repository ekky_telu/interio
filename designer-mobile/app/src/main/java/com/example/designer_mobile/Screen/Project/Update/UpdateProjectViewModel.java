package com.example.designer_mobile.Screen.Project.Update;

import com.example.designer_mobile.Base.BaseErrorResponse;
import com.example.designer_mobile.Base.BaseMasterCode;
import com.example.designer_mobile.Base.BaseResponse;
import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.Model.MasterCode;
import com.example.designer_mobile.Model.Project;
import com.example.designer_mobile.Network.Service;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.example.designer_mobile.Utils.ErrorUtil;
import com.example.designer_mobile.Utils.StateViewModel;
import com.example.designer_mobile.Utils.UtilApi;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProjectViewModel extends StateViewModel<UpdateProjectViewModel.State> {
    private static final String TAG = "UpdateProjectViewModel";

    Service mApiService = UtilApi.getApiService();

    AppState appState = AppState.getInstance();

    BaseMasterCode masterCode = new BaseMasterCode();

    @Override
    protected UpdateProjectViewModel.State initState() {
        return new UpdateProjectViewModel.State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        UpdateProjectViewModel.Status status = UpdateProjectViewModel.Status.INIT;
        String message = "";
        Project project;
        String[] propertyTypeCodeList;
        String[] propertyTypeNameList;
        String[] designStyleCodeList;
        String[] designStyleNameList;
        String[] unitDurationCodeList;
        String[] unitDurationNameList;
    }

    void getPropertyTypeList() {
        state.isLoading = true;
        state.status = UpdateProjectViewModel.Status.INIT;
        updateState();

        String parent_code = "PRT";
        mApiService.getMasterCodeByParent(parent_code).enqueue(new Callback<List<MasterCode>>() {
            @Override
            public void onResponse(Call<List<MasterCode>> call, Response<List<MasterCode>> response) {
                state.isLoading = false;
                List<MasterCode> data = response.body();

                state.propertyTypeCodeList = masterCode.getCodeList(data);
                state.propertyTypeNameList = masterCode.getNameList(data);

                updateState();
            }

            @Override
            public void onFailure(Call<List<MasterCode>> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    void getDesignStyleList() {
        state.isLoading = true;
        state.status = UpdateProjectViewModel.Status.INIT;
        updateState();

        String parent_code = "DSY";
        mApiService.getMasterCodeByParent(parent_code).enqueue(new Callback<List<MasterCode>>() {
            @Override
            public void onResponse(Call<List<MasterCode>> call, Response<List<MasterCode>> response) {
                state.isLoading = false;
                List<MasterCode> data = response.body();

                state.designStyleCodeList = masterCode.getCodeList(data);
                state.designStyleNameList = masterCode.getNameList(data);
                updateState();
            }

            @Override
            public void onFailure(Call<List<MasterCode>> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    void getUnitDurationList() {
        state.isLoading = true;
        state.status = UpdateProjectViewModel.Status.INIT;
        updateState();

        String parent_code = "UDT";
        mApiService.getMasterCodeByParent(parent_code).enqueue(new Callback<List<MasterCode>>() {
            @Override
            public void onResponse(Call<List<MasterCode>> call, Response<List<MasterCode>> response) {
                state.isLoading = false;
                List<MasterCode> data = response.body();

                state.unitDurationCodeList = masterCode.getCodeList(data);
                state.unitDurationNameList = masterCode.getNameList(data);
                updateState();
            }

            @Override
            public void onFailure(Call<List<MasterCode>> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    void getProject(String projectId) {
        state.isLoading = true;
        state.status = UpdateProjectViewModel.Status.INIT;
        updateState();

        mApiService.getProjectById(projectId).enqueue(new Callback<BaseResponse<Project>>() {
            @Override
            public void onResponse(Call<BaseResponse<Project>> call, Response<BaseResponse<Project>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.project = response.body().getData();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Project>> call, Throwable t) {

            }
        });

    }

    void updateProject(Map<String, String> parameters) {
        state.isLoading = true;
        state.status = UpdateProjectViewModel.Status.INIT;
        updateState();

        mApiService.updateProjectById(
                parameters.get("project_id"),
                appState.getDesigner().getId(),
                parameters.get("title"),
                parameters.get("location"),
                parameters.get("property_type"),
                parameters.get("room_length"),
                parameters.get("room_width"),
                parameters.get("room_height"),
                parameters.get("design_style"),
                parameters.get("duration"),
                parameters.get("description")
        ).enqueue(new Callback<BaseResponse<Project>>() {
            @Override
            public void onResponse(Call<BaseResponse<Project>> call, Response<BaseResponse<Project>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = Status.SUCCESS;
                    state.message = response.body().getMessage();
                    state.project = response.body().getData();

                    getDataUser();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Project>> call, Throwable t) {

            }
        });

    }

    private void getDataUser() {
        mApiService.getProfile().enqueue(new Callback<BaseResponse<Designer>>() {
            @Override
            public void onResponse(Call<BaseResponse<Designer>> call, Response<BaseResponse<Designer>> response) {
                Designer designer = response.body().getData();
                appState.setDesigner(designer);
            }

            @Override
            public void onFailure(Call<BaseResponse<Designer>> call, Throwable t) {
                state.isLoading = false;
                state.status = UpdateProjectViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }
}
