package com.example.designer_mobile.Network;

import android.util.Log;

import com.example.designer_mobile.SharedPreferences.AppState;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class Client {
    public static final String BASE_URL = "https://www.interio.id/api/";
    public static final String PROJECT_IMAGE_URL = "https://www.interio.id/images/projects/";
    public static final String GALLERY_IMAGE_URL = "https://www.interio.id/images/galleries/";
    public static final String RESULT_IMAGE_URL = "https://www.interio.id/images/result/";

    private static Retrofit retrofit = null;

    public static Retrofit getRetrofit() {
        return retrofit;
    }

    public static Retrofit getClient(String url) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addNetworkInterceptor(chain -> {
            Request original = chain.request();

            Request.Builder requestBuilder = original.newBuilder()
                    .addHeader("Accept", "application/json")
                    .method(original.method(), original.body());

            if (AppState.getInstance().hasToken()) {
                String token = AppState.getInstance().provideToken();
                requestBuilder.addHeader("Authorization", "Bearer " + token);
            }

            Request request = requestBuilder.build();

            return chain.proceed(request);
        }).addInterceptor(interceptor).build();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofit;
    }
}
