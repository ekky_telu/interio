package com.example.designer_mobile.Screen.Chat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.designer_mobile.Adapter.ChatAdapter;
import com.example.designer_mobile.R;
import com.example.designer_mobile.Screen.Home.Home;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatFragment extends Fragment {
    private String TAG = "ChatFragment";

    @BindView(R.id.chat_list)
    RecyclerView mChatList;
    @BindView(R.id.noData)
    RelativeLayout mNoData;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    private FirebaseDatabase firebase = FirebaseDatabase.getInstance();
    private DatabaseReference dbRef;

    private List<String> chatUid = new ArrayList<>();
    private List<String> name = new ArrayList<>();
    private List<String> lastMessage = new ArrayList<>();

    private String designerUid;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_chat_list, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        chatUid.clear();
        name.clear();
        lastMessage.clear();
        init();
    }

    private void init() {
        dbRef = firebase.getReference();
        designerUid = AppState.getInstance().getDesignerUid();

        getDesignerChats();
    }

    private void getDesignerChats() {
        setVisibilityProgressBar(true);
        dbRef.child("designers").child(designerUid).child("chatExistWith").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChildren()) {
                    mNoData.setVisibility(View.VISIBLE);
                    setVisibilityProgressBar(false);
                    return;
                }

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    chatUid.add(snapshot.getValue().toString());
                    getChatInfo(snapshot.getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getChatInfo(String chatUid) {
        /* Get Last Message and chat UID */
        dbRef.child("chats").child(chatUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lastMessage.add(dataSnapshot.child("lastMessage").getValue().toString());

                /* Get user UID by chat UID */
                getUserUid(dataSnapshot.getKey());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getUserUid(String chatUid) {
        dbRef.child("chatMembers").child(chatUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String userUid = null;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (!snapshot.getKey().equals(designerUid)) {
                        userUid = snapshot.getKey();
                        break;
                    }
                }

                setUserInfo(userUid);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setUserInfo(String userUid) {
        dbRef.child("users").child(userUid).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                name.add(dataSnapshot.getValue().toString());

                showChatList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void showChatList() {
        mChatList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        ChatAdapter adapter = new ChatAdapter(chatUid, name, lastMessage);
        mChatList.setAdapter(adapter);

        setVisibilityProgressBar(false);
    }

    private void setVisibilityProgressBar(boolean visibility) {
        mProgressBar.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility)
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
