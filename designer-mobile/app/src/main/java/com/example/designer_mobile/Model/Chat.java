package com.example.designer_mobile.Model;

public class Chat {

    private String lastMessage;
    private String lastTime;
    private String lastDate;
    private String lastSentBy;
    private String lastSentRole;

    public Chat(String lastMessage, String lastTime, String lastDate, String lastSentBy, String lastSentRole) {
        this.lastMessage = lastMessage;
        this.lastTime = lastTime;
        this.lastDate = lastDate;
        this.lastSentBy = lastSentBy;
        this.lastSentRole = lastSentRole;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastTime() {
        return lastTime;
    }

    public void setLastTime(String lastTime) {
        this.lastTime = lastTime;
    }

    public String getLastDate() {
        return lastDate;
    }

    public void setLastDate(String lastDate) {
        this.lastDate = lastDate;
    }

    public String getLastSentBy() {
        return lastSentBy;
    }

    public void setLastSentBy(String lastSentBy) {
        this.lastSentBy = lastSentBy;
    }

    public String getLastSentRole() {
        return lastSentRole;
    }

    public void setLastSentRole(String lastSentRole) {
        this.lastSentRole = lastSentRole;
    }
}
