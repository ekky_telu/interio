package com.example.designer_mobile.Adapter;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.designer_mobile.Model.PortfolioPhoto;
import com.example.designer_mobile.Network.Client;
import com.example.designer_mobile.R;

import java.util.List;

public class UpdatePortfolioPhotoAdapter extends RecyclerView.Adapter<UpdatePortfolioPhotoAdapter.ViewHolder> {
    static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mPhoto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mPhoto = itemView.findViewById(R.id.photo);
        }
    }

    private List<PortfolioPhoto> photos;
    private Context context;
    private String imageUrl;

    public UpdatePortfolioPhotoAdapter(Context context, List<PortfolioPhoto> photos, String imageUrl) {
        this.photos = photos;
        this.context = context;
        this.imageUrl = imageUrl;
    }

    @NonNull
    @Override
    public UpdatePortfolioPhotoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_create_portfolio_photo, parent, false);
        UpdatePortfolioPhotoAdapter.ViewHolder holder = new UpdatePortfolioPhotoAdapter.ViewHolder(layoutView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d("ADAPTER", imageUrl);
        PortfolioPhoto photo = photos.get(position);
        Glide.with(context)
                .load(imageUrl + photo.getImage())
                .placeholder(R.mipmap.ic_interio_logo)
                .into(holder.mPhoto);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }
}
