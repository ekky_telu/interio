package com.example.designer_mobile.Model;

import com.google.gson.annotations.SerializedName;

public class DesignStyle {
    @SerializedName("id")
    private String id;

    @SerializedName("designer_id")
    private String designer_id;

    @SerializedName("design_style")
    private String design_style;

    @SerializedName("design_style_detail")
    private MasterCode detail;

    public DesignStyle(String design_style) {
        this.design_style = design_style;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesignerId() {
        return designer_id;
    }

    public void setDesignerId(String designer_id) {
        this.designer_id = designer_id;
    }

    public String getDesignStyle() {
        return design_style;
    }

    public void setDesignStyle(String design_style) {
        this.design_style = design_style;
    }

    public MasterCode getDetail() {
        return detail;
    }

    public void setDetail(MasterCode detail) {
        this.detail = detail;
    }
}
