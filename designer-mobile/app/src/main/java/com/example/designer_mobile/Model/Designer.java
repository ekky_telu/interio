package com.example.designer_mobile.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Designer {
    @SerializedName("id")
    private String id;

    @SerializedName("username")
    private String username;

    @SerializedName("email")
    private String email;

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("last_name")
    private String last_name;

    @SerializedName("phone")
    private String phone;

    @SerializedName("province")
    private String province;

    @SerializedName("city")
    private String city;

    @SerializedName("address")
    private String address;

    @SerializedName("price")
    private String price;

    @SerializedName("status")
    private String status;

    @SerializedName("name")
    private String name;

    @SerializedName("main_design_style")
    private String main_design_style;

    @SerializedName("main_design_style_detail")
    private MasterCode main_design_style_detail;

    @SerializedName("design_style")
    private List<DesignStyle> designStyle;

    @SerializedName("projects_count")
    private String projects_count;

    @SerializedName("galleries_count")
    private String galleries_count;

    @SerializedName("projects")
    private List<Project> projects;

    @SerializedName("galleries")
    private List<Gallery> galleries;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return first_name;
    }

    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }

    public String getLastName() {
        return last_name;
    }

    public void setLastName(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMainDesignStyle() {
        return main_design_style;
    }

    public void setMainDesignStyle(String main_design_style) {
        this.main_design_style = main_design_style;
    }

    public MasterCode getMainDesignStyleDetail() {
        return main_design_style_detail;
    }

    public void setMainDesignStyleDetail(MasterCode main_design_style_detail) {
        this.main_design_style_detail = main_design_style_detail;
    }

    public List<DesignStyle> getDesignStyle() {
        return designStyle;
    }

    public void setDesignStyle(List<DesignStyle> designStyle) {
        this.designStyle = designStyle;
    }

    public String getProjectsCount() {
        return projects_count;
    }

    public void setProjectsCount(String projects_count) {
        this.projects_count = projects_count;
    }

    public String getGalleriesCount() {
        return galleries_count;
    }

    public void setGalleriesCount(String galleries_count) {
        this.galleries_count = galleries_count;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Gallery> getGalleries() {
        return galleries;
    }

    public void setGalleries(List<Gallery> galleries) {
        this.galleries = galleries;
    }

    public String getName() {
        String name = this.getFirstName() + " " + this.getLastName();
        return name;
    }
}
