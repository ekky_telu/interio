package com.example.designer_mobile.Adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.designer_mobile.Model.DesignStyle;
import com.example.designer_mobile.R;

import java.util.ArrayList;
import java.util.List;

public class DesignStyleRegisterAdapter extends RecyclerView.Adapter<DesignStyleRegisterAdapter.ViewHolder> {

    class ViewHolder extends RecyclerView.ViewHolder {
        private Button mDesignStyle;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            mDesignStyle = itemView.findViewById(R.id.design_style);
        }
    }

    private String[] designStyleCode;
    private String[] designStyleName;
    private List<String> designStyleChosen = new ArrayList<>();
    private boolean isAddToMainDesign;
    private List<String> designStyleCodeToShow = new ArrayList<>();
    private List<String> designStyleNameToShow = new ArrayList<>();
    private List<DesignStyle> existingDesignStyles = new ArrayList<>();

    public DesignStyleRegisterAdapter(String[] designStyleCode, String[] designStyleName) {
        this.designStyleCode = designStyleCode;
        this.designStyleName = designStyleName;
    }

    public DesignStyleRegisterAdapter(String[] designStyleCode, String[] designStyleName, boolean isAddToMainDesign, List<DesignStyle> existingDesignStyles) {
        this.designStyleCode = designStyleCode;
        this.designStyleName = designStyleName;
        this.isAddToMainDesign = isAddToMainDesign;
        this.existingDesignStyles = existingDesignStyles;
    }

    @NonNull
    @Override
    public DesignStyleRegisterAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_design_style_register, parent, false);
        ViewHolder holder = new ViewHolder(layoutView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DesignStyleRegisterAdapter.ViewHolder holder, int position) {
        holder.mDesignStyle.setText(designStyleName[position]);

        for (DesignStyle designStyle : existingDesignStyles) {
            if (designStyle.getDesignStyle().equals(designStyleCode[position])) {
                setButtonActive(holder, position);
                setDesignStyleCodeToShow(designStyleCode[position]);
                setDesignStyleNameToShow(designStyleName[position]);
                break;
            }
        }

        holder.mDesignStyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (designStyleChosen.contains(designStyleCode[position])) {
                    setButtonNonActive(holder, position);
                    if (isAddToMainDesign) {
                        removeDesignStyleCodeToShow(designStyleCode[position]);
                        removeDesignStyleNameToShow(designStyleName[position]);
                    }
                } else {
                    setButtonActive(holder, position);
                    if (isAddToMainDesign) {
                        setDesignStyleCodeToShow(designStyleCode[position]);
                        setDesignStyleNameToShow(designStyleName[position]);
                    }
                }
            }
        });
    }

    private void setButtonActive(@NonNull DesignStyleRegisterAdapter.ViewHolder holder, int position) {
        holder.mDesignStyle.setBackgroundResource(R.drawable.bg_design_style_active_button);
        designStyleChosen.add(designStyleCode[position]);
    }

    private void setButtonNonActive(@NonNull DesignStyleRegisterAdapter.ViewHolder holder, int position) {
        holder.mDesignStyle.setBackgroundResource(R.drawable.bg_design_style_button);
        designStyleChosen.remove(designStyleCode[position]);
    }

    public List<String> getItems() {
        return designStyleChosen;
    }

    public List<String> getDesignStyleCodeToShow() {
        return designStyleCodeToShow;
    }

    public void setDesignStyleCodeToShow(String designStyleCodeToShow) {
        this.designStyleCodeToShow.add(designStyleCodeToShow);
    }

    public void removeDesignStyleCodeToShow(String designStyleCodeToShow) {
        this.designStyleCodeToShow.remove(designStyleCodeToShow);
    }

    public List<String> getDesignStyleNameToShow() {
        return designStyleNameToShow;
    }

    public void setDesignStyleNameToShow(String designStyleNameToShow) {
        this.designStyleNameToShow.add(designStyleNameToShow);
    }

    public void removeDesignStyleNameToShow(String designStyleNameToShow) {
        this.designStyleNameToShow.remove(designStyleNameToShow);
    }

    @Override
    public int getItemCount() {
        return designStyleCode.length;
    }
}
