package com.example.designer_mobile.SharedPreferences;

public interface TokenProvider {

    void setToken(String token);

    boolean hasToken();

    String provideToken();

    void removeToken();

}
