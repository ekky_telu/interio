package com.example.designer_mobile.Screen.ProfileDesigner;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.example.designer_mobile.Adapter.PagerAdapter;
import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.R;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileFragment extends Fragment {
    private static final String TAG = "ProfileFragmentActivity";

    /* Define ViewModel */
    private ProfileDesignerViewModel viewModel;

    private String id;

    /* Define Component */
    @BindView(R.id.nameProf)
    TextView mNameDs;
    @BindView(R.id.usernameProf)
    TextView mUsername;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_profile_designer, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        init();

//        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
//        // Set the text for each tab.
//        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_label1));
//        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_label2));
//
//        // Set the tabs to fill the entire layout.
//        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//
//        // Use PagerAdapter to manage page views in fragments.
//        // Each page is represented by its own fragment.
//        final ViewPager viewPager = view.findViewById(R.id.pager);
//        final androidx.viewpager.widget.PagerAdapter adapter = new PagerAdapter(getFragmentManager(), tabLayout.getTabCount());
//        viewPager.setAdapter(adapter);
//        // Setting a listener for clicks.
//        Log.d("Ini test", "Apakah ini adaa 1");
//        viewPager.addOnPageChangeListener(new
//                TabLayout.TabLayoutOnPageChangeListener(tabLayout));
//
//        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                viewPager.setCurrentItem(tab.getPosition());
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//            }
//        });
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(ProfileDesignerViewModel.class);
        viewModel.getState().observe(getActivity(), state -> {
            renderLoading(state.isLoading);
            renderDataProfile(state.designer, state.message);
        });
    }

    private void renderLoading(boolean isLoading) {
       mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderDataProfile(Designer designer, String message) {
        if (message != null) {
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            return;
        }

        if (designer != null) {
            mNameDs.setText(designer.getUsername());
            mUsername.setText(designer.getEmail());

            Log.d("Ini test", "Apakah ini adaa 4");
        }
    }
}
