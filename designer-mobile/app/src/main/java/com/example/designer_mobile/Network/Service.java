package com.example.designer_mobile.Network;

import com.example.designer_mobile.Base.BaseResponse;
import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.Model.Gallery;
import com.example.designer_mobile.Model.Login;
import com.example.designer_mobile.Model.MasterCode;
import com.example.designer_mobile.Model.Project;
import com.example.designer_mobile.Model.Transaction;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Service {

    @GET("designer/project/{id}")
    Call<BaseResponse<Project>> getProjectById(
            @Path("id") String id
    );

    @GET("designer/gallery/{id}")
    Call<BaseResponse<Gallery>> getGalleryById(
            @Path("id") String id
    );

    @Multipart
    @POST("designer/project")
    Call<BaseResponse<Project>> storeProject(
            @Part("designer_id") RequestBody designer_id,
            @Part("title") RequestBody title,
            @Part("location") RequestBody location,
            @Part("property_type") RequestBody property_type,
            @Part("room_length") RequestBody room_length,
            @Part("room_width") RequestBody room_width,
            @Part("room_height") RequestBody room_height,
            @Part("design_style") RequestBody design_style,
            @Part("duration") RequestBody duration,
            @Part("description") RequestBody description,
            @Part MultipartBody.Part[] photos
    );

    @GET("mastercode")
    Call<List<MasterCode>> getMasterCodeByParent(
            @Query("parent_code") String parent_code
    );

    @FormUrlEncoded
    @POST("register")
    Call<BaseResponse<Designer>> register(
            @Field("username") String username,
            @Field("email") String email,
            @Field("password") String password,
            @Field("password_confirmation") String password_confirmation,
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("phone") String phone,
            @Field("role") String role,
            @Field("design_style[]") String[] design_style
    );

    @FormUrlEncoded
    @POST("login")
    Call<BaseResponse<Login>> login(
            @Field("username") String username,
            @Field("password") String password,
            @Field("role") String role
    );

    @GET("designer/profile")
    Call<BaseResponse<Designer>> getProfile();

    @FormUrlEncoded
    @PUT("designer/profile/update")
    Call<BaseResponse<Designer>> updateProfile(
            @Field("username") String username,
            @Field("email") String email,
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("phone") String phone,
            @Field("role") String role,
            @Field("province") String province,
            @Field("city") String city,
            @Field("address") String address
    );

    @GET("designer/transaction")
    Call<BaseResponse<List<Transaction>>> listPesanan(
            @Query("designer_id") String designer_id
    );


    @FormUrlEncoded
    @POST("designer/profile/password")
    Call<BaseResponse> updatepassword(
            @Field("old_password") String old_password,
            @Field("password") String password,
            @Field("password_confirmation") String password_confirmation,
            @Field("role") String role
    );

    @GET("designer/designer/{id}")
    Call<BaseResponse<Designer>> getDesignerById(
            @Path("id") String id
    );

    @FormUrlEncoded
    @POST("designer/profile/price")
    Call<BaseResponse<Designer>> updatePrice(
            @Field("price") String price
    );

    @FormUrlEncoded
    @POST("designer/profile/design-style")
    Call<BaseResponse<Designer>> updateDesignStyle(
            @Field("main_design_style") String main_design_style,
            @Field("design_style[]") String[] design_style
    );

    @Multipart
    @POST("designer/gallery")
    Call<BaseResponse<Gallery>> storeGallery(
            @Part("designer_id") RequestBody designer_id,
            @Part("title") RequestBody title,
            @Part("property_type") RequestBody property_type,
            @Part("room_length") RequestBody room_length,
            @Part("room_width") RequestBody room_width,
            @Part("room_height") RequestBody room_height,
            @Part("design_style") RequestBody design_style,
            @Part("description") RequestBody description,
            @Part MultipartBody.Part[] photos
    );

    @GET("designer/logout")
    Call<BaseResponse> logout();

    @GET("designer/designer")
    Call<BaseResponse<List<Designer>>> getAllDesigners();

    @GET("designer/project")
    Call<BaseResponse<List<Project>>> getAllProjects();

    @GET("designer/transaction/{id}")
    Call<BaseResponse<Transaction>> getTransactionById(
            @Path("id") String id
    );

    @PUT("designer/transaction/accept/{id}")
    Call<BaseResponse> acceptTransaction(
            @Path("id") String id
    );

    @PUT("designer/transaction/cancel/{id}")
    Call<BaseResponse> cancelTransaction(
            @Path("id") String id
    );

    @Multipart
    @POST("designer/transaction/send-design")
    Call<BaseResponse> sendDesign(
            @Part("transaction_id") RequestBody transaction_id,
            @Part("result_description") RequestBody result_description,
            @Part MultipartBody.Part[] photos
    );

    @FormUrlEncoded
    @POST("check-username")
    Call<BaseResponse> checkUsername(
            @Field("role") String role,
            @Field("username") String username
    );

    @FormUrlEncoded
    @POST("check-email")
    Call<BaseResponse> checkEmail(
            @Field("role") String role,
            @Field("email") String email
    );

    @FormUrlEncoded
    @PUT("designer/project/{id}")
    Call<BaseResponse<Project>> updateProjectById(
            @Path("id") String id,
            @Field("designer_id") String designer_id,
            @Field("title") String title,
            @Field("location") String location,
            @Field("property_type") String property_type,
            @Field("room_length") String room_length,
            @Field("room_width") String room_width,
            @Field("room_height") String room_height,
            @Field("design_style") String design_style,
            @Field("duration") String duration,
            @Field("description") String description
    );

    @FormUrlEncoded
    @PUT("designer/gallery/{id}")
    Call<BaseResponse<Gallery>> updateGalleryById(
            @Path("id") String id,
            @Field("designer_id") String designer_id,
            @Field("title") String title,
            @Field("property_type") String property_type,
            @Field("room_length") String room_length,
            @Field("room_width") String room_width,
            @Field("room_height") String room_height,
            @Field("design_style") String design_style,
            @Field("description") String description
    );
}
