package com.example.designer_mobile.Screen.ProfileDesigner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.designer_mobile.Adapter.PagerAdapter;
import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.R;
import com.example.designer_mobile.Screen.Gallery.Create.CreateGallery;
import com.example.designer_mobile.Screen.Project.Create.CreateProject;
import com.example.designer_mobile.Screen.SettingProfile.ActivitySettingProfile;
import com.example.designer_mobile.SharedPreferences.AppState;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileDesigner extends AppCompatActivity {

    private static final String TAG = "ProfileDesignerActivity";

    /* Define ViewModel */
    private ProfileDesignerViewModel viewModel;

    private int selectedTab;

    /* Define Component */
    @BindView(R.id.nameProf)
    TextView mNameDs;

    @BindView(R.id.usernameProf)
    TextView mUsername;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.pager)
    ViewPager viewPager;

    @BindView(R.id.gayaDesain)
    TextView mDesignStyle;

    @BindView(R.id.projectCount)
    TextView mProjectCount;

    @BindView(R.id.pengaturanProf)
    Button mBtnSetting;

    @BindView(R.id.fab_btn)
    FloatingActionButton mBtnAdd;

    String designerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_designer);

        ButterKnife.bind(this);
        designerId = getIntent().getStringExtra("designer_id");

        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_label1));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_label2));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab = tab.getPosition();
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(ProfileDesignerViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderDataProfile(state.designer, state.message);
        });

        if (designerId != null) {
            viewModel.getDesigner(designerId);
        } else {
            viewModel.getProfile();
        }
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderDataProfile(Designer designer, String message) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            return;
        }

        if (viewModel.getState().getValue().status == ProfileDesignerViewModel.Status.SUCCESS) {
            if (!designer.getId().equals(AppState.getInstance().getDesigner().getId())) {
                mBtnSetting.setVisibility(View.GONE);
                mBtnAdd.setVisibility(View.GONE);
            } else {
                mBtnSetting.setVisibility(View.VISIBLE);
                mBtnAdd.setVisibility(View.VISIBLE);
            }

            mNameDs.setText(designer.getName());
            mUsername.setText("@" + designer.getUsername());
            mDesignStyle.setText((designer.getMainDesignStyle() != null) ? designer.getMainDesignStyleDetail().getDescription() : "-");
            mProjectCount.setText(designer.getProjectsCount());



            PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), designer);
            viewPager.setAdapter(adapter);


        }
    }

    public void back(View view) {
        onBackPressed();
    }

    public void add(View view) {
        if (selectedTab == 0) {
            Intent addGallery = new Intent(this, CreateGallery.class);
            startActivity(addGallery);
        } else {
            Intent addProject = new Intent(this, CreateProject.class);
            startActivity(addProject);
        }
    }

    public void toSetting(View view) {
        Intent settingProfile = new Intent(this, ActivitySettingProfile.class);
        startActivity(settingProfile);
    }
}
