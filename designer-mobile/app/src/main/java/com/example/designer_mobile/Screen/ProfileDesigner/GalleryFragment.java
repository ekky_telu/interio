package com.example.designer_mobile.Screen.ProfileDesigner;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.designer_mobile.Adapter.GalleryAdapter;
import com.example.designer_mobile.Base.SpacesItemDecoration;
import com.example.designer_mobile.Model.Designer;
import com.example.designer_mobile.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class GalleryFragment extends Fragment {
    @BindView(R.id.galleryList)
    RecyclerView galleryList;
    @BindView(R.id.noData)
    TextView noData;

    private Designer designer;

    public GalleryFragment(Designer designer) {
        this.designer = designer;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (designer.getGalleriesCount().equals("0")) {
            noData.setVisibility(View.VISIBLE);
        } else {
            GalleryAdapter adapter = new GalleryAdapter(getContext(), designer.getGalleries());
            galleryList.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
            galleryList.setAdapter(adapter);

            SpacesItemDecoration decoration = new SpacesItemDecoration(15);
            galleryList.addItemDecoration(decoration);
        }
    }
}
