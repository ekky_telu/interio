package com.example.client_mobile.Screen.CatalogDesigner;

import android.util.Log;

import com.example.client_mobile.Base.BaseErrorResponse;
import com.example.client_mobile.Base.BaseResponse;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.Network.Service;
import com.example.client_mobile.Screen.CatalogDesigner.CatalogDesignerViewModel;
import com.example.client_mobile.SharedPreferences.AppState;
import com.example.client_mobile.Utils.ErrorUtil;
import com.example.client_mobile.Utils.StateViewModel;
import com.example.client_mobile.Utils.UtilApi;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CatalogDesignerViewModel extends StateViewModel<CatalogDesignerViewModel.State> {
    private static final String TAG = "CatalogDesignerVM";

    private Service mApiService = UtilApi.getApiService();

    @Override
    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    public static class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message = "";
        List<Designer> designer;
    }

    void getCatalog() {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();

        mApiService.getAllDesigners().enqueue(new Callback<BaseResponse<List<Designer>>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<List<Designer>>> call, @NotNull Response<BaseResponse<List<Designer>>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = Status.SUCCESS;
                    state.designer = response.body().getData();
                    Log.d(TAG, state.designer.get(0).getId());
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = Status.ERROR;
                    state.message = errorResponse.getMessage();

                    Log.d("message response", state.message);
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<List<Designer>>> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = Status.ERROR;
                state.message = t.getMessage();
                Log.d("message failure", state.message);

                updateState();
            }
        });
    }
}
