package com.example.client_mobile.SharedPreferences;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.client_mobile.Model.User;
import com.google.gson.Gson;

public class AppState implements TokenProvider {
    private static AppState instance;
    private static final String TOKEN_KEY = "access_token";
    private static final String IS_LOGGED_IN = "is_logged_in";
    private static final String CURRENT_USER = "current_user";
    private static final String USER_UID = "user_uid";
    private SharedPreferences pref;

    public static AppState getInstance() {
        if (instance == null) {
            synchronized (AppState.class) {
                if (instance == null) {
                    instance = new AppState();
                }
            }
        }
        return instance;
    }

    public void initSharedPrefs(Application application) {
        pref = application.getSharedPreferences("designer_mobile", Context.MODE_PRIVATE);
    }

    @Override
    public void setToken(String token) {
        pref.edit().putString(TOKEN_KEY, token).apply();
    }

    @Override
    public boolean hasToken() {
        return pref.contains(TOKEN_KEY);
    }

    @Override
    public String provideToken() {
        return pref.getString(TOKEN_KEY, null);
    }

    @Override
    public void removeToken() {
        pref.edit().remove(TOKEN_KEY).apply();
    }

    public void setUserUid(String uid) {
//        Gson gson = new Gson();
        pref.edit().putString(USER_UID, uid).apply();
    }

    public String getUserUid() {
        return pref.getString(USER_UID, null);
    }

    public void setUser(User user) {
        Gson gson = new Gson();
        pref.edit().putString(CURRENT_USER, gson.toJson(user)).apply();
    }

    public User getUser() {
        Gson gson = new Gson();
        String userJson = pref.getString(CURRENT_USER, null);

        if (userJson == null) {
            return null;
        } else {
            return gson.fromJson(userJson, User.class);
        }
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGGED_IN, false);
    }

    public void setIsLoggedIn(Boolean status) {
        pref.edit().putBoolean(IS_LOGGED_IN, status).apply();
    }

    public void logout() {
        removeToken();
        setIsLoggedIn(false);
    }
}
