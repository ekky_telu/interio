package com.example.client_mobile.Screen.ListPesanan;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.client_mobile.Adapter.ListPesananAdapter;
import com.example.client_mobile.Model.Transaction;
import com.example.client_mobile.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListPesanan extends AppCompatActivity {
    private static final String TAG = "ListPesananActivity";

    //vars
    private ArrayList<String> mOrder = new ArrayList<>();
    private ArrayList<String> mStatus = new ArrayList<>();

    private String user_id;
    private Context mContext;
    private ListPesananViewModel viewModel;

    /* Define Component */

    @BindView(R.id.order_number)
    TextView mOrderNum;

    @BindView(R.id.status)
    TextView mStatusOrd;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.noData)
    RelativeLayout mNoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_pesanan);

        ButterKnife.bind(this);
        init();
    }

    private void init() {
        user_id = getIntent().getStringExtra("user_id");
        viewModel = new ViewModelProvider(this).get(ListPesananViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            initRecyclerView(state.listPesanan);
        });

        viewModel.getListPesanan(user_id);
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void initRecyclerView(List<Transaction> transactions) {
        if (transactions.size() == 0) {
            mNoData.setVisibility(View.VISIBLE);
            return;
        }

        RecyclerView recyclerView = findViewById(R.id.recyclerViewPesanan);
        ListPesananAdapter adapter = new ListPesananAdapter(this, transactions);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
    }
}
