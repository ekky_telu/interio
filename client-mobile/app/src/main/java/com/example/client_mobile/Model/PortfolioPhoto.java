package com.example.client_mobile.Model;

import com.google.gson.annotations.SerializedName;

public class PortfolioPhoto {
    @SerializedName("id")
    private String id;

    @SerializedName("project_id")
    private String project_id;

    @SerializedName("gallery_id")
    private String gallery_id;

    @SerializedName("transaction_detail_id")
    private String transaction_detail_id;

    @SerializedName("title")
    private String title;

    @SerializedName("image")
    private String image;

    @SerializedName("description")
    private String description;

    @SerializedName("main_image")
    private String main_image;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectId() {
        return project_id;
    }

    public void setProjectId(String project_id) {
        this.project_id = project_id;
    }

    public String getGalleryId() {
        return gallery_id;
    }

    public void setGalleryId(String gallery_id) {
        this.gallery_id = gallery_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMainImage() {
        return main_image;
    }

    public void setMainImage(String main_image) {
        this.main_image = main_image;
    }

    public String getTransactionDetailId() {
        return transaction_detail_id;
    }

    public void setTransaction_detail_id(String transactionDetailId) {
        this.transaction_detail_id = transaction_detail_id;
    }
}
