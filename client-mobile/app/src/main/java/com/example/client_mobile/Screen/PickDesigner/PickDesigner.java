package com.example.client_mobile.Screen.PickDesigner;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.client_mobile.Adapter.PickDesignerPortfolioAdapter;
import com.example.client_mobile.Adapter.PortfolioPhotoAdapter;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.Model.Gallery;
import com.example.client_mobile.Model.PortfolioPhoto;
import com.example.client_mobile.Model.Project;
import com.example.client_mobile.R;
import com.example.client_mobile.Screen.PaymentDetails.PaymentDetails;
import com.example.client_mobile.Screen.PickDesigner.PickDesignerViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PickDesigner extends AppCompatActivity {
    private static final String TAG = "PickDesignerActivity";

    /* Define ViewModel */
    private PickDesignerViewModel viewModel;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.designer_name)
    TextView mDesignerName;
    @BindView(R.id.designer_price)
    TextView mDesignerPrice;
    @BindView(R.id.designer_portfolio)
    RecyclerView mDesignerPortfolio;

    String mDescription;
    String mPropertyType;
    int mRoomLength;
    int mRoomWidth;
    int mRoomHeight;
    String mDesignStyle;
    String mDesignerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_designer);

        ButterKnife.bind(this);

        mDescription = getIntent().getStringExtra("description");
        mPropertyType = getIntent().getStringExtra("property_type");
        mRoomLength = getIntent().getIntExtra("room_length", 0);
        mRoomWidth = getIntent().getIntExtra("room_width", 0);
        mRoomHeight = getIntent().getIntExtra("room_height", 0);
        mDesignStyle = getIntent().getStringExtra("design_style");
        mDesignerId = getIntent().getStringExtra("designer_id");

        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(PickDesignerViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderResponse(state.status);
            renderDesigner(state.designer);
        });

        viewModel.getDesigner(mDesignerId);
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderResponse(PickDesignerViewModel.Status status) {
        if (status == PickDesignerViewModel.Status.SUCCESS) {
            Toast.makeText(this, viewModel.getState().getValue().message, Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, PaymentDetails.class);
            intent.putExtra("transaction_id", viewModel.getState().getValue().transaction.getId());
            startActivity(intent);
            finish();
        } else if (status == PickDesignerViewModel.Status.ERROR) {
            Toast.makeText(this, viewModel.getState().getValue().message, Toast.LENGTH_SHORT).show();
        }
    }

    private void renderDesigner(Designer designer) {
        if (designer != null) {
            mDesignerName.setText(designer.getName());
            mDesignerPrice.setText(designer.getPrice());

            List<PortfolioPhoto> portfolioPhotos = new ArrayList<>();
            for (Project project : designer.getProjects()) {
                portfolioPhotos.addAll(project.getPhotos());
            }

            for (Gallery gallery : designer.getGalleries()) {
                portfolioPhotos.addAll(gallery.getPhotos());
            }

            PickDesignerPortfolioAdapter adapter = new PickDesignerPortfolioAdapter(portfolioPhotos, this);
            mDesignerPortfolio.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
            mDesignerPortfolio.setAdapter(adapter);
        }
    }

    public void createOrder(View view) {
        viewModel.createOrder(
                mDescription,
                mPropertyType,
                mRoomLength,
                mRoomWidth,
                mRoomHeight,
                mDesignStyle,
                mDesignerId);
    }

    public void back(View view) {
        onBackPressed();
    }
}
