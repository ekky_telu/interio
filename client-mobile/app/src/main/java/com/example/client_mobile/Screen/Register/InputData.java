package com.example.client_mobile.Screen.Register;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.client_mobile.R;
import com.example.client_mobile.Screen.Login.Login;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InputData extends AppCompatActivity {
    String TAG = "InputDataActivity";

    String mUsername, mPassword, mPasswordConfirmation;

    /* Define ViewModel */
    private RegisterViewModel viewModel;

    String mRole = "client";

    @BindView(R.id.fname)
    EditText mFName;
    @BindView(R.id.lname)
    EditText mLName;
    @BindView(R.id.email)
    EditText mEmail;
    @BindView(R.id.nohp)
    EditText mPhone;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register3);
        ButterKnife.bind(this);

        mUsername = getIntent().getStringExtra("username");
        mPassword = getIntent().getStringExtra("password");
        mPasswordConfirmation = getIntent().getStringExtra("password_confirmation");

        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(RegisterViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderResponse(state.isError, state.status, state.message);
        });
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    void renderResponse(boolean isError, RegisterViewModel.Status status, String message) {
        if (isError) {
            if (!viewModel.getState().getValue().message.equals("") && viewModel.getState().getValue().status == RegisterViewModel.Status.ERROR) {
                Toast.makeText(this, viewModel.getState().getValue().message, Toast.LENGTH_SHORT).show();
            }
            return;
        }

        if (!message.equals("")) {
            if (status == RegisterViewModel.Status.SUCCESS) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

                Intent login = new Intent(this, Login.class);
                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(login);
                finish();
            } else if (status == RegisterViewModel.Status.ERROR) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void register(View view) {
        viewModel.checkEmail(mEmail.getText().toString(), "client");

        viewModel.register(
                mUsername,
                mEmail.getText().toString(),
                mPassword,
                mPasswordConfirmation,
                mFName.getText().toString(),
                mLName.getText().toString(),
                mPhone.getText().toString(),
                mRole);
    }

    public void showLogin(View view) {
        Intent intent = new Intent(this, Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
