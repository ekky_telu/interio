package com.example.client_mobile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.client_mobile.Model.PortfolioPhoto;
import com.example.client_mobile.Network.Client;
import com.example.client_mobile.R;

import java.util.List;

public class PickDesignerPortfolioAdapter extends RecyclerView.Adapter<PickDesignerPortfolioAdapter.ViewHolder> {

    private List<PortfolioPhoto> portfolioPhotos;
    private Context context;

    public PickDesignerPortfolioAdapter(List<PortfolioPhoto> portfolioPhotos, Context context) {
        this.portfolioPhotos = portfolioPhotos;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pick_designer_portfolio, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PortfolioPhoto portfolioPhoto = portfolioPhotos.get(position);
        String imageUrl;
        if (portfolioPhoto.getGalleryId() != null) {
            imageUrl = Client.GALLERY_IMAGE_URL;
        } else {
            imageUrl = Client.PROJECT_IMAGE_URL;
        }

        Glide.with(context)
                .load(imageUrl + portfolioPhoto.getImage())
                .into(holder.mPhoto);
    }

    @Override
    public int getItemCount() {
        return portfolioPhotos.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mPhoto;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            mPhoto = itemView.findViewById(R.id.photo);
        }
    }
}
