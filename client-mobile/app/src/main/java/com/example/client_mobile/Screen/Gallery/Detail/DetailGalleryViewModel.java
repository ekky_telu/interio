package com.example.client_mobile.Screen.Gallery.Detail;

import com.example.client_mobile.Base.BaseErrorResponse;
import com.example.client_mobile.Base.BaseResponse;
import com.example.client_mobile.Model.Gallery;
import com.example.client_mobile.Network.Service;
import com.example.client_mobile.SharedPreferences.AppState;
import com.example.client_mobile.Utils.ErrorUtil;
import com.example.client_mobile.Utils.StateViewModel;
import com.example.client_mobile.Utils.UtilApi;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailGalleryViewModel extends StateViewModel<DetailGalleryViewModel.State> {
    private static final String TAG = "DetailGalleryViewModel";

    Service mApiService = UtilApi.getApiService();

    AppState appState = AppState.getInstance();

    @Override
    protected DetailGalleryViewModel.State initState() {
        return new DetailGalleryViewModel.State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message = null;
        Gallery gallery = new Gallery();
    }

    void getData(String id) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();

        mApiService.getGalleryById(id).enqueue(new Callback<BaseResponse<Gallery>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Gallery>> call, @NotNull Response<BaseResponse<Gallery>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = Status.SUCCESS;
                    state.gallery = response.body().getData();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<Gallery>> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }
}
