package com.example.client_mobile.Utils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public abstract class StateViewModel<S> extends ViewModel {

    protected abstract S initState();

    protected S state = initState();
    private MutableLiveData<S> liveState;

    public LiveData<S> getState() {
        if (liveState == null) {
            liveState = new MutableLiveData<>();
            liveState.setValue(state);
        }

        return liveState;
    }

    protected void updateState() {
        liveState.setValue(state);
    }
}
