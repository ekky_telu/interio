package com.example.client_mobile.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.client_mobile.Model.Transaction;
import com.example.client_mobile.R;
import com.example.client_mobile.Screen.DetailPesanan.DetailPesanan;
import com.example.client_mobile.Screen.PenerimaanKonsepDesain.PenerimaanKonsepDesain;

import java.util.List;

public class ListPesananAdapter extends RecyclerView.Adapter<ListPesananAdapter.ViewHolder> {
    private static final String TAG = "ListPesananAdapter";

    private List<Transaction> transactions;
    private Context mContext;

    public ListPesananAdapter(Context mContext, List<Transaction> transactions) {
        this.transactions = transactions;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_pesanan, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Transaction transaction = transactions.get(position);
        holder.orderNumber.setText(transaction.getDetail().get(0).getPropertyTypeDetail().getDescription() + " - " + transaction.getDesigner().getFirstName());
        holder.status.setText(transaction.getStatusDetail().getDescription());

        holder.itemListLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (transaction.getStatusDetail().getDescription().equals("Desain Sudah Dikirim")
                        || transaction.getStatusDetail().getDescription().equals("Selesai")) {
                    Intent detail = new Intent(v.getContext(), PenerimaanKonsepDesain.class);
                    detail.putExtra("transaction_id", transaction.getId());
                    v.getContext().startActivity(detail);
                } else {
                    Intent detail = new Intent(v.getContext(), DetailPesanan.class);
                    detail.putExtra("transaction_id", transaction.getId());
                    v.getContext().startActivity(detail);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView orderNumber;
        TextView status;
        LinearLayout itemListLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            orderNumber = itemView.findViewById(R.id.order_number);
            status = itemView.findViewById(R.id.status);
            itemListLayout = itemView.findViewById(R.id.item_list_layout);


        }

    }
}
