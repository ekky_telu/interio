package com.example.client_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.client_mobile.Screen.Home.Home;
import com.example.client_mobile.Screen.Login.Login;
import com.example.client_mobile.SharedPreferences.AppState;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
                if (AppState.getInstance().isLoggedIn()) {
                    startActivity(new Intent(SplashScreen.this, Home.class));
                } else {
                    startActivity(new Intent(SplashScreen.this, Login.class));
                }
            }
        }, 2000);
    }

}
