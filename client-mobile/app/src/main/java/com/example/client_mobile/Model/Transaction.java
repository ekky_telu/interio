package com.example.client_mobile.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Transaction {
    @SerializedName("id")
    private String id;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("user")
    private User user;

    @SerializedName("designer_id")
    private String designer_id;

    @SerializedName("designer")
    private Designer designer;

    @SerializedName("description")
    private String description;

    @SerializedName("location")
    private String location;

    @SerializedName("estimated_duration")
    private int estimated_duration;

    @SerializedName("duration")
    private int duration;

    @SerializedName("total_room")
    private int total_room;

    @SerializedName("total_size")
    private String total_size;

    @SerializedName("total_price")
    private String total_price;

    @SerializedName("status")
    private String status;

    @SerializedName("status_detail")
    private MasterCode status_detail;

    @SerializedName("payment_method")
    private String payment_method;

    @SerializedName("payment_account_no")
    private String payment_account_no;

    @SerializedName("payment_account_name")
    private String payment_account_name;

    @SerializedName("payment_amount")
    private String payment_amount;

    @SerializedName("payment_status")
    private String payment_status;

    @SerializedName("progress_count")
    private String progress_count;

    @SerializedName("detail")
    private List<TransactionDetail> detail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return user_id;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDesignerId() {
        return designer_id;
    }

    public void setDesignerId(String designer_id) {
        this.designer_id = designer_id;
    }

    public Designer getDesigner() {
        return designer;
    }

    public void setDesigner(Designer designer) {
        this.designer = designer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getEstimatedDuration() {
        return estimated_duration;
    }

    public void setEstimatedDuration(int estimated_duration) {
        this.estimated_duration = estimated_duration;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getTotalRoom() {
        return total_room;
    }

    public void setTotalRoom(int total_room) {
        this.total_room = total_room;
    }

    public String getTotalSize() {
        return total_size;
    }

    public void setTotalSize(String total_size) {
        this.total_size = total_size;
    }

    public String getTotalPrice() {
        return total_price;
    }

    public void setTotalPrice(String total_price) {
        this.total_price = total_price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaymentMethod() {
        return payment_method;
    }

    public void setPaymentMethod(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getPaymentAccountNo() {
        return payment_account_no;
    }

    public void setPaymentAccountNo(String payment_account_no) {
        this.payment_account_no = payment_account_no;
    }

    public String getPaymentAccountName() {
        return payment_account_name;
    }

    public void setPaymentAccountName(String payment_account_name) {
        this.payment_account_name = payment_account_name;
    }

    public String getPaymentAmount() {
        return payment_amount;
    }

    public void setPaymentAmount(String payment_amount) {
        this.payment_amount = payment_amount;
    }

    public String getPaymentStatus() {
        return payment_status;
    }

    public void setPaymentStatus(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getProgressCount() {
        return progress_count;
    }

    public void setProgressCount(String progress_count) {
        this.progress_count = progress_count;
    }

    public MasterCode getStatusDetail() {
        return status_detail;
    }

    public void setStatusDetail(MasterCode status_detail) {
        this.status_detail = status_detail;
    }

    public List<TransactionDetail> getDetail() {
        return detail;
    }

    public void setDetail(List<TransactionDetail> detail) {
        this.detail = detail;
    }
}
