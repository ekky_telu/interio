package com.example.client_mobile.Screen.Login;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.client_mobile.Base.BaseErrorResponse;
import com.example.client_mobile.Base.BaseResponse;
import com.example.client_mobile.Model.User;
import com.example.client_mobile.Network.Service;
import com.example.client_mobile.SharedPreferences.AppState;
import com.example.client_mobile.Utils.ErrorUtil;
import com.example.client_mobile.Utils.StateViewModel;
import com.example.client_mobile.Utils.UtilApi;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.example.client_mobile.Model.Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends StateViewModel<LoginViewModel.State> {
    private static final String TAG = "LoginViewModel";
    Service mApiService = UtilApi.getApiService();
    AppState appState = AppState.getInstance();
    FirebaseDatabase firebase = FirebaseDatabase.getInstance();

    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message = "";
        JsonArray errors;
        Login login = new Login();
    }

    void login(String username, String password, String role) {
        state.isLoading = true;
        state.status = LoginViewModel.Status.INIT;
        updateState();

        mApiService.login(username, password, role).enqueue(new Callback<BaseResponse<Login>>() {
            @Override
            public void onResponse(Call<BaseResponse<Login>> call, Response<BaseResponse<Login>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.login = response.body().getData();
                    state.message = response.body().getMessage();
                    appState.setToken(state.login.getAccessToken());
                    appState.setIsLoggedIn(true);
                    getDataUser();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = LoginViewModel.Status.ERROR;
                    state.errors = errorResponse.getErrors();

                    JsonObject errorObj = (JsonObject) state.errors.get(0);
                    state.message = errorObj.get("message").getAsString();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Login>> call, Throwable t) {
                state.isLoading = false;
                state.status = LoginViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    private void getDataUser() {
        mApiService.getProfile().enqueue(new Callback<BaseResponse<User>>() {
            @Override
            public void onResponse(Call<BaseResponse<User>> call, Response<BaseResponse<User>> response) {
                User user = response.body().getData();
                appState.setUser(user);
                state.status = Status.SUCCESS;

                updateUserUid();

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<User>> call, Throwable t) {
                state.isLoading = false;
                state.status = LoginViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    private void updateUserUid() {
        DatabaseReference dbRef = firebase.getReference();
        dbRef.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (snapshot.child("username").getValue().toString().equals(appState.getUser().getUsername())) {
                        appState.setUserUid(snapshot.getKey());
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
