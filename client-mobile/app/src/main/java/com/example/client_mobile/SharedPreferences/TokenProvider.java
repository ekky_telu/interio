package com.example.client_mobile.SharedPreferences;

public interface TokenProvider {
    void setToken(String token);

    boolean hasToken();

    String provideToken();

    void removeToken();
}
