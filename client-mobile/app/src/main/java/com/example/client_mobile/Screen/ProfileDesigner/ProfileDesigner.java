package com.example.client_mobile.Screen.ProfileDesigner;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.example.client_mobile.Adapter.PagerAdapter;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.R;
import com.example.client_mobile.Screen.Chat.DetailChat;
import com.example.client_mobile.Screen.DetailRoom.DetailRoom;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileDesigner extends AppCompatActivity {

    private static final String TAG = "ProfileDesignerActivity";

    /* Define ViewModel */
    private ProfileDesignerViewModel viewModel;

    /* Define Component */
    @BindView(R.id.nameProf)
    TextView mNameDs;

    @BindView(R.id.usernameProf)
    TextView mUsername;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.pager)
    ViewPager viewPager;

    @BindView(R.id.gayaDesain)
    TextView mDesignStyle;

    @BindView(R.id.projectCount)
    TextView mProjectCount;

    FirebaseDatabase firebase = FirebaseDatabase.getInstance();
    DatabaseReference dbRef = firebase.getReference();

    Designer designer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_designer);

        ButterKnife.bind(this);

        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_label1));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_label2));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        String id = getIntent().getStringExtra("designer_id");
        viewModel = new ViewModelProvider(this).get(ProfileDesignerViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderDataProfile(state.designer, state.message, state.status);
        });

        viewModel.getProfileDesigner(id);
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderDataProfile(Designer designer, String message, ProfileDesignerViewModel.Status status) {
        if (status == ProfileDesignerViewModel.Status.SUCCESS) {
            mNameDs.setText(designer.getName());
            mUsername.setText("@" + designer.getUsername());
            mDesignStyle.setText((designer.getMainDesignStyle() == null) ? "-" : designer.getMainDesignStyleDetail().getDescription());
            mProjectCount.setText(designer.getProjectsCount());

            PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), designer);
            viewPager.setAdapter(adapter);

            this.designer = designer;
        } else {
            if (message != null) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void back(View view) {
        onBackPressed();
    }

    public void toChat(View view) {
        Query query = dbRef.child("designers").orderByChild("id").equalTo(designer.getId());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String designerUid = null;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    designerUid = snapshot.getKey();
                    break;
                }

                showChat(designerUid);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void showChat(String designerUid) {
        Intent chat = new Intent(this, DetailChat.class);
        chat.putExtra("designer_uid", designerUid);
        startActivity(chat);
    }

    public void toDetailRoom(View view) {
        if (designer.getPrice() == null) {
            Toast.makeText(this, "Desainer Belum Mengatur Harga", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent detailRoom = new Intent(this, DetailRoom.class);
        detailRoom.putExtra("designer_id", designer.getId());
        startActivity(detailRoom);
    }
}
