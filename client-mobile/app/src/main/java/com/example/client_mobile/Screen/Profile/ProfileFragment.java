package com.example.client_mobile.Screen.Profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.client_mobile.Adapter.PagerAdapter;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.Model.User;
import com.example.client_mobile.R;
import com.example.client_mobile.Screen.SettingProfile.SettingProfile;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileFragment extends Fragment {
    private static final String TAG = "ProfileFragment";

    /* Define ViewModel */
    private ProfileViewModel viewModel;

    /* Define Component */
    @BindView(R.id.name)
    TextView mName;

    @BindView(R.id.username)
    TextView mUsername;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.transactionCount)
    TextView mTransactionCount;

    @BindView(R.id.archiveCount)
    TextView mArchiveCount;

    @BindView(R.id.btnSetting)
    Button mBtnSetting;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment, container, false);
        ButterKnife.bind(this, view);

        mBtnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settingProfile = new Intent(v.getContext(), SettingProfile.class);
                startActivity(settingProfile);
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        init();
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        viewModel.getState().observe(getActivity(), state -> {
            renderLoading(state.isLoading);
            renderDataProfile(state.user, state.message, state.status);
        });

        viewModel.getProfile();
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderDataProfile(User user, String message, ProfileViewModel.Status status) {
        if (status == ProfileViewModel.Status.SUCCESS) {
            mName.setText(user.getName());
            mUsername.setText("@" + user.getUsername());
        } else {
            if (message != null) {
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
