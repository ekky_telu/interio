package com.example.client_mobile.Network;

import android.view.SurfaceControl;

import com.example.client_mobile.Base.BaseResponse;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.Model.Gallery;
import com.example.client_mobile.Model.Login;
import com.example.client_mobile.Model.MasterCode;
import com.example.client_mobile.Model.Project;
import com.example.client_mobile.Model.Transaction;
import com.example.client_mobile.Model.User;

import java.util.List;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Service {
    /*
     * Example : Method GET, Link Based on API Documentation
     * @GET("designer/project/{id}")
     * Call<BaseResponse<Project>> getProjectById(
     *       @Path("id") String id
     * );
     */

    @GET("client/designer/{id}")
    Call<BaseResponse<Designer>> getDesignerById(
            @Path("id") String id
    );

    @GET("client/transaction/{id}")
    Call<BaseResponse<Transaction>> getTransactionById(
            @Path("id") String id
    );

    @FormUrlEncoded
    @POST("client/transaction")
    Call<BaseResponse<Transaction>> detailRoom(
            @Field("description") String description,
            @Field("property_type") String property_type,
            @Field("room_length") int room_length,
            @Field("room_width") int room_width,
            @Field("room_height") int room_height,
            @Field("design_style") String design_style,
            @Field("designer_id") String designer_id
    );

    @FormUrlEncoded
    @PUT("client/transaction/payment/{id}")
    Call<BaseResponse> payment(
            @Path("id") String id,
            @Field("payment_amount") int payment_amount,
            @Field("payment_method") String payment_method
    );

    @GET("mastercode")
    Call<List<MasterCode>> getMasterCodeByParent(
            @Query("parent_code") String parent_code
    );

    @FormUrlEncoded
    @POST("register")
    Call<BaseResponse<User>> register(
            @Field("username") String username,
            @Field("email") String email,
            @Field("password") String password,
            @Field("password_confirmation") String password_confirmation,
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("phone") String phone,
            @Field("role") String role
    );

    @FormUrlEncoded
    @POST("login")
    Call<BaseResponse<Login>> login(
            @Field("username") String username,
            @Field("password") String password,
            @Field("role") String role
    );

    @GET("client/profile")
    Call<BaseResponse<User>> getProfile();

    @GET("client/designer")
    Call<BaseResponse<List<Designer>>> getAllDesigners();

    @GET("client/project")
    Call<BaseResponse<List<Project>>> getAllProjects();

    @GET("client/project/{id}")
    Call<BaseResponse<Project>> getProjectById(
            @Path("id") String id
    );

    @GET("client/gallery/{id}")
    Call<BaseResponse<Gallery>> getGalleryById(
            @Path("id") String id
    );

    @FormUrlEncoded
    @PUT("client/profile/update")
    Call<BaseResponse<User>> updateProfile(
            @Field("username") String username,
            @Field("email") String email,
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("phone") String phone,
            @Field("role") String role,
            @Field("province") String province,
            @Field("city") String city,
            @Field("address") String address
    );

    @FormUrlEncoded
    @POST("client/profile/password")
    Call<BaseResponse> updatePassword(
            @Field("old_password") String old_password,
            @Field("password") String password,
            @Field("password_confirmation") String password_confirmation,
            @Field("role") String role
    );

    @GET("client/logout")
    Call<BaseResponse> logout();

    @GET("client/transaction")
    Call<BaseResponse<List<Transaction>>> getTransactionByClientId(
            @Query("user_id") String user_id
    );

    @PUT("client/transaction/cancel/{id}")
    Call<BaseResponse> cancelTransaction(
            @Path("id") String id
    );

    @PUT("client/transaction/complete/{id}")
    Call<BaseResponse> completeTransaction(
            @Path("id") String id
    );

    @FormUrlEncoded
    @POST("check-username")
    Call<BaseResponse> checkUsername(
            @Field("role") String role,
            @Field("username") String username
    );

    @FormUrlEncoded
    @POST("check-email")
    Call<BaseResponse> checkEmail(
            @Field("role") String role,
            @Field("email") String email
    );
}
