package com.example.client_mobile.Screen.ProfileDesigner;

import android.util.Log;

import com.example.client_mobile.Base.BaseErrorResponse;
import com.example.client_mobile.Base.BaseResponse;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.Network.Service;
import com.example.client_mobile.SharedPreferences.AppState;
import com.example.client_mobile.Utils.ErrorUtil;
import com.example.client_mobile.Utils.StateViewModel;
import com.example.client_mobile.Utils.UtilApi;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileDesignerViewModel extends StateViewModel<ProfileDesignerViewModel.State> {

    private static final String TAG = "ProfileDesignerVM";

    private Service mApiService = UtilApi.getApiService();
    private AppState appState = AppState.getInstance();

    @Override
    protected ProfileDesignerViewModel.State initState() {
        return new State();
    }

    public enum Status {
        INIT, SUCCESS, ERROR
    }

    public class State {
        public boolean isLoading = false;
        ProfileDesignerViewModel.Status status = ProfileDesignerViewModel.Status.INIT;
        public String message = null;
        public Designer designer;
    }

    void getProfileDesigner(String id) {
        state.isLoading = true;
        state.status = ProfileDesignerViewModel.Status.INIT;
        updateState();

        mApiService.getDesignerById(id).enqueue(new Callback<BaseResponse<Designer>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Designer>> call, @NotNull Response<BaseResponse<Designer>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = Status.SUCCESS;
                    state.designer = response.body().getData();
                    Log.d(TAG, state.designer.getId());
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<Designer>> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = Status.ERROR;
                state.message = t.getMessage();
                updateState();
            }
        });
    }
}
