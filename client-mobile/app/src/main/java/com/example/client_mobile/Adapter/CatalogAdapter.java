package com.example.client_mobile.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.Model.Gallery;
import com.example.client_mobile.Model.PortfolioPhoto;
import com.example.client_mobile.Model.Project;
import com.example.client_mobile.R;
import com.example.client_mobile.Screen.ProfileDesigner.ProfileDesigner;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class CatalogAdapter extends RecyclerView.Adapter<CatalogAdapter.ViewHolder> {

    private LayoutInflater layoutInflater;
    private List<Designer> designers;
    private Context context;

    public CatalogAdapter(Context context, List<Designer> designers) {
        this.layoutInflater = LayoutInflater.from(context);
        this.designers = designers;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.custom_view, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Designer designer = designers.get(i);
        viewHolder.textName.setText(designer.getName());
        viewHolder.textUsername.setText("@" + designer.getUsername());
        viewHolder.textName2.setText(designer.getName());
        viewHolder.textUsername2.setText("@" + designer.getUsername());

        List<PortfolioPhoto> photos = new ArrayList<>();
        for (Project project : designer.getProjects()) {
            photos.addAll(project.getPhotos());
        }

        for (Gallery gallery : designer.getGalleries()) {
            photos.addAll(gallery.getPhotos());
        }

        viewHolder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toProfileDesigner(v, designer);
            }
        });

        if (photos.size() == 0) {
            viewHolder.mNoData.setVisibility(View.VISIBLE);
            viewHolder.mCatalogLayout.setVisibility(View.GONE);
            return;
        }

        CatalogPhotoAdapter adapter = new CatalogPhotoAdapter(context, photos);
        viewHolder.mPhotos.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, true));
        viewHolder.mPhotos.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return designers.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textName, textUsername, textName2, textUsername2;
        RecyclerView mPhotos;
        LinearLayout mNoData, mCatalogLayout;

        RelativeLayout mMainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.designer_name);
            textUsername = itemView.findViewById(R.id.designer_username);
            textName2 = itemView.findViewById(R.id.designer_name2);
            textUsername2 = itemView.findViewById(R.id.designer_username2);
            mPhotos = itemView.findViewById(R.id.photos);
            mNoData = itemView.findViewById(R.id.noData);
            mCatalogLayout = itemView.findViewById(R.id.catalog_layout);
            mMainLayout = itemView.findViewById(R.id.main_layout);
        }
    }

    private void toProfileDesigner(View v, Designer designer) {
        Intent intent = new Intent(v.getContext(), ProfileDesigner.class);
        intent.putExtra("designer_id", designer.getId());
        v.getContext().startActivity(intent);
    }
}