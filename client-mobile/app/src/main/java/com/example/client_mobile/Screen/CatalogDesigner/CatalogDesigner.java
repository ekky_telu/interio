package com.example.client_mobile.Screen.CatalogDesigner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.example.client_mobile.Adapter.CatalogAdapter;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CatalogDesigner extends Fragment {
    CatalogAdapter adapter;
    ArrayList<String> items;

    private static final String TAG = "CatalogDesignerFragment";

    /* Define ViewModel */
    private CatalogDesignerViewModel viewModel;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.noData)
    RelativeLayout mNoData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_catalog_designer, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        init();
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(CatalogDesignerViewModel.class);
        viewModel.getState().observe(getActivity(), state -> {
            renderLoading(state.isLoading);
            renderCatalog(state.designer);
        });

        viewModel.getCatalog();
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderCatalog(List<Designer> designers) {
        if(designers != null) {
            if (designers.size() == 0) {
                mNoData.setVisibility(View.VISIBLE);
                return;
            }

            adapter = new CatalogAdapter(getContext(), designers);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, true));
            recyclerView.setAdapter(adapter);
        }
    }
}
