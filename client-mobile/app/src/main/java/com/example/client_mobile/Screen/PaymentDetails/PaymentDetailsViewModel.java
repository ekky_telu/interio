package com.example.client_mobile.Screen.PaymentDetails;

import android.util.Log;

import com.example.client_mobile.Base.BaseErrorResponse;
import com.example.client_mobile.Base.BaseResponse;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.Model.Transaction;
import com.example.client_mobile.Network.Service;
import com.example.client_mobile.Screen.CatalogDesigner.CatalogDesignerViewModel;
import com.example.client_mobile.Screen.DetailRoom.DetailRoomViewModel;
import com.example.client_mobile.Screen.PaymentDetails.PaymentDetailsViewModel;
import com.example.client_mobile.SharedPreferences.AppState;
import com.example.client_mobile.Utils.ErrorUtil;
import com.example.client_mobile.Utils.StateViewModel;
import com.example.client_mobile.Utils.UtilApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentDetailsViewModel extends StateViewModel<PaymentDetailsViewModel.State> {
    private static final String TAG = "PaymentDetailsViewModel";

    Service mApiService = UtilApi.getApiService();

    AppState appState = AppState.getInstance();

    @Override
    protected PaymentDetailsViewModel.State initState() {
        return new PaymentDetailsViewModel.State();
    }

    enum Status{
        INIT, SUCCESS, ERROR
    }

    public class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message = "";
        Transaction transaction;
    }

    void getTransaction(String id) {
        state.isLoading = true;
        state.status = PaymentDetailsViewModel.Status.INIT;
        updateState();

        mApiService.getTransactionById(id).enqueue(new Callback<BaseResponse<Transaction>>() {
            @Override
            public void onResponse(Call<BaseResponse<Transaction>> call, Response<BaseResponse<Transaction>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.transaction = response.body().getData();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<Transaction>> call, Throwable t) {
                state.isLoading = false;
                state.status = Status.ERROR;
                state.message = t.getMessage();
                updateState();
            }
        });
    }

    void updatePayment(String id, int payment_amount, String payment_method) {
        state.isLoading = true;
        state.status = PaymentDetailsViewModel.Status.INIT;
        updateState();

        mApiService.payment(id, payment_amount, payment_method).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = Status.SUCCESS;
                    state.message = response.body().getMessage();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                state.isLoading = false;
                state.status = Status.ERROR;
                state.message = t.getMessage();
                updateState();
            }
        });
    }
}
