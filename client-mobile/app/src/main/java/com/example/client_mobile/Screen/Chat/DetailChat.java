package com.example.client_mobile.Screen.Chat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.client_mobile.Adapter.DetailChatAdapter;
import com.example.client_mobile.Model.Chat;
import com.example.client_mobile.Model.ChatMessage;
import com.example.client_mobile.R;
import com.example.client_mobile.SharedPreferences.AppState;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailChat extends AppCompatActivity {

    private static String TAG = "DetailChatActivity";
    FirebaseDatabase firebase = FirebaseDatabase.getInstance();
    DatabaseReference dbRef;
    DetailChatAdapter adapter;

    String chatUid;
    String userUid;
    String designerUid;
    List<ChatMessage> chatMessages = new ArrayList<>();

    @BindView(R.id.toolbar)
    Toolbar mToolBar;
    @BindView(R.id.messages)
    RecyclerView mMessages;
    @BindView(R.id.name)
    TextView mName;
    @BindView(R.id.username)
    TextView mUsername;
    @BindView(R.id.message)
    TextView mMessage;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_chat);

        dbRef = firebase.getReference();

        ButterKnife.bind(this);
        setToolbar(mToolBar);

        chatUid = getIntent().getStringExtra("chat_uid");
        userUid = AppState.getInstance().getUserUid();
        designerUid = getIntent().getStringExtra("designer_uid");

        if (designerUid != null) {
            checkChatExist();
        } else {
            showChat(chatUid);
        }
    }

    private void showMessages() {
        adapter = new DetailChatAdapter(chatMessages, userUid);

        mMessages.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mMessages.setAdapter(adapter);
        setVisibilityProgressBar(false);
    }

    public void sendMessage(View view) {
        String message = mMessage.getText().toString();
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String date = dateFormat.format(c);
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        String time = timeFormat.format(c);

        ChatMessage chatMessage = new ChatMessage(userUid, message, time, date);
        dbRef.child("messages").child(chatUid).push().setValue(chatMessage);

        chatMessages.add(chatMessage);
        adapter.notifyItemInserted(chatMessages.size() - 1);
        mMessage.setText("");

        Map<String, Object> chatData = new HashMap<>();
        chatData.put("lastDate", date);
        chatData.put("lastTime", time);
        chatData.put("lastMessage", message);
        chatData.put("lastSentBy", userUid);
        chatData.put("lastSentRole", "users");

        dbRef.child("chats").child(chatUid).updateChildren(chatData);
    }

    private void checkChatExist() {
        setVisibilityProgressBar(true);
        dbRef.child("users").child(userUid).child("chatExistWith").child(designerUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                    addToChat();
                } else {
                    chatUid = dataSnapshot.getValue().toString();
                    showChat(dataSnapshot.getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("Error", databaseError.getMessage());
            }
        });
    }

    private void addToChat() {
        String message = "Halo " + AppState.getInstance().getUser().getFirstName() + "\n\nSilakan tanyakan apapun mengenai desain inteior";

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = dateFormat.format(c);

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        String formattedTime = timeFormat.format(c);

        Chat chat = new Chat(message, formattedTime, formattedDate, designerUid, "designers");
        DatabaseReference chatRef = dbRef.child("chats").push();
        chatRef.setValue(chat);

        dbRef.child("chatMembers").child(Objects.requireNonNull(chatRef.getKey())).child(userUid).setValue(true);
        dbRef.child("chatMembers").child(chatRef.getKey()).child(designerUid).setValue(true);

        ChatMessage chatMessage = new ChatMessage(designerUid, message, formattedTime, formattedDate);
        dbRef.child("messages").child(chatRef.getKey()).push().setValue(chatMessage);

        dbRef.child("users").child(userUid).child("chatExistWith").child(designerUid).setValue(chatRef.getKey());
        dbRef.child("designers").child(designerUid).child("chatExistWith").child(userUid).setValue(chatRef.getKey());

        showChat(chatRef.getKey());
    }

    private void showChat(String chatUid) {
        setVisibilityProgressBar(true);
        dbRef.child("chatMembers").child(chatUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String designerUid = null;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (!snapshot.getKey().equals(userUid)) {
                        designerUid = snapshot.getKey();
                        break;
                    }
                }

                setDesignerInfo(designerUid);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        dbRef.child("messages").child(chatUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String sentBy = snapshot.child("sentBy").getValue().toString();
                    String message = snapshot.child("message").getValue().toString();
                    String date = snapshot.child("date").getValue().toString();
                    String time = snapshot.child("time").getValue().toString();

                    ChatMessage chatMessage = new ChatMessage(sentBy, message, time, date);
                    chatMessages.add(chatMessage);

                    showMessages();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setDesignerInfo(String designerUid) {
        dbRef.child("designers").child(designerUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String firstName = dataSnapshot.child("firstName").getValue().toString();
                String username = dataSnapshot.child("username").getValue().toString();

                mName.setText(firstName);
                mUsername.setText("@" + username);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_dark);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setVisibilityProgressBar(boolean visibility) {
        mProgressBar.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
