package com.example.client_mobile.Model;

import com.google.gson.annotations.SerializedName;

public class Login {
    @SerializedName("access_token")
    private String access_token;

    public String getAccessToken() {
        return access_token;
    }

    public void setAccessToken(String access_token) {
        this.access_token = access_token;
    }
}
