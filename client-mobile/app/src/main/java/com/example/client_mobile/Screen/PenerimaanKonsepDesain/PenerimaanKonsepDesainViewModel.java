package com.example.client_mobile.Screen.PenerimaanKonsepDesain;

import com.example.client_mobile.Base.BaseErrorResponse;
import com.example.client_mobile.Base.BaseResponse;
import com.example.client_mobile.Model.Transaction;
import com.example.client_mobile.Network.Service;
import com.example.client_mobile.SharedPreferences.AppState;
import com.example.client_mobile.Utils.ErrorUtil;
import com.example.client_mobile.Utils.StateViewModel;
import com.example.client_mobile.Utils.UtilApi;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PenerimaanKonsepDesainViewModel extends StateViewModel<PenerimaanKonsepDesainViewModel.State> {
    private static final String TAG = "RegisterActivity";
    Service mApiService = UtilApi.getApiService();

    AppState appState = AppState.getInstance();

    @Override
    protected PenerimaanKonsepDesainViewModel.State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        PenerimaanKonsepDesainViewModel.Status status = PenerimaanKonsepDesainViewModel.Status.INIT;
        String message = "";
        Transaction transaction;
    }

    void getTransaction(String transactionId) {
        state.isLoading = true;
        state.status = PenerimaanKonsepDesainViewModel.Status.INIT;
        updateState();

        mApiService.getTransactionById(transactionId).enqueue(new Callback<BaseResponse<Transaction>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Transaction>> call, @NotNull Response<BaseResponse<Transaction>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.transaction = response.body().getData();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = PenerimaanKonsepDesainViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<Transaction>> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = PenerimaanKonsepDesainViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void completeTransaction(String transactionId) {
        state.isLoading = true;
        state.status = PenerimaanKonsepDesainViewModel.Status.INIT;
        updateState();

        mApiService.completeTransaction(transactionId).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse> call, @NotNull Response<BaseResponse> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = PenerimaanKonsepDesainViewModel.Status.SUCCESS;
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = PenerimaanKonsepDesainViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = PenerimaanKonsepDesainViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }
}
