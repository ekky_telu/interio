package com.example.client_mobile.Screen.Register;

import com.example.client_mobile.Base.BaseErrorResponse;
import com.example.client_mobile.Base.BaseMasterCode;
import com.example.client_mobile.Base.BaseResponse;
import com.example.client_mobile.Model.MasterCode;
import com.example.client_mobile.Model.User;
import com.example.client_mobile.Network.Service;
import com.example.client_mobile.SharedPreferences.AppState;
import com.example.client_mobile.Utils.ErrorUtil;
import com.example.client_mobile.Utils.StateViewModel;
import com.example.client_mobile.Utils.UtilApi;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterViewModel extends StateViewModel<RegisterViewModel.State> {
    private static final String TAG = "RegisterViewModel";
    private Service mApiService = UtilApi.getApiService();
    private FirebaseDatabase firebase = FirebaseDatabase.getInstance();

    @Override
    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    static class State {
        boolean isLoading = false;
        RegisterViewModel.Status status = RegisterViewModel.Status.INIT;
        String message = "";
        JsonArray errors;
        boolean isError = true;
    }

    void register(String username, String email, String password, String password_confirmation, String first_name, String last_name, String phone, String role) {
        state.isLoading = true;
        state.status = RegisterViewModel.Status.INIT;
        updateState();

        mApiService.register(username, email, password, password_confirmation, first_name, last_name, phone, role).enqueue(new Callback<BaseResponse<User>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<User>> call, @NotNull Response<BaseResponse<User>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    BaseResponse<User> baseResponse = response.body();
                    state.status = RegisterViewModel.Status.SUCCESS;
                    state.message = baseResponse.getMessage();

                    User user = baseResponse.getData();
                    DatabaseReference dbRef = firebase.getReference();
                    dbRef.child("users").push().setValue(user);
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = RegisterViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }
                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<User>> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = RegisterViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void checkUsername(String username, String role) {
        state.isLoading = true;
        state.status = RegisterViewModel.Status.INIT;
        updateState();

        mApiService.checkUsername(role, username).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                state.isLoading = false;
                if (!response.isSuccessful()) {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = RegisterViewModel.Status.ERROR;
                    state.errors = errorResponse.getErrors();
                    state.isError = true;

                    JsonObject errorObj = (JsonObject) state.errors.get(0);
                    state.message = errorObj.get("message").getAsString();
                } else {
                    state.status = Status.SUCCESS;
                    state.isError = false;
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    void checkEmail(String email, String role) {
        state.isLoading = true;
        state.status = RegisterViewModel.Status.INIT;
        updateState();

        mApiService.checkEmail(role, email).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                state.isLoading = false;
                if (!response.isSuccessful()) {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = RegisterViewModel.Status.ERROR;
                    state.errors = errorResponse.getErrors();
                    state.isError = true;

                    JsonObject errorObj = (JsonObject) state.errors.get(0);
                    state.message = errorObj.get("message").getAsString();
                } else {
                    state.status = Status.SUCCESS;
                    state.isError = false;
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }
}
