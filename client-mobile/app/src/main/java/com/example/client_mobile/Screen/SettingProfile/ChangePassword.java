package com.example.client_mobile.Screen.SettingProfile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.client_mobile.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePassword extends AppCompatActivity {

    private static final String TAG = "ChangePasswordActivity";

    /* Define ViewModel */
    private SettingProfileViewModel viewModel;

    /* Define Component */
    @BindView(R.id.sandi_saat_ini)
    TextView mOldPass;
    @BindView(R.id.kata_sandi_baru)
    TextView mPass;
    @BindView(R.id.konfirmasi_kata_sandi_baru)
    TextView mPassConf;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    String mRole = "client";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        ButterKnife.bind(this);
        init();
    }

    public void intentSP(View view) {
        onBackPressed();
        finish();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(SettingProfileViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderResponse(state.status, state.message);
        });
    }

    void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    void renderResponse(SettingProfileViewModel.Status status, String message) {
        if (message != null) {
            if (status == SettingProfileViewModel.Status.SUCCESS) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

                onBackPressed();
                finish();
            } else if (status == SettingProfileViewModel.Status.ERROR) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void save(View view) {
        viewModel.updatePassword(mOldPass.getText().toString(), mPass.getText().toString(), mPassConf.getText().toString(), mRole);
    }
}
