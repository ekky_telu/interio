package com.example.client_mobile.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.client_mobile.Model.ChatMessage;
import com.example.client_mobile.R;

import java.util.List;

public class DetailChatAdapter extends RecyclerView.Adapter<DetailChatAdapter.ViewHolder> {

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mMessage;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            mMessage = itemView.findViewById(R.id.message);
        }
    }

    private List<ChatMessage> messages;
    private String userUid;

    public DetailChatAdapter(List<ChatMessage> messages, String userUid) {
        this.messages = messages;
        this.userUid = userUid;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sent_message, parent, false);
            ViewHolder holder = new ViewHolder(layoutView);
            return holder;
        } else {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_received_message, parent, false);
            ViewHolder holder = new ViewHolder(layoutView);
            return holder;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        if (holder.getItemViewType() == 0) {
            holder.mMessage.setText(messages.get(position).getMessage());
//        } else {
//
//        }
    }

    @Override
    public int getItemViewType(int position) {
        if (messages.get(position).getSentBy().equals(userUid)) {
            return 0;
        }

        return 1;
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }
}
