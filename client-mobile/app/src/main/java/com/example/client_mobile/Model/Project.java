package com.example.client_mobile.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Project {
    @SerializedName("id")
    private String id;

    @SerializedName("designer_id")
    private String designer_id;

    @SerializedName("title")
    private String title;

    @SerializedName("location")
    private String location;

    @SerializedName("property_type")
    private String property_type;

    @SerializedName("property_type_detail")
    private MasterCode property_type_detail;

    @SerializedName("room_length")
    private String room_length;

    @SerializedName("room_width")
    private String room_width;

    @SerializedName("room_height")
    private String room_height;

    @SerializedName("design_style")
    private String design_style;

    @SerializedName("design_style_detail")
    private MasterCode design_style_detail;

    @SerializedName("duration")
    private String duration;

    @SerializedName("description")
    private String description;

    @SerializedName("photos")
    private List<PortfolioPhoto> photos;

    @SerializedName("designer")
    private Designer designer;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesignerId() {
        return designer_id;
    }

    public void setDesignerId(String designer_id) {
        this.designer_id = designer_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPropertyType() {
        return property_type;
    }

    public void setPropertyType(String property_type) {
        this.property_type = property_type;
    }

    public String getRoomLength() {
        return room_length;
    }

    public void setRoomLength(String room_length) {
        this.room_length = room_length;
    }

    public String getRoomWidth() {
        return room_width;
    }

    public void setRoomWidth(String room_width) {
        this.room_width = room_width;
    }

    public String getRoomHeight() {
        return room_height;
    }

    public void setRoomHeight(String room_height) {
        this.room_height = room_height;
    }

    public String getDesignStyle() {
        return design_style;
    }

    public void setDesignStyle(String design_style) {
        this.design_style = design_style;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<PortfolioPhoto> getPhotos() {
        return photos;
    }

    public void setPhotos(List<PortfolioPhoto> photos) {
        this.photos = photos;
    }

    public Designer getDesigner() {
        return designer;
    }

    public void setDesigner(Designer designer) {
        this.designer = designer;
    }

    public MasterCode getPropertyTypeDetail() {
        return property_type_detail;
    }

    public void setPropertyTypeDetail(MasterCode property_type_detail) {
        this.property_type_detail = property_type_detail;
    }

    public MasterCode getDesignStyleDetail() {
        return design_style_detail;
    }

    public void setDesignStyleDetail(MasterCode design_style_detail) {
        this.design_style_detail = design_style_detail;
    }
}
