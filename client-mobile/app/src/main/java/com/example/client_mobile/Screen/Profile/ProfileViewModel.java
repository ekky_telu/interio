package com.example.client_mobile.Screen.Profile;

import android.util.Log;

import com.example.client_mobile.Model.User;
import com.example.client_mobile.Network.Service;
import com.example.client_mobile.SharedPreferences.AppState;
import com.example.client_mobile.Utils.StateViewModel;
import com.example.client_mobile.Utils.UtilApi;

public class ProfileViewModel extends StateViewModel<ProfileViewModel.State> {

    private static final String TAG = "ProfileViewModel";

    private Service mApiService = UtilApi.getApiService();
    private AppState appState = AppState.getInstance();

    @Override
    protected ProfileViewModel.State initState() {
        return new State();
    }

    public enum Status {
        INIT, SUCCESS, ERROR
    }

    public class State {
        public boolean isLoading = false;
        ProfileViewModel.Status status = ProfileViewModel.Status.INIT;
        public String message = null;
        User user = new User();
    }

    void getProfile() {
        state.isLoading = true;
        state.user = appState.getUser();

        Log.d(TAG, state.user.getId());

        if (!state.user.getId().equals("")) {
            Log.d(TAG, state.user.getId());
            state.status = Status.SUCCESS;
            state.isLoading = false;
        } else {
            state.isLoading = false;
            state.status = Status.ERROR;
            state.message = "Terjadi Kesalahan";
        }

        updateState();
    }
}
