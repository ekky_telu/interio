package com.example.client_mobile.Screen.ListPesanan;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.client_mobile.Adapter.ListPesananAdapter;
import com.example.client_mobile.Model.Transaction;
import com.example.client_mobile.R;
import com.example.client_mobile.SharedPreferences.AppState;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartFragment extends Fragment {
    private static final String TAG = "ListPesananActivity";

    //vars
    private ArrayList<String> mOrder = new ArrayList<>();
    private ArrayList<String> mStatus = new ArrayList<>();

    private String user_id;
    private Context mContext;
    private ListPesananViewModel viewModel;

    /* Define Component */
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.recyclerViewPesanan)
    RecyclerView recyclerView;

    @BindView(R.id.noData)
    RelativeLayout mNoData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_list_pesanan, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        init();
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        user_id = AppState.getInstance().getUser().getId();
        viewModel = new ViewModelProvider(this).get(ListPesananViewModel.class);
        viewModel.getState().observe(getActivity(), state -> {
            renderLoading(state.isLoading);
            initRecyclerView(state.listPesanan);
        });

        viewModel.getListPesanan(user_id);
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void initRecyclerView(List<Transaction> transactions) {
        if (transactions.size() == 0) {
            mNoData.setVisibility(View.VISIBLE);
        } else {
            mNoData.setVisibility(View.GONE);
        }

        ListPesananAdapter adapter = new ListPesananAdapter(getContext(), transactions);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
    }
}
