package com.example.client_mobile.Adapter;

import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.Screen.ProfileDesigner.GalleryFragment;
import com.example.client_mobile.Screen.ProfileDesigner.ProjectFragment;

import org.jetbrains.annotations.NotNull;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    private Designer designer;

    public PagerAdapter(FragmentManager fm, int NumOfTabs, Designer designer) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.designer = designer;
    }

    /**
     * Return the Fragment associated with a specified position.
     *
     * @param position
     */
    @NotNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new GalleryFragment(designer);
            case 1:
                return new ProjectFragment(designer);
            default:
                return null;
        }
    }

    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
