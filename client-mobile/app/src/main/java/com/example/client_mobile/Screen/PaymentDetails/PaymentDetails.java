package com.example.client_mobile.Screen.PaymentDetails;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.client_mobile.Model.Transaction;
import com.example.client_mobile.R;
import com.example.client_mobile.Screen.PaymentDetails.PaymentDetailsViewModel;
import com.example.client_mobile.Screen.PickDesigner.PickDesigner;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentDetails extends AppCompatActivity {
    private static final String TAG = "PaymentDetailsActivity";

    /* Define ViewModel */
    private PaymentDetailsViewModel viewModel;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.harga)
    TextView mHarga;
    @BindView(R.id.luas)
    TextView mLuas;
    @BindView(R.id.totalHarga)
    TextView mTotalHarga;

    String mTransactionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);

        ButterKnife.bind(this);
        mTransactionId = getIntent().getStringExtra("transaction_id");
//        mTransactionId = "9045cdd0-760a-40aa-a262-86ebef276418";
        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(PaymentDetailsViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderResponse(state.status, state.message);
            renderTransaction(state.transaction);
        });

        viewModel.getTransaction(mTransactionId);
    }

    private void renderTransaction(Transaction transaction) {
        if (transaction != null) {
            mLuas.setText(transaction.getTotalSize());
            mTotalHarga.setText(transaction.getTotalPrice());
            mHarga.setText(transaction.getDesigner().getPrice());
        }
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderResponse(PaymentDetailsViewModel.Status status, String message) {
        if (status == PaymentDetailsViewModel.Status.SUCCESS) {
            Toast.makeText(this, message,
                    Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, TransactionResults.class);
            intent.putExtra("transaction_id", mTransactionId);
            startActivity(intent);
            finish();
        } else if (status == PaymentDetailsViewModel.Status.ERROR) {
            Toast.makeText(this, message,
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void paymentConf(View view) {
        viewModel.updatePayment(mTransactionId, Integer.parseInt(mHarga.getText().toString()), "PYMTF");
    }
}
