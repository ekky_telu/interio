package com.example.client_mobile.Model;

public class ChatMessage {
    private String sentBy;
    private String message;
    private String time;
    private String date;

    public ChatMessage(String sentBy, String message, String time, String date) {
        this.sentBy = sentBy;
        this.message = message;
        this.time = time;
        this.date = date;
    }

    public String getSentBy() {
        return sentBy;
    }

    public void setSentBy(String sentBy) {
        this.sentBy = sentBy;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
