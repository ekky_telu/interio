package com.example.client_mobile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.client_mobile.Model.PortfolioPhoto;
import com.example.client_mobile.Network.Client;
import com.example.client_mobile.R;

import java.util.List;

public class CatalogPhotoAdapter extends RecyclerView.Adapter<CatalogPhotoAdapter.ViewHolder> {

    private Context context;
    private List<PortfolioPhoto> photos;

    public CatalogPhotoAdapter(Context context, List<PortfolioPhoto> photos) {
        this.context = context;
        this.photos = photos;
    }

    @NonNull
    @Override
    public CatalogPhotoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_catalog_photo, parent, false);
        return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull CatalogPhotoAdapter.ViewHolder holder, int position) {
        PortfolioPhoto photo = photos.get(position);

        String imageUrl = "";
        if (photo.getGalleryId() != null) {
            imageUrl = Client.GALLERY_IMAGE_URL;
        } else if (photo.getProjectId() != null) {
            imageUrl = Client.PROJECT_IMAGE_URL;
        }

        Glide.with(context)
                .load(imageUrl + photo.getImage())
                .placeholder(R.mipmap.ic_interio_logo)
                .into(holder.mPhoto);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mPhoto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mPhoto = itemView.findViewById(R.id.photo);
        }
    }
}
