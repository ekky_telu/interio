package com.example.client_mobile.Screen.PickDesigner;

import com.example.client_mobile.Base.BaseErrorResponse;
import com.example.client_mobile.Base.BaseResponse;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.Model.Transaction;
import com.example.client_mobile.Network.Service;
import com.example.client_mobile.Screen.PickDesigner.PickDesignerViewModel;
import com.example.client_mobile.SharedPreferences.AppState;
import com.example.client_mobile.Utils.ErrorUtil;
import com.example.client_mobile.Utils.StateViewModel;
import com.example.client_mobile.Utils.UtilApi;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PickDesignerViewModel extends StateViewModel<PickDesignerViewModel.State> {
    private static final String TAG = "PickDesignerViewModel";

    Service mApiService = UtilApi.getApiService();

    AppState appState = AppState.getInstance();

    @Override
    protected PickDesignerViewModel.State initState() {
        return new PickDesignerViewModel.State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    public class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message = "";
        Transaction transaction;
        Designer designer;
    }

    void getDesigner(String designer_id) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();

        mApiService.getDesignerById(designer_id).enqueue(new Callback<BaseResponse<Designer>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Designer>> call, @NotNull Response<BaseResponse<Designer>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.designer = response.body().getData();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<Designer>> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = Status.ERROR;
                state.message = t.getMessage();
                updateState();
            }
        });
    }

    void createOrder(String description, String property_type, int room_length, int room_width, int room_height, String design_style, String designer_id) {
        state.isLoading = true;
        state.status = PickDesignerViewModel.Status.INIT;
        updateState();

        mApiService.detailRoom(
                description,
                property_type,
                room_length,
                room_width,
                room_height,
                design_style,
                designer_id
        ).enqueue(new Callback<BaseResponse<Transaction>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Transaction>> call, @NotNull Response<BaseResponse<Transaction>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = Status.SUCCESS;
                    state.message = response.body().getMessage();
                    state.transaction = response.body().getData();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<Transaction>> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = Status.ERROR;
                state.message = t.getMessage();
                updateState();
            }
        });
    }
}
