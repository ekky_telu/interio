package com.example.client_mobile.Screen.Home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.client_mobile.Adapter.ProjectAdapter;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.Model.Project;
import com.example.client_mobile.R;
import com.example.client_mobile.Adapter.TopDesignerAdapter;
import com.example.client_mobile.Screen.Chat.ChatList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends Fragment {
    private static final String TAG = "HomeFragment";

    /* Define ViewModel */
    private HomeViewModel viewModel;

    @BindView(R.id.RVTop)
    RecyclerView recyclerViewTop;
    @BindView(R.id.RVPost)
    RecyclerView recyclerViewPost;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.btnChat)
    Button mBtnChat;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        ButterKnife.bind(this, view);

        mBtnChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chatIntent = new Intent(getContext(), ChatList.class);
                startActivity(chatIntent);
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        init();
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        viewModel.getState().observe(getActivity(), state -> {
            renderLoading(state.isLoading);
            renderBestDesigners(state.designers, state.status);
            renderLastPosting(state.projects, state.status);
        });

        viewModel.getBestDesigner();
        viewModel.getLastPosting();
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderBestDesigners(List<Designer> designers, HomeViewModel.Status status) {
        if (status == HomeViewModel.Status.ERROR) {
            Toast.makeText(getContext(), "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
            return;
        }

        TopDesignerAdapter adapter = new TopDesignerAdapter(getContext(), designers);
        recyclerViewTop.setAdapter(adapter);
        recyclerViewTop.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
    }

    private void renderLastPosting(List<Project> projects, HomeViewModel.Status status) {
        if (status == HomeViewModel.Status.ERROR) {
            Toast.makeText(getContext(), "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
            return;
        }

        ProjectAdapter adapter = new ProjectAdapter(getContext(), projects);
        recyclerViewPost.setAdapter(adapter);
        recyclerViewPost.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
    }
}
