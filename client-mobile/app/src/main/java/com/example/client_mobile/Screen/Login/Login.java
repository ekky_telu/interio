package com.example.client_mobile.Screen.Login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.client_mobile.R;
import com.example.client_mobile.Screen.Home.Home;
import com.example.client_mobile.Screen.Register.Register;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Login extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    /* Define ViewModel */
    private LoginViewModel viewModel;

    /* Define Component */
    @BindView(R.id.username)
    TextView mUsername;

    @BindView(R.id.password)
    TextView mPassword;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    String mRole = "client";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        init();
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderResponse(state.status, state.message);
        });
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    void renderResponse(LoginViewModel.Status status, String message) {
        if (status == LoginViewModel.Status.SUCCESS) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

            Intent home = new Intent(this, Home.class);
            home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(home);
            finish();
        } else if (status == LoginViewModel.Status.ERROR) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    public void login(View view) {
        validate();

        viewModel.login(mUsername.getText().toString(), mPassword.getText().toString(), mRole);
    }

    public void validate() {
        if (mUsername.getText().toString().equals("")) {
            Toast.makeText(this, "Username Belum Diisi", Toast.LENGTH_SHORT).show();
            return;
        }

        if (mPassword.getText().toString().equals("")) {
            Toast.makeText(this, "Password Belum Diisi", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public void showRegister(View view) {
        Intent intent = new Intent(this, Register.class);
        startActivity(intent);
    }
}
