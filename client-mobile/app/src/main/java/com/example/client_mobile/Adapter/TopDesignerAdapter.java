package com.example.client_mobile.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.R;
import com.example.client_mobile.Screen.ProfileDesigner.ProfileDesigner;

import java.util.ArrayList;
import java.util.List;

public class TopDesignerAdapter extends RecyclerView.Adapter<TopDesignerAdapter.ViewHolder> {
    private static final String TAG = "TopDesignerAdapter";
    private List<Designer> mDesigners;
    private Context mContext;

    public TopDesignerAdapter(Context mContext, List<Designer> mDesigners) {
        this.mContext = mContext;
        this.mDesigners = mDesigners;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_top_designer, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Designer designer = mDesigners.get(position);
        Glide.with(mContext)
                .load("")
                .placeholder(R.mipmap.ic_interio_logo)
                .into(holder.imageTop);
        holder.designerName.setText(designer.getName());
        holder.description.setText(designer.getProjectsCount() + " Proyek\n" + designer.getGalleriesCount() + " Foto Galeri");
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileDesigner = new Intent(v.getContext(), ProfileDesigner.class);
                profileDesigner.putExtra("designer_id", designer.getId());
                v.getContext().startActivity(profileDesigner);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDesigners.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageTop;
        TextView designerName;
        TextView description;
        LinearLayout parentLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageTop = itemView.findViewById(R.id.imageTop);
            designerName = itemView.findViewById(R.id.namaDesigner);
            description = itemView.findViewById(R.id.description);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
