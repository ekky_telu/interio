package com.example.client_mobile.Screen.Chat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.client_mobile.Adapter.ChatAdapter;
import com.example.client_mobile.R;
import com.example.client_mobile.Screen.Home.Home;
import com.example.client_mobile.SharedPreferences.AppState;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatList extends AppCompatActivity {

    String TAG = "ChatListActivity";

    @BindView(R.id.chat_list)
    RecyclerView mChatList;
    @BindView(R.id.noData)
    RelativeLayout mNoData;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    FirebaseDatabase firebase = FirebaseDatabase.getInstance();
    DatabaseReference dbRef;

    List<String> chatUid = new ArrayList<>();
    List<String> name = new ArrayList<>();
    List<String> lastMessage = new ArrayList<>();

    String userUid = AppState.getInstance().getUserUid();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);

        ButterKnife.bind(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        chatUid.clear();
        name.clear();
        lastMessage.clear();
        init();
    }

    private void init() {
        dbRef = firebase.getReference();
        getUserChats();
    }

    private void getUserChats() {
        setVisibilityProgressBar(true);
        dbRef.child("users").child(userUid).child("chatExistWith").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChildren()) {
                    mNoData.setVisibility(View.VISIBLE);
                    setVisibilityProgressBar(false);
                    return;
                }

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    chatUid.add(snapshot.getValue().toString());
                    getChatInfo(snapshot.getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getChatInfo(String chatUid) {
        dbRef.child("chats").child(chatUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lastMessage.add(dataSnapshot.child("lastMessage").getValue().toString());
                getDesignerUid(dataSnapshot.getKey());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getDesignerUid(String chatUid) {
        dbRef.child("chatMembers").child(chatUid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String designerUid = null;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (!snapshot.getKey().equals(userUid)) {
                        designerUid = snapshot.getKey();
                        break;
                    }
                }

                setDesignerInfo(designerUid);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setDesignerInfo(String designerUid) {
        dbRef.child("designers").child(designerUid).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                name.add(dataSnapshot.getValue().toString());

                showChatList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void showChatList() {
        mChatList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        ChatAdapter adapter = new ChatAdapter(chatUid, name, lastMessage);
        mChatList.setAdapter(adapter);

        setVisibilityProgressBar(false);
    }

    private void setVisibilityProgressBar(boolean visibility) {
        mProgressBar.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void back(View view) {
        onBackPressed();
        finish();
    }
}
