package com.example.client_mobile.Screen.Home;

import com.example.client_mobile.Base.BaseErrorResponse;
import com.example.client_mobile.Base.BaseResponse;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.Model.Project;
import com.example.client_mobile.Network.Service;
import com.example.client_mobile.SharedPreferences.AppState;
import com.example.client_mobile.Utils.ErrorUtil;
import com.example.client_mobile.Utils.StateViewModel;
import com.example.client_mobile.Utils.UtilApi;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeViewModel extends StateViewModel<HomeViewModel.State> {
    private static final String TAG = "HomeViewModel";
    private Service mApiService = UtilApi.getApiService();
    AppState appState = AppState.getInstance();

    @Override
    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    static class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message = "";
        List<Designer> designers = new ArrayList<>();
        List<Project> projects = new ArrayList<>();
    }

    void getBestDesigner() {
        state.isLoading = true;
        state.status = HomeViewModel.Status.INIT;
        updateState();

        mApiService.getAllDesigners().enqueue(new Callback<BaseResponse<List<Designer>>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<List<Designer>>> call, @NotNull Response<BaseResponse<List<Designer>>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = HomeViewModel.Status.SUCCESS;
                    state.designers = response.body().getData();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = HomeViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<List<Designer>>> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = HomeViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void getLastPosting() {
        state.isLoading = true;
        state.status = HomeViewModel.Status.INIT;
        updateState();

        mApiService.getAllProjects().enqueue(new Callback<BaseResponse<List<Project>>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<List<Project>>> call, @NotNull Response<BaseResponse<List<Project>>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = HomeViewModel.Status.SUCCESS;
                    state.projects = response.body().getData();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = HomeViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<List<Project>>> call, @NotNull Throwable t) {
                state.isLoading = false;
                state.status = HomeViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }
}
