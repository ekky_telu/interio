package com.example.client_mobile.Screen.SettingProfile;

import com.example.client_mobile.Base.BaseErrorResponse;
import com.example.client_mobile.Base.BaseMasterCode;
import com.example.client_mobile.Base.BaseResponse;
import com.example.client_mobile.Model.User;
import com.example.client_mobile.Model.User;
import com.example.client_mobile.Network.Service;
import com.example.client_mobile.SharedPreferences.AppState;
import com.example.client_mobile.Utils.ErrorUtil;
import com.example.client_mobile.Utils.StateViewModel;
import com.example.client_mobile.Utils.UtilApi;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingProfileViewModel extends StateViewModel<SettingProfileViewModel.State> {
    private static final String TAG = "SettingProfileViewModel";
    Service mApiService = UtilApi.getApiService();
    AppState appState = AppState.getInstance();

    protected State initState() {
        return new State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message;
        User user;
        JsonArray errors;
        String[] designStyleCodeList;
        String[] designStyleNameList;
    }

    void updateProfile(String username, String email, String first_name, String last_name, String phone, String province, String city, String address, String role) {
        state.isLoading = true;
        state.status = Status.INIT;
        updateState();

        mApiService.updateProfile(username, email, first_name, last_name, phone, role, province, city, address).enqueue(new Callback<BaseResponse<User>>() {
            @Override
            public void onResponse(Call<BaseResponse<User>> call, Response<BaseResponse<User>> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = SettingProfileViewModel.Status.SUCCESS;
                    state.message = response.body().getMessage();
                    getDataUser();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = SettingProfileViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse<User>> call, Throwable t) {
                state.isLoading = false;
                state.status = SettingProfileViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void updatePassword(String old_password, String password, String password_confirmation, String role) {
        state.isLoading = true;
        state.status = SettingProfileViewModel.Status.INIT;
        updateState();

        mApiService.updatePassword(old_password, password, password_confirmation, role).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = SettingProfileViewModel.Status.SUCCESS;
                    state.message = response.body().getMessage();
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = SettingProfileViewModel.Status.ERROR;
                    state.errors = errorResponse.getErrors();

                    JsonObject errorObj = (JsonObject) state.errors.get(0);
                    state.message = errorObj.get("message").getAsString();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                state.isLoading = false;
                state.status = SettingProfileViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    private void getDataUser() {
        mApiService.getProfile().enqueue(new Callback<BaseResponse<User>>() {
            @Override
            public void onResponse(Call<BaseResponse<User>> call, Response<BaseResponse<User>> response) {
                User user = response.body().getData();
                appState.setUser(user);
            }

            @Override
            public void onFailure(Call<BaseResponse<User>> call, Throwable t) {
                state.isLoading = false;
                state.status = SettingProfileViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void logout() {
        state.isLoading = true;
        state.status = SettingProfileViewModel.Status.INIT;
        updateState();

        mApiService.logout().enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                state.isLoading = false;
                if (response.isSuccessful()) {
                    state.status = SettingProfileViewModel.Status.SUCCESS;
                    state.message = response.body().getMessage();
                    appState.setIsLoggedIn(false);
                } else {
                    BaseErrorResponse errorResponse = ErrorUtil.parseError(response);
                    state.status = SettingProfileViewModel.Status.ERROR;
                    state.message = errorResponse.getMessage();
                }

                updateState();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                state.isLoading = false;
                state.status = SettingProfileViewModel.Status.ERROR;
                state.message = t.getMessage();

                updateState();
            }
        });
    }

    void getProfile() {
        state.user = appState.getUser();
    }
}
