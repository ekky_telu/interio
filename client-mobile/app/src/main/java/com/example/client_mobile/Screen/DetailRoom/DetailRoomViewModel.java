package com.example.client_mobile.Screen.DetailRoom;

import com.example.client_mobile.Base.BaseErrorResponse;
import com.example.client_mobile.Base.BaseMasterCode;
import com.example.client_mobile.Base.BaseResponse;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.Model.MasterCode;
import com.example.client_mobile.Model.Transaction;
import com.example.client_mobile.Network.Service;
import com.example.client_mobile.Screen.DetailRoom.DetailRoomViewModel;
import com.example.client_mobile.SharedPreferences.AppState;
import com.example.client_mobile.Utils.ErrorUtil;
import com.example.client_mobile.Utils.StateViewModel;
import com.example.client_mobile.Utils.UtilApi;

import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailRoomViewModel extends StateViewModel<DetailRoomViewModel.State> {
    private static final String TAG = "DetailRoomViewModel";

    Service mApiService = UtilApi.getApiService();

    AppState appState = AppState.getInstance();

    BaseMasterCode masterCode = new BaseMasterCode();

    @Override
    protected DetailRoomViewModel.State initState() {
        return new DetailRoomViewModel.State();
    }

    enum Status {
        INIT, SUCCESS, ERROR
    }

    public class State {
        boolean isLoading = false;
        Status status = Status.INIT;
        String message = "";
        String[] propertyTypeCodeList;
        String[] propertyTypeNameList;
        String[] designStyleCodeList;
        String[] designStyleNameList;
    }

    void getPropertyTypeList() {
        state.isLoading = true;
        state.status = DetailRoomViewModel.Status.INIT;
        updateState();

        String parent_code = "PRT";
        mApiService.getMasterCodeByParent(parent_code).enqueue(new Callback<List<MasterCode>>() {
            @Override
            public void onResponse(Call<List<MasterCode>> call, Response<List<MasterCode>> response) {
                state.isLoading = false;
                List<MasterCode> data = response.body();

                state.propertyTypeCodeList = masterCode.getCodeList(data);
                state.propertyTypeNameList = masterCode.getNameList(data);

                updateState();
            }

            @Override
            public void onFailure(Call<List<MasterCode>> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }

    void getDesignStyleList() {
        state.isLoading = true;
        state.status = DetailRoomViewModel.Status.INIT;
        updateState();

        String parent_code = "DSY";
        mApiService.getMasterCodeByParent(parent_code).enqueue(new Callback<List<MasterCode>>() {
            @Override
            public void onResponse(Call<List<MasterCode>> call, Response<List<MasterCode>> response) {
                state.isLoading = false;
                List<MasterCode> data = response.body();

                state.designStyleCodeList = masterCode.getCodeList(data);
                state.designStyleNameList = masterCode.getNameList(data);
                updateState();
            }

            @Override
            public void onFailure(Call<List<MasterCode>> call, Throwable t) {
                state.isLoading = false;
                updateState();
            }
        });
    }
}
