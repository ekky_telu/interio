package com.example.client_mobile.Screen.Gallery.Detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.example.client_mobile.Adapter.PortfolioPhotoAdapter;
import com.example.client_mobile.Base.SpacesItemDecoration;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.Model.Gallery;
import com.example.client_mobile.Network.Client;
import com.example.client_mobile.R;
import com.example.client_mobile.Screen.Chat.DetailChat;
import com.example.client_mobile.Screen.ProfileDesigner.ProfileDesigner;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailGallery extends AppCompatActivity {

    private static final String TAG = "DetailGalleryActivity";

    /* Define ViewModel */
    private DetailGalleryViewModel viewModel;

    /* Define Context */
    private Context mContext;

    /* Define ID Gallery */
    private String id;

    /* Define Component */
    @BindView(R.id.name)
    TextView mName;
    @BindView(R.id.username)
    TextView mUsername;
    @BindView(R.id.designer_type)
    TextView mDesignerType;
    @BindView(R.id.design_style)
    TextView mDesignStyle;
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.description)
    TextView mDescription;
    @BindView(R.id.property_type)
    TextView mPropertyType;
    @BindView(R.id.room_size)
    TextView mRoomSize;
    @BindView(R.id.toolbar)
    Toolbar mToolBar;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.photos)
    RecyclerView mPhotos;

    FirebaseDatabase firebase = FirebaseDatabase.getInstance();
    DatabaseReference dbRef = firebase.getReference();

    Designer designer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_gallery);

        ButterKnife.bind(this);
        mContext = this;

        setToolbar(mToolBar);
        init();
    }

    private void init() {
        id = getIntent().getStringExtra("gallery_id");
        viewModel = new ViewModelProvider(this).get(DetailGalleryViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderData(state.gallery, state.status);
        });

        viewModel.getData(id);
    }

    void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void renderData(Gallery gallery, DetailGalleryViewModel.Status status) {
        if (gallery != null && status == DetailGalleryViewModel.Status.SUCCESS) {
            double roomSize = (Integer.parseInt(gallery.getRoomLength())) *
                    (Integer.parseInt(gallery.getRoomWidth())) *
                    (Integer.parseInt(gallery.getRoomHeight()));

            mName.setText(gallery.getDesigner().getName());
            mUsername.setText("@" + gallery.getDesigner().getUsername());
            mDesignerType.setText("Designer (Individual)");
            mDesignStyle.setText(gallery.getDesignStyleDetail().getDescription());
            mTitle.setText(gallery.getTitle());
            mDescription.setText(gallery.getDescription());
            mPropertyType.setText(gallery.getPropertyTypeDetail().getDescription());
            mRoomSize.setText(String.valueOf(roomSize));

            mPhotos.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL) {
                @Override
                public boolean canScrollVertically() {
                    return false;
                }
            });

            PortfolioPhotoAdapter adapter = new PortfolioPhotoAdapter(this, gallery.getPhotos(), Client.GALLERY_IMAGE_URL);
            mPhotos.setAdapter(adapter);
            SpacesItemDecoration decoration = new SpacesItemDecoration(25);
            mPhotos.addItemDecoration(decoration);

            designer = gallery.getDesigner();
        }
    }

    private void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showChat(String designerUid) {
        Intent chat = new Intent(this, DetailChat.class);
        chat.putExtra("designer_uid", designerUid);
        startActivity(chat);
    }

    public void toChat(View view) {
        Query query = dbRef.child("designers").orderByChild("id").equalTo(designer.getId());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String designerUid = null;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    designerUid = snapshot.getKey();
                    break;
                }

                showChat(designerUid);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void toProfileDesigner(View view) {
        Intent profileDesigner = new Intent(this, ProfileDesigner.class);
        profileDesigner.putExtra("designer_id", designer.getId());
        startActivity(profileDesigner);
    }
}
