package com.example.client_mobile;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.client_mobile.Model.Chat;
import com.example.client_mobile.Model.ChatMessage;
import com.example.client_mobile.Screen.Chat.DetailChat;
import com.example.client_mobile.SharedPreferences.AppState;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class Coba extends AppCompatActivity {
    String TAG = "Coba";

    Button coba;

    FirebaseDatabase firebase = FirebaseDatabase.getInstance();
    DatabaseReference dbRef;
    String designerUid;
    String userUid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coba);

        dbRef = firebase.getReference();

        coba = findViewById(R.id.chat);

        coba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                designerUid = "-M4ntf89y8bn7IGQy2bo";
                userUid = AppState.getInstance().getUserUid();

                dbRef.child("users").child(userUid).child("chatExistWith").child(designerUid).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() == null) {
                            addToChat();
                        } else {
                            goToChatDetail(dataSnapshot.getValue().toString());
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d("Error", databaseError.getMessage());
                    }
                });
            }
        });
    }

    private void addToChat() {
        String message = "Halo " + AppState.getInstance().getUser().getFirstName() + "\n\nSilakan tanyakan apapun mengenai desain inteior";

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = dateFormat.format(c);

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        String formattedTime = timeFormat.format(c);

        Chat chat = new Chat(message, formattedTime, formattedDate, designerUid,"designers");
        DatabaseReference chatRef = dbRef.child("chats").push();
        chatRef.setValue(chat);

        dbRef.child("chatMembers").child(Objects.requireNonNull(chatRef.getKey())).child(userUid).setValue(true);
        dbRef.child("chatMembers").child(chatRef.getKey()).child(designerUid).setValue(true);

        ChatMessage chatMessage = new ChatMessage(designerUid, message, formattedTime, formattedDate);
        dbRef.child("messages").child(chatRef.getKey()).push().setValue(chatMessage);

        dbRef.child("users").child(userUid).child("chatExistWith").child(designerUid).setValue(chatRef.getKey());
        dbRef.child("designers").child(designerUid).child("chatExistWith").child(userUid).setValue(chatRef.getKey());

        goToChatDetail(chatRef.getKey());
    }

    private void goToChatDetail(String chatUid) {
        Intent intent = new Intent(this, DetailChat.class);
        intent.putExtra("chat_uid", chatUid);
        startActivity(intent);
    }
}
