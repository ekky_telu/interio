package com.example.client_mobile.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.client_mobile.Model.Designer;
import com.example.client_mobile.Model.Project;
import com.example.client_mobile.Network.Client;
import com.example.client_mobile.R;
import com.example.client_mobile.Screen.Project.Detail.DetailProject;

import java.util.List;

public class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.ViewHolder> {
    private List<Project> projects;
    private Context context;
    private Designer designer;

    public ProjectAdapter(Context context, List<Project> projects) {
        this.context = context;
        this.projects = projects;
    }

    public ProjectAdapter(Context context, List<Project> projects, Designer designer) {
        this.context = context;
        this.projects = projects;
        this.designer = designer;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_project, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Project project = projects.get(position);

        Glide.with(context)
                .load("")
                .placeholder(R.mipmap.ic_interio_logo)
                .into(holder.avatar);
        Glide.with(context)
                .load(Client.PROJECT_IMAGE_URL + project.getPhotos().get(0).getImage())
                .placeholder(R.mipmap.ic_interio_logo)
                .into(holder.photo);
        holder.title.setText(project.getTitle());

        if (designer != null) {
            holder.designerName.setText(designer.getName());
            holder.designerUsername.setText("@" + designer.getUsername());
        } else {
            holder.designerName.setText(project.getDesigner().getName());
            holder.designerUsername.setText("@" + project.getDesigner().getUsername());
        }

        holder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detailProject = new Intent(v.getContext(), DetailProject.class);
                detailProject.putExtra("project_id", project.getId());
                v.getContext().startActivity(detailProject);
            }
        });
    }

    @Override
    public int getItemCount() {
        return projects.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView photo;
        TextView title;
        TextView designerName;
        TextView designerUsername;
        ImageView avatar;
        LinearLayout mLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            photo = itemView.findViewById(R.id.photo);
            title = itemView.findViewById(R.id.title);
            designerName = itemView.findViewById(R.id.designerName);
            designerUsername = itemView.findViewById(R.id.designerUsername);
            avatar = itemView.findViewById(R.id.avatar);
            mLayout = itemView.findViewById(R.id.layout);
        }
    }
}
