package com.example.client_mobile.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.client_mobile.R;
import com.example.client_mobile.Screen.Chat.DetailChat;

import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mPhoto;
        TextView mName;
        TextView mLastMessage;
        LinearLayout mDisplayMessage;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            mPhoto = itemView.findViewById(R.id.photo);
            mName = itemView.findViewById(R.id.name);
            mLastMessage = itemView.findViewById(R.id.last_message);
            mDisplayMessage = itemView.findViewById(R.id.display_message);
        }
    }

    private List<String> chatUid;
    private List<String> name;
    private List<String> lastMessage;

    public ChatAdapter(List<String> chatUid, List<String> name, List<String> lastMessage) {
        this.chatUid = chatUid;
        this.name = name;
        this.lastMessage = lastMessage;
    }

    @NonNull
    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
        ViewHolder holder = new ViewHolder(layoutView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ChatAdapter.ViewHolder holder, int position) {
        holder.mName.setText(name.get(position));
        holder.mLastMessage.setText(lastMessage.get(position));

        holder.mDisplayMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DetailChat.class);
                intent.putExtra("chat_uid", chatUid.get(position));
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return name.size();
    }
}
