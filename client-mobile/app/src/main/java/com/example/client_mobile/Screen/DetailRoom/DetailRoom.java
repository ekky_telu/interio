package com.example.client_mobile.Screen.DetailRoom;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.client_mobile.Base.BottomSheetListView;
import com.example.client_mobile.R;
import com.example.client_mobile.Screen.DetailRoom.DetailRoomViewModel;
import com.example.client_mobile.Screen.PaymentDetails.PaymentDetails;
import com.example.client_mobile.Screen.PaymentDetails.TransactionResults;
import com.example.client_mobile.Screen.PickDesigner.PickDesigner;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class DetailRoom extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private static final String TAG = "DetailRoomActivity";

    /* Define ViewModel */
    private DetailRoomViewModel viewModel;

    String mPropertyTypeCode, mDesignStyleCode, mDesignerId;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.deskripsi_input)
    EditText mDeskripsi;
    @BindView(R.id.property_type)
    EditText mTipeProperti;
    @BindView(R.id.design_style)
    EditText mGayaDesain;
    @BindView(R.id.panjang_input)
    EditText mPanjang;
    @BindView(R.id.lebar_input)
    EditText mLebar;
    @BindView(R.id.tinggi_input)
    EditText mTinggi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_room);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.property_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ButterKnife.bind(this);
        mDesignerId = getIntent().getStringExtra("designer_id");
        init();
    }

    public void displayToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String spinnerLabel = adapterView.getItemAtPosition(i).toString();
        displayToast(spinnerLabel);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //Do nothing.
    }

    private void init() {
        viewModel = new ViewModelProvider(this).get(DetailRoomViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
        });

        viewModel.getPropertyTypeList();
        viewModel.getDesignStyleList();

        mTipeProperti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPropertyType(v);
            }
        });

        mGayaDesain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDesignStyle(v);
            }
        });
    }

    public void showPropertyType(View view) {
        String title = "Tipe Properti";
        String[] codeList = viewModel.getState().getValue().propertyTypeCodeList;
        String[] nameList = viewModel.getState().getValue().propertyTypeNameList;

        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(R.layout.bottom_sheet_view);

        BottomSheetListView listViewName = dialog.findViewById(R.id.list_item);
        TextView titleView = dialog.findViewById(R.id.list_title);

        titleView.setText(title);

        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.item_bottom_sheet, nameList);
        listViewName.setAdapter(adapter);

        listViewName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mPropertyTypeCode = codeList[position];
                mTipeProperti.setText(parent.getItemAtPosition(position).toString());

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void showDesignStyle(View view) {
        String title = "Gaya Desain";
        String[] codeList = viewModel.getState().getValue().designStyleCodeList;
        String[] nameList = viewModel.getState().getValue().designStyleNameList;

        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(R.layout.bottom_sheet_view);

        BottomSheetListView listViewName = dialog.findViewById(R.id.list_item);
        TextView titleView = dialog.findViewById(R.id.list_title);

        titleView.setText(title);

        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.item_bottom_sheet, nameList);
        listViewName.setAdapter(adapter);

        listViewName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDesignStyleCode = codeList[position];
                mGayaDesain.setText(parent.getItemAtPosition(position).toString());

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void next(View view) {
        if (!validate()) {
            Toast.makeText(this, "Lengkapi Data!", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent pickDesigner = new Intent(this, PickDesigner.class);
        pickDesigner.putExtra("description", mDeskripsi.getText().toString());
        pickDesigner.putExtra("property_type", mPropertyTypeCode);
        pickDesigner.putExtra("room_length", Integer.parseInt(mPanjang.getText().toString()));
        pickDesigner.putExtra("room_width", Integer.parseInt(mLebar.getText().toString()));
        pickDesigner.putExtra("room_height", Integer.parseInt(mTinggi.getText().toString()));
        pickDesigner.putExtra("design_style", mDesignStyleCode);
        pickDesigner.putExtra("designer_id", mDesignerId);
        startActivity(pickDesigner);
    }

    private boolean validate() {
        return !mDeskripsi.getText().toString().equals("")
                && !mTipeProperti.getText().toString().equals("")
                && !mPanjang.getText().toString().equals("")
                && !mLebar.getText().toString().equals("")
                && !mLebar.getText().toString().equals("")
                && !mGayaDesain.getText().toString().equals("");
    }

    public void back(View view) {
        onBackPressed();
        finish();
    }
}
