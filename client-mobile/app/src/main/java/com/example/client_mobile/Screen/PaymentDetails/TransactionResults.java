package com.example.client_mobile.Screen.PaymentDetails;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.client_mobile.R;
import com.example.client_mobile.Screen.DetailPesanan.DetailPesanan;
import com.example.client_mobile.Screen.Home.Home;

public class TransactionResults extends AppCompatActivity {

    String transactionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_results);

        transactionId = getIntent().getStringExtra("transaction_id");
    }

    public void toHome(View view) {
        Intent home = new Intent(this, Home.class);
        startActivity(home);
        finish();
    }

    public void toDetailTransaction(View view) {
        Intent detail = new Intent(this, DetailPesanan.class);
        detail.putExtra("transaction_id", transactionId);
        startActivity(detail);
        finish();
    }
}
