package com.example.client_mobile.Base;

import com.google.gson.JsonArray;

public class BaseErrorResponse {
    private String message;
    private JsonArray errors;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JsonArray getErrors() {
        return errors;
    }

    public void setErrors(JsonArray errors) {
        this.errors = errors;
    }
}
