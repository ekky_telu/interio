package com.example.client_mobile.Screen.SettingProfile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.client_mobile.Model.User;
import com.example.client_mobile.R;
import com.example.client_mobile.Screen.SettingProfile.SettingProfile;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangeProfile extends AppCompatActivity {

    private static final String TAG = "ChangeProfileActivity";

    /* Define ViewModel */
    private SettingProfileViewModel viewModel;

    @BindView(R.id.nama_depan)
    EditText mNamaDepan;
    @BindView(R.id.nama_belakang)
    EditText mNamaBelakang;
    @BindView(R.id.email)
    EditText mEmail;
    @BindView(R.id.nomor_telepon)
    EditText mNomorTelepon;
    @BindView(R.id.provinsi)
    EditText mProvinsi;
    @BindView(R.id.kota)
    EditText mKota;
    @BindView(R.id.alamat_lengkap)
    EditText mAlamat;
    @BindView(R.id.username1)
    TextView mUsername;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    String mRole = "client";
    String mUsernameAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_profile);

        ButterKnife.bind(this);
        mProgressBar.bringToFront();
        mEmail.setEnabled(false);
        init();
    }

    public void init() {
        viewModel = new ViewModelProvider(this).get(SettingProfileViewModel.class);
        viewModel.getState().observe(this, state -> {
            renderLoading(state.isLoading);
            renderResponse(state.status, state.message);
            renderProfile(state.user);
        });

        viewModel.getProfile();
    }

    void renderProfile(User user) {
        mUsername.setText("@" + user.getUsername());
        mUsernameAPI = user.getUsername();
        mNamaDepan.setText(user.getFirstName());
        mNamaBelakang.setText(user.getLastName());
        mEmail.setText(user.getEmail());
        mNomorTelepon.setText(user.getPhone());
        mProvinsi.setText(user.getProvince());
        mKota.setText(user.getCity());
        mAlamat.setText(user.getAddress());
    }

    void renderLoading(boolean isLoading) {
        mProgressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        if (isLoading)
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    void renderResponse(SettingProfileViewModel.Status status, String message) {
        if (message != null) {
            if (status == SettingProfileViewModel.Status.SUCCESS) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

                onBackPressed();
                finish();
            } else if (status == SettingProfileViewModel.Status.ERROR) {
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void intentSP(View view) {
        onBackPressed();
        finish();
    }

    public void save(View view) {
        viewModel.updateProfile(mUsernameAPI,
                mEmail.getText().toString(),
                mNamaDepan.getText().toString(),
                mNamaBelakang.getText().toString(),
                mNomorTelepon.getText().toString(),
                mProvinsi.getText().toString(),
                mKota.getText().toString(),
                mAlamat.getText().toString(),
                mRole);
    }
}
