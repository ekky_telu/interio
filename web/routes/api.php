<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//    return $request->user();
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//});

Route::resource('/portfolio', 'Api\PortfolioController');

Route::post('/login', 'Api\AuthController@login')->name('login');
Route::post('/register', 'Api\UserController@store')->name('register');
Route::post('/check-username', 'Api\UserController@check_username')->name('check.username');
Route::post('/check-email', 'Api\UserController@check_email')->name('check.email');
Route::resource('/mastercode', 'Api\MastercodeController');

Route::group(['middleware' => 'auth:api', 'prefix' => 'client'], function () {
    Route::get('/profile', 'Api\AuthController@profile')->name('profile.user');
    Route::get('/logout', 'Api\AuthController@logout')->name('logout.user');

    Route::group(['prefix' => 'profile'], function () {
        Route::put('/update', 'Api\AuthController@update')->name('update.profile.user');
        Route::post('/password', 'Api\AuthController@update_password')->name('update.password.profile.user');
    });

    Route::group(['prefix' => 'transaction'], function () {
        Route::put('/payment/{transaction}', 'Api\TransactionController@update_payment')->name('payment.transaction.user');
        Route::put('/cancel/{transaction}', 'Api\TransactionController@cancel_transaction')->name('cancel.transaction.user');
        Route::put('/complete/{transaction}', 'Api\TransactionController@complete_transaction')->name('complete.transaction.user');
    });

    Route::resource('/user', 'Api\UserController');
    Route::resource('/designer', 'Api\DesignerController')->only(['index', 'show']);
    Route::resource('/project', 'Api\ProjectController')->only(['index', 'show']);
    Route::resource('/gallery', 'Api\GalleryController')->only(['index', 'show']);
    Route::resource('/transaction', 'Api\TransactionController')->only(['index', 'show', 'store']);
});

Route::group(['middleware' => 'auth:designer', 'prefix' => 'designer'], function () {
    Route::get('/profile', 'Api\AuthController@profile')->name('profile.designer');
    Route::get('/logout', 'Api\AuthController@logout')->name('logout.designer');

    Route::group(['prefix' => 'profile'], function () {
        Route::put('/update', 'Api\AuthController@update')->name('update.profile.designer');
        Route::post('/password', 'Api\AuthController@update_password')->name('update.password.profile.designer');
        Route::post('/price', 'Api\AuthController@update_price')->name('update.price.profile.designer');
        Route::post('/design-style', 'Api\AuthController@update_design_style')->name('update.design.style.profile.designer');
    });

    Route::group(['prefix' => 'transaction'], function () {
        Route::put('/accept/{transaction}', 'Api\TransactionController@accept_transaction')->name('accept.transaction.user');
        Route::put('/cancel/{transaction}', 'Api\TransactionController@cancel_transaction')->name('cancel.transaction.user');
        Route::post('/send-design', 'Api\TransactionController@send_design')->name('complete.transaction.user');
    });

    Route::resource('/user', 'Api\UserController');
    Route::resource('/designer', 'Api\DesignerController');
    Route::resource('/project', 'Api\ProjectController');
    Route::resource('/gallery', 'Api\GalleryController');
    Route::resource('/transaction', 'Api\TransactionController')->only(['index', 'show']);
});
