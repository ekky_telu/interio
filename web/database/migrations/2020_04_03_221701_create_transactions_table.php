<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('user_id');
            $table->uuid('designer_id');
            $table->text('description');
            $table->text('location')->nullable();
            $table->integer('estimated_duration')->nullable();
            $table->integer('duration')->nullable();
            $table->integer('total_room')->nullable();
            $table->decimal('total_size')->nullable();
            $table->decimal('total_price', 12, 2)->nullable();
            $table->string('status')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('payment_account_no')->nullable();
            $table->string('payment_account_name')->nullable();
            $table->decimal('payment_amount', 12, 2)->nullable();
            $table->string('payment_status')->nullable();
            $table->integer('progress_count')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
