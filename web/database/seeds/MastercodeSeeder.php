<?php

use Illuminate\Database\Seeder;

class MastercodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Mastercode::truncate();

        $mastercode = json_decode(File::get(database_path('sources/mastercode.json')), true);

        foreach ($mastercode as $item) {
            \App\Models\Mastercode::create([
                'parent_code' => $item['parent_code'],
                'code' => $item['code'],
                'description' => $item['description']
            ]);
        }
    }
}
