{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Register') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('register') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>--}}

{{--                                @error('name')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Register') }}--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endsection--}}

    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'interio') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-7 p-0 auth-container-bg">
            <div class="auth-bg" style="background-image: url('../images/base/auth-background.png');">
            </div>
        </div>
        <div class="col-sm-5 text-center auth-container">
            <div class="auth-content">
                <div class="text-title mb-3">Register</div>
                <form action="{{ route('register') }}" method="POST" class="text-left pr-4 pl-4">
                    @csrf
                    <div class="row">
                        <div class="form-group col">
                            <label class="text-caption-bold">Nama</label>
                            <input type="text" class="form-control @error('first_name') is-invalid @enderror"
                                   id="first_name" name="first_name" value="{{ old('first_name') }}"
                                   placeholder="Nama Depan">
                            @error('first_name')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group col">
                            <label class="text-caption-bold">&nbsp;</label>
                            <input type="text" class="form-control @error('last_name') is-invalid @enderror" id="last_name"
                                   name="last_name" value="{{ old('last_name') }}"
                                   placeholder="Nama Belakang">
                            @error('last_name')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="text-caption-bold">Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email"
                               name="email" placeholder="saya@gmail.com" value="{{ old('email') }}">
                        @error('email')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label class="text-caption-bold">Password</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                   id="password" name="password" placeholder="Password" value="{{ old('password') }}">
                            @error('password')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group col">
                            <label class="text-caption-bold">&nbsp;</label>
                            <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror"
                                   id="password_confirmation" name="password_confirmation"
                                   value="{{ old('password_confirmation') }}"
                                   placeholder="Konfirmasi Password">
                            @error('password_confirmation')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    {{--                <div class="row">--}}
                    {{--                    <div class="form-group col">--}}
                    {{--                        <label class="text-caption-bold">Address</label>--}}
                    {{--                        <select class="form-control @error('province') is-invalid @enderror" id="province"--}}
                    {{--                                name="province">--}}
                    {{--                            <option>Provinsi</option>--}}
                    {{--                        </select>--}}
                    {{--                        @error('province')--}}
                    {{--                        <div class="invalid-feedback">{{ $message }}</div>--}}
                    {{--                        @enderror--}}
                    {{--                    </div>--}}
                    {{--                    <div class="form-group col">--}}
                    {{--                        <label class="text-caption-bold">&nbsp;</label>--}}
                    {{--                        <select class="form-control @error('city') is-invalid @enderror" id="city" name="city">--}}
                    {{--                            <option>Pilih Kota</option>--}}
                    {{--                        </select>--}}
                    {{--                        @error('city')--}}
                    {{--                        <div class="invalid-feedback">{{ $message }}</div>--}}
                    {{--                        @enderror--}}
                    {{--                    </div>--}}
                    {{--                </div>--}}
                    {{--                <div class="form-group">--}}
                    {{--                    <textarea placeholder="Alamat Lengkap" class="form-control @error('address') is-invalid @enderror"--}}
                    {{--                              id="address" name="address">{{ old('address') }}</textarea>--}}
                    {{--                    @error('address')--}}
                    {{--                    <div class="invalid-feedback">{{ $message }}</div>--}}
                    {{--                    @enderror--}}
                    {{--                </div>--}}
                    <div class="row">
                        <div class="form-group col">
                            <label class="text-caption-bold">Tanggal Lahir</label>
                            <input type="text" class="form-control @error('birthdate') is-invalid @enderror" id="birthdate"
                                   name="birthdate" value="{{ old('birthdate') }}"
                                   placeholder="dd-mm-yyyy">
                            @error('birthdate')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group col">
                            <label class="text-caption-bold">Telepon</label>
                            <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone"
                                   name="phone" value="{{ old('phone') }}"
                                   placeholder="0813xxxxxxxx">
                            @error('phone')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group mb-4">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="role" name="role" value="ROLDS">
                            <label class="custom-control-label" for="role">Kamu seorang desainer interior?</label>
                        </div>
                    </div>
                    <div class="form-action">
                        <button class="btn btn-primary mb-4">Register</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{--<div class="col-sm-12">--}}
{{--    <div class="row">--}}
{{--        <div class="col-sm-7 p-0"--}}
{{--             style="background-image: url('../images/base/auth-background.png'); background-size: 100% auto; background-repeat: no-repeat">--}}
{{--        </div>--}}
{{--        <div class="col-sm-5 text-center pr-5" style="margin: 2% 0">--}}
{{--            --}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
</body>
</html>
