<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'interio') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-7 p-0 auth-container-bg">
            <div class="auth-bg" style="background-image: url('../images/base/auth-background.png');">
            </div>
        </div>
        <div class="col-sm-5 text-center auth-container">
            <div class="auth-content">
                <div class="text-title mb-5">Selamat Datang!</div>
                <form action="{{ route('login') }}" method="POST" class="text-left pr-4 pl-4">
                    @csrf
                    <div class="form-group">
                        <label class="text-caption-bold">Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="saya@gmail.com">
                    </div>
                    <div class="form-group mb-4">
                        <label class="text-caption-bold">Password</label>
                        <input type="password" class="form-control" id="password" name="password"
                               placeholder="Password">
                        <div class="text-right">
                            <small class="text-primary text-message">Lupa Password?</small>
                        </div>
                    </div>
                    <div class="form-action">
                        <button class="btn btn-primary mt-4 mb-4">Login</button>
                        <button class="btn btn-outline-primary mb-4"
                                onclick="window.location.href = '{{ route('register') }}'" type="button">Register
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
