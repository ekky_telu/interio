@extends('layouts.app')

@section('content')
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-9 headline-box bg-primary">
                <div class="row">
                    <div class="text-headline text-white col-sm-12">
                        Konsultasi,<br>Wujudkan Ruangan Impianmu!
                    </div>
                </div>
                <div class="row">
                    <div class="text-body text-white col-sm-7">
                        Bersama interio.id kamu dapat membuat ruangan sesuai dengan impianmu dengan mudah dan murah
                    </div>
                </div>
            </div>
            <div class="col-sm-3 bg-white text-center" style="height: 643px;">
                <div class="user-has-joined-box">
                    <div class="user-has-joined-info">
                        <div class="col-sm-12">
                            <div class="text-yellow text-headline">
                                {{ $user }}
                            </div>
                        </div>
                        <div class="col-sm-12 p-0">
                            <div class="text-body">
                                Total Pengguna di Indonesia
                            </div>
                        </div>
                    </div>
                    <div class="user-has-joined-info">
                        <div class="col-sm-12">
                            <div class="text-yellow text-headline">
                                {{ $designer }}
                            </div>
                        </div>
                        <div class="col-sm-12 p-0">
                            <div class="text-body">
                                Total Desainer
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 text-center mt-5">
        <div class="text-title">
            Mengenal interio.id
        </div>
        <div class="text-body mt-3">
            <span class="text-body-bold">interio</span> adalah platfom yang dirancang khusus untuk pemilik bangunan
            dalam menyelesaikan kesulitan dalam mendapatkan desainer interior berkualitas yang sesuai budget dan
            kebutuhan.
        </div>
    </div>
    <div class="col-sm-12 text-left mt-5 p-5">
        <div class="text-title">
            Apa yang kami punya?
        </div>
        <div class="text-body mt-3">
            <div class="row">
                <div class="col-sm-6 text-center">
                    <img src="{{ asset('images/base/doctors.png') }}">
                </div>
                <div class="col-sm-6 text-justify">
                    <p class="p-2">
                        <span class="text-body-bold">interio</span> menyediakan layanan konsultasi bagi kamu yang masih
                        belum mengetahui atau belum terbayang
                        akan desain.
                    </p>
                    <p class="p-2">
                        Demi Desain yang Berkualitas, kami memiliki SOP atau Standar Product bagi designer untuk
                        menggunakan
                        beberapa teori heuristik.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 text-left mt-5 p-5">
        <div class="text-body mt-3">
            <div class="row">
                <div class="col-sm-6 text-justify">
                    <p class="p-2">
                        <span class="text-body-bold">interio</span> memberikan layanan bagi pemilik project untuk
                        melakukan monitoring progress yang dilaksakan oleh desainer sebagai solusi yang dikhawatirkan
                        oleh pemilik project terhadap deadline.
                    </p>
                </div>
                <div class="col-sm-6 text-center">
                    <img src="{{ asset('images/base/professor.png') }}">
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 text-left mt-5 p-5">
        <div class="text-body mt-3">
            <div class="row">
                <div class="col-sm-6 text-center">
                    <img src="{{ asset('images/base/scrum-board.png') }}">
                </div>
                <div class="col-sm-6 text-justify">
                    <p class="p-2">
                        Anda memiliki bangunan tapi kesulitan untuk mendapatkan desainer interior yang sesuai dengan
                        keinginan dan selera anda?
                    </p>
                    <p class="p-2">
                        Melalui <span class="text-body-bold">interio</span> pemilik bangunan dapat memilih desainer
                        interior yang sesuai dengan
                        keinginannya melalui fitur katalog yang menampilkan portofolio dari setiap desainer interior
                        hingga karya - karya yang diciptakannya.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 p-0" style="position: absolute">
        <img class="img-background-home" src="{{ asset('images/base/mask-group.png') }}">
    </div>
    <div class="col-sm-12 text-center home-step-box mb-5">
        <div class="home-step-content">
            <div class="row">
                <div class="col-sm-5">
                    <img src="{{ asset('images/base/online-cv.png') }}">
                </div>
                <div class="col-sm-7 mt-3">
                    <div class="row">
                        <div class="text-center text-subheader">
                            Konsultasikan desain ruangan mu melalui kami secara <span class="text-yellow">GRATIS</span>
                            hanya dengan 3 langkah
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-sm-4 text-center">
                            <div class="circle text-primary text-body-bold">
                                1
                            </div>
                            <div class="text-caption">Login / registrasi dengan akun kamu</div>
                        </div>
                        <div class="col-sm-4">
                            <div class="circle text-primary text-body-bold">
                                2
                            </div>
                            <div class="text-caption">Pilih desainer interior pada fitur katalog</div>
                        </div>
                        <div class="col-sm-4">
                            <div class="circle text-primary text-body-bold">
                                3
                            </div>
                            <div class="text-caption">Dapatkan rekomendasi dari expertise gratis</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
