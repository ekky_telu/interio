<div class="footer">
    <div class="col-sm-12">
        <div class="row ml-4 mr-4">
            <div class="col-sm-9">
                <div class="text-white text-headline">interio.id</div>
            </div>
            <div class="col-sm-3">
                <div class="row mt-4">
                    <div class="col-sm-3">
                        <img src="{{ asset('images/base/facebook.png') }}">
                    </div>
                    <div class="col-sm-3">
                        <img src="{{ asset('images/base/twitter.png') }}">
                    </div>
                    <div class="col-sm-3">
                        <img src="{{ asset('images/base/instagram.png') }}">
                    </div>
                    <div class="col-sm-3">
                        <img src="{{ asset('images/base/youtube.png') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <ul>
                    <li class="text-body-bold text-white ">Tentang Kami</li>
                    <li class="text-message text-white">Apa itu interio.id?</li>
                    <li class="text-message text-white">Cara kerja interio.id</li>
                    <li class="text-message text-white">Karir</li>
                    <li class="text-message text-white">Kontak Kami</li>
                </ul>
            </div>
            <div class="col-sm-4">
                <ul>
                    <li class="text-body-bold text-white ">Tim interio.id</li>
                    <li class="text-message text-white">Rekan Kerjasama</li>
                    <li class="text-message text-white">Tim desainer berkualitas</li>
                    <li class="text-message text-white">Investor</li>
                </ul>
            </div>
            <div class="col-sm-4">
                <ul>
                    <li class="text-body-bold text-white ">Artikel</li>
                    <li class="text-message text-white">Artikel interio.id</li>
                    <li class="text-message text-white">Artikel penting</li>
                </ul>
            </div>
        </div>
    </div>
</div>
