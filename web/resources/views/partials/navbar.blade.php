<nav class="navbar fixed-top navbar-expand-lg navbar-light p-0">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-9 navbar bg-primary">
                <a class="navbar-brand text-white text-title" href="/">interio.id</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#main-navbar" aria-controls="main-navbar"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="main-navbar">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link text-white text-caption-bold" href="#">Beranda</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white text-caption-bold" href="#">Tentang Kami</a>
                        </li>
                        {{--                        <li class="nav-item">--}}
                        {{--                            <a class="nav-link text-white text-caption-bold" href="#">Konsul-in</a>--}}
                        {{--                        </li>--}}
                        {{--                        <li class="nav-item">--}}
                        {{--                            <a class="nav-link text-white text-caption-bold" href="#">Desain-in</a>--}}
                        {{--                        </li>--}}
                    </ul>
                </div>
            </div>
            <div class="col-sm-3 navbar">
                <ul class="navbar-nav">
                    @if(\Auth::check())
                        <li class="nav-item col-sm-7">
                            <a class="nav-auth nav-link text-primary text-caption-bold">Hi, {{ \Illuminate\Support\Facades\Auth::user()->first_name }}</a>
                        </li>
                        <li class="nav-item col-sm-5">
                            <a class="nav-auth nav-link text-primary text-caption-bold" href="{{route('logout')}}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                        </li>
                    @else
                        <li class="nav-item col-sm-6">
                            <a class="nav-auth nav-link text-primary text-caption-bold"
                               href="{{ route('login') }}">Login</a>
                        </li>
                        <li class="nav-item col-sm-6">
                            <a class="nav-auth nav-link text-primary text-caption-bold" href="{{ route('register') }}">Register</a>
                        </li>
                    @endif
                </ul>
            </div>
            <form id="logout-form" action="{{route('logout')}}" method="POST" style="display:none;">
                @csrf
            </form>
        </div>
    </div>
</nav>
