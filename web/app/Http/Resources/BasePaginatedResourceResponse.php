<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\PaginatedResourceResponse;
use Illuminate\Support\Arr;

class BasePaginatedResourceResponse extends PaginatedResourceResponse
{
    /**
     * Pagination Links
     *
     * @param array $paginated
     * @return array
     */
    protected function paginationLinks($paginated)
    {
        return [
            'first' => $paginated['first_page_url'] ?? null,
            'last' => $paginated['last_page_url'] ?? null,
            'prev' => $paginated['prev_page_url'] ?? null,
            'next' => $paginated['next_page_url'] ?? null,
        ];
    }

    /**
     * Meta Pagination
     *
     * @param array $paginated
     * @return array
     */
    protected function meta($paginated)
    {
        $paginated['to'] = $paginated['current_page'] != $paginated['last_page'] ? ($paginated['current_page']+1) : null;
        $meta = ['pagination' => Arr::except($paginated, [
            'data',
            'last_page_url',
            'prev_page_url',
            'next_page_url',
        ])];

        return $meta;
    }
}
