<?php

namespace App\Http\Resources\User;

use App\Http\Resources\BaseResource;
use App\Http\Resources\Gallery\GalleryResource;
use App\Http\Resources\MasterCodeResource;
use App\Http\Resources\Project\ProjectResource;

class DesignerResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['id'],
            'username' => $this['username'],
            'email' => $this['email'],
            'first_name' => $this['first_name'],
            'last_name' => $this['last_name'],
            'phone' => $this['phone'],
            'province' => $this['province'],
            'city' => $this['city'],
            'address' => $this['address'],
            'price' => $this['price'],
            'main_design_style' => $this['main_design_style'],
            'main_design_style_detail' => MasterCodeResource::make($this['main_style']),
            'projects_count' => $this['projects']->count() ?? 0,
            'projects' => ProjectResource::collection($this->whenLoaded('projects')),
            'galleries_count' => $this['galleries']->count() ?? 0,
            'galleries' => GalleryResource::collection($this->whenLoaded('galleries')),
            'design_style' => DesignerStyleResource::collection($this['styles'])
        ];
    }
}
