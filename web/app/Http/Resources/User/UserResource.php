<?php

namespace App\Http\Resources\User;

use App\Http\Resources\BaseResource;

class UserResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['id'],
            'username' => $this['username'],
            'email' => $this['email'],
            'first_name' => $this['first_name'],
            'last_name' => $this['last_name'],
            'phone' => $this['phone'],
            'province' => $this['province'],
            'city' => $this['city'],
            'address' => $this['address']
        ];
    }
}
