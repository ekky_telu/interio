<?php

namespace App\Http\Resources\User;

use App\Http\Resources\MasterCodeResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DesignerStyleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['id'],
            'designer_id' => $this['designer_id'],
            'design_style' => $this['design_style'],
            'design_style_detail' => MasterCodeResource::make($this['detail'])
        ];
    }
}
