<?php

namespace App\Http\Resources\Transaction;

use App\Http\Resources\BaseResource;
use App\Http\Resources\MasterCodeResource;
use App\Http\Resources\User\DesignerResource;
use App\Http\Resources\User\UserResource;

class TransactionResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['id'],
            'user_id' => $this['user_id'],
            'user' => UserResource::make($this['user']),
            'designer_id' => $this['designer_id'],
            'designer' => DesignerResource::make($this['designer']),
            'description' => $this['description'],
            'location' => $this['location'],
            'estimated_duration' => $this['estimated_duration'],
            'duration' => $this['duration'],
            'total_room' => $this['total_room'],
            'total_size' => $this['total_size'],
            'total_price' => $this['total_price'],
            'status' => $this['status'],
            'status_detail' => MasterCodeResource::make($this['status_detail']),
            'payment_method' => $this['payment_method'],
            'payment_account_no' => $this['payment_account_no'],
            'payment_account_name' => $this['payment_account_name'],
            'payment_amount' => $this['payment_amount'],
            'payment_status' => $this['payment_status'],
            'progress_count' => $this['progress_count'],
            'detail' => TransactionDetailResource::collection($this['detail'])
        ];
    }
}
