<?php

namespace App\Http\Resources\Transaction;

use App\Http\Resources\BaseResource;
use App\Http\Resources\MasterCodeResource;

class TransactionDetailResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['id'],
            'transaction_id' => $this['transaction_id'],
            'property_type' => $this['property_type'],
            'property_type_detail' => MasterCodeResource::make($this['property_type_detail']),
            'room_length' => $this['room_length'],
            'room_width' => $this['room_width'],
            'room_height' => $this['room_height'],
            'design_style' => $this['design_style'],
            'design_style_detail' => MasterCodeResource::make($this['design_style_detail']),
            'status' => $this['status'],
            'status_detail' => MasterCodeResource::make($this['status_detail']),
            'result_description' => $this['result_description'],
            'result_photo' => TransactionResultPhotoResource::collection($this['result_photo'])
        ];
    }
}
