<?php

namespace App\Http\Resources\Transaction;

use App\Http\Resources\BaseResource;

class TransactionResultPhotoResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'transaction_detail_id' => $this['transaction_detail_id'],
            'title' => $this['title'],
            'image' => $this['image'],
            'description' => $this['description']
        ];
    }
}
