<?php

namespace App\Http\Resources\Project;

use App\Http\Resources\BaseResource;
use App\Http\Resources\MasterCodeResource;
use App\Http\Resources\User\DesignerResource;

class ProjectResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['id'],
            'designer_id' => $this['designer_id'],
            'designer' => DesignerResource::make($this->whenLoaded('designer')),
            'title' => $this['title'],
            'location' => $this['location'],
            'property_type' => $this['property_type'],
            'property_type_detail' => MasterCodeResource::make($this['property_type_detail']),
            'room_length' => $this['room_length'],
            'room_width' => $this['room_width'],
            'room_height' => $this['room_height'],
            'design_style' => $this['design_style'],
            'design_style_detail' => MasterCodeResource::make($this['design_style_detail']),
            'duration' => $this['duration'],
            'description' => $this['description'],
            'photos' => ProjectPhotoResource::collection($this['photos'])
        ];
    }
}
