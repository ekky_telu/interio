<?php

namespace App\Http\Resources\Project;

use App\Http\Resources\BaseResource;

class ProjectPhotoResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['id'],
            'project_id' => $this['project_id'],
            'title' => $this['title'],
            'image' => $this['image'],
            'description' => $this['description'],
            'main_image' => $this['main_image']
        ];
    }
}
