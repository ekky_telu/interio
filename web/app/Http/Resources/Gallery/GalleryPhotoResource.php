<?php

namespace App\Http\Resources\Gallery;

use Illuminate\Http\Resources\Json\JsonResource;

class GalleryPhotoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['id'],
            'gallery_id' => $this['gallery_id'],
            'title' => $this['title'],
            'image' => $this['image'],
            'description' => $this['description'],
            'main_image' => $this['main_image'],
        ];
    }
}
