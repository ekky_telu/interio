<?php

namespace App\Http\Controllers\Web;

use App\Models\Designer;
use App\Models\User;
use App\Http\Controllers\Controller;

class LandingPageController extends Controller
{
    public function index()
    {
        $user = (User::count() + Designer::count());
        $designer = Designer::count();
        return view('home', compact('user', 'designer'));
    }
}
