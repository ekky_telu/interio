<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ErrorResponse;
use App\Http\Requests\CreateTransactionRequest;
use App\Http\Requests\SendDesignRequest;
use App\Http\Requests\UpdatePaymentTransactionRequest;
use App\Http\Resources\BaseResponse;
use App\Http\Resources\Transaction\TransactionResource;
use App\Models\Designer;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use App\Models\TransactionResultPhoto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = ($request->input('limit') == null) ? 10 : $request->input('limit');
        $data = Transaction::when($keyword = $request->get('user_id'), function ($query) use ($keyword) {
            $query->where('user_id', $keyword);
        })->when($keyword = $request->get('designer_id'), function ($query) use ($keyword) {
            $query->where('designer_id', $keyword);
        })->paginate($limit);

        return TransactionResource::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTransactionRequest $request)
    {
        $validated = $request->validated();

        DB::beginTransaction();
        try {
            $validated['user_id'] = $request->user()->id;
            $validated['status'] = 'STSUP';
            $transaction = Transaction::create($validated);

            $validated['transaction_id'] = $transaction->id;
            TransactionDetail::create($validated);

            $validated['total_room'] = 1;
            $validated['total_size'] = $validated['room_length'] * $validated['room_width'] * $validated['room_height'];
            $validated['total_price'] = Designer::where('id', $validated['designer_id'])->first()->price * $validated['total_size'];
            $validated['estimated_duration'] = '14';
            $transaction->update($validated);

            DB::commit();
            return (new BaseResponse())->setMessage('Berhasil Menyimpan Pesanan')->setData(TransactionResource::make($transaction))->build();
        } catch (\Exception $e) {
            DB::rollBack();

            dd($e);

            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Transaction $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        return TransactionResource::make($transaction);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Transaction $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Transaction $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }

    public function update_payment(UpdatePaymentTransactionRequest $request, Transaction $transaction)
    {
        $validated = $request->validated();
        $validated['payment_status'] = 'PYTDN';

        $result = $transaction->update($validated);
        if ($result) {
            return (new BaseResponse())->setMessage('Berhasil Melakukan Pembayaran')->build();
        } else {
            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }

    public function accept_transaction(Request $request, Transaction $transaction)
    {
        $validated['status'] = 'STSOP';

        $result = $transaction->update($validated);
        $transaction->detail()->update($validated);
        if ($result) {
            return (new BaseResponse())->setMessage('Berhasil Menerima Pesanan')->build();
        } else {
            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }

    public function cancel_transaction(Request $request, Transaction $transaction)
    {
        $validated['status'] = 'STSCL';

        $result = $transaction->update($validated);
        $transaction->detail()->update($validated);
        if ($result) {
            return (new BaseResponse())->setMessage('Berhasil Membatalkan Pesanan')->build();
        } else {
            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }

    public function complete_transaction(Request $request, Transaction $transaction)
    {
        $validated['status'] = 'STSDN';

        $result = $transaction->update($validated);
        $transaction->detail()->update($validated);
        if ($result) {
            return (new BaseResponse())->setMessage('Berhasil Menyelesaikan Pesanan')->build();
        } else {
            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }

    public function send_design(SendDesignRequest $request)
    {
        $validated = $request->validated();
        $validated['status'] = 'STSDL';

        DB::beginTransaction();

        try {
            $transaction = Transaction::find($validated['transaction_id']);
            $transaction_detail = TransactionDetail::where('transaction_id', $validated['transaction_id'])->first();

            $transaction->update($validated);
            $transaction_detail->update($validated);

            $validated['transaction_detail_id'] = $transaction_detail->id;
            foreach ($validated['photos'] as $photo) {
                $validated['image'] = rand() . $photo->getClientOriginalName();
                $photo->move(public_path('images/result/'),  $validated['image']);
                TransactionResultPhoto::create($validated);
            }

            DB::commit();

            return (new BaseResponse())->setMessage('Berhasil Mengirim Desain')->build();
        } catch (\Exception $exception) {
            DB::rollBack();

            dd($exception);

            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }
}
