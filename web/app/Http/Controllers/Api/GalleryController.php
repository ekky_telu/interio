<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ErrorResponse;
use App\Http\Requests\GalleryRequest;
use App\Http\Resources\BaseResponse;
use App\Http\Resources\Gallery\GalleryResource;
use App\Models\Gallery;
use App\Models\GalleryPhotos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = ($request->input('limit') == null) ? 10 : $request->input('limit');
        $data = Gallery::with('designer', 'photos')
            ->when($keyword = $request->get('designer_id'), function ($query) use ($keyword) {
                $query->where('designer_id', $keyword);
            })
            ->orderByDesc('created_at')
            ->paginate($limit);

        return GalleryResource::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryRequest $request)
    {
        $validated = $request->validated();

        DB::beginTransaction();
        try {
            $gallery = Gallery::create($validated);
            foreach ($validated['photos'] as $photo) {
                $image_name = rand() . $photo->getClientOriginalName();
                $photo->move(public_path('images/galleries/'), $image_name);
                GalleryPhotos::create(['gallery_id' => $gallery->id, 'image' => $image_name]);
            }

            DB::commit();

            return (new BaseResponse())->setMessage('Berhasil Menambahkan Foto')->setData(GalleryResource::make($gallery))->build();
        } catch (\Exception $exception) {
            DB::rollBack();

            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        $data = $gallery->load('designer', 'photos');
        return GalleryResource::make($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(GalleryRequest $request, Gallery $gallery)
    {
        $validated = $request->validated();

        DB::beginTransaction();
        try {
            $gallery->update($validated);
            DB::commit();

            return (new BaseResponse())->setMessage('Berhasil Mengubah Foto')->setData(GalleryResource::make($gallery))->build();
        } catch (\Exception $exception) {
            DB::rollBack();

            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery)
    {
        $result = $gallery->delete();
        if ($result) {
            return (new BaseResponse())->setMessage('Berhasil Menghapus Galeri')->build();
        } else {
            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }
}
