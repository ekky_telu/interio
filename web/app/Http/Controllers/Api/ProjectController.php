<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ErrorResponse;
use App\Http\Requests\ProjectRequest;
use App\Http\Resources\BaseResponse;
use App\Http\Resources\Project\ProjectResource;
use App\Models\Project;
use App\Models\ProjectPhoto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = ($request->input('limit') == null) ? 10 : $request->input('limit');
        $data = Project::with('designer', 'photos')
            ->when($keyword = $request->get('designer_id'), function ($query) use ($keyword) {
                $query->where('designer_id', $keyword);
            })
            ->orderByDesc('created_at')
            ->paginate($limit);

        return ProjectResource::collection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {
        $validated = $request->validated();

        DB::beginTransaction();
        try {
            $project = Project::create($validated);
            foreach ($validated['photos'] as $photo) {
                $image_name = rand() . $photo->getClientOriginalName();
                $photo->move(public_path('images/projects/'), $image_name);
                ProjectPhoto::create(['project_id' => $project->id, 'image' => $image_name]);
            }

            DB::commit();

            return (new BaseResponse())->setMessage('Berhasil Menambahkan Proyek')->setData(ProjectResource::make($project->load('designer', 'photos')))->setStatus(Response::HTTP_CREATED)->build();
        } catch (\Exception $exception) {
            DB::rollBack();

            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $data = $project->load('designer', 'photos');
        return ProjectResource::make($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectRequest $request, Project $project)
    {
        $validated = $request->validated();

        DB::beginTransaction();
        try {
            $project->update($validated);
            DB::commit();

            return (new BaseResponse())->setMessage('Berhasil Mengubah Proyek')->setData(ProjectResource::make($project->load('designer', 'photos')))->setStatus(Response::HTTP_CREATED)->build();
        } catch (\Exception $exception) {
            DB::rollBack();

            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Project $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $result = $project->delete();
        if ($result) {
            return (new BaseResponse())->setMessage('Berhasil Menghapus Project')->build();
        } else {
            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }
}
