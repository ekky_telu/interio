<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ErrorResponse;
use App\Http\Requests\DesignStyleRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\PriceRequest;
use App\Http\Requests\UserRequest;
use App\Http\Resources\BaseResponse;
use App\Http\Resources\User\DesignerResource;
use App\Http\Resources\User\UserResource;
use App\Models\Designer;
use App\Models\DesignerStyle;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $validated = $request->validated();

        if ($validated['role'] == 'client') {
            $user = User::where('email', $validated['username'])->orWhere('username', $validated['username'])->first();
        } else {
            $user = Designer::where('email', $validated['username'])->orWhere('username', $validated['username'])->first();
        }

        if ($user) {
            if (Hash::check($validated['password'], $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;

                $data = [
                    'access_token' => $token,
                    'token_type' => 'bearer'
                ];

                return (new BaseResponse())->setMessage('Berhasil Login')->setData($data)->build();
            } else {
                return (new ErrorResponse())->errorResponse("Password Salah", 422);
            }

        } else {
            return (new ErrorResponse())->errorResponse("User Tidak Terdaftar", 422);
        }
    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();

        return (new BaseResponse())->setMessage('Berhasil Logout')->build();
    }

    public function profile(Request $request)
    {
        if ($request->user('designer')) {
            return DesignerResource::make($request->user()->load('projects', 'galleries', 'styles'));
        } else {
            return UserResource::make($request->user());
        }
    }

    public function update(UserRequest $request)
    {
        $validated = $request->validated();
        $result = $request->user()->update($validated);

        if ($result) {
            if ($request->user('designer')) {
                return (new BaseResponse())->setMessage('Berhasil Mengubah Data Diri')->setData(DesignerResource::make($request->user()))->build();
            } else {
                return (new BaseResponse())->setMessage('Berhasil Mengubah Data Diri')->setData(UserResource::make($request->user()))->build();
            }
        } else {
            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }

    public function update_password(PasswordRequest $request)
    {
        $validated = $request->validated();

        if (!Hash::check($validated['old_password'], $request->user()->password)) {
            return (new ErrorResponse())->errorResponse("Password Lama Salah", Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $validated['password'] = bcrypt($validated['password']);
        $result = $request->user()->update($validated);

        if ($result) {
            return (new BaseResponse())->setMessage('Berhasil Mengubah Password')->build();
        } else {
            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }

    public function update_price(PriceRequest $request)
    {
        $validated = $request->validated();
        $result = $request->user()->update($validated);

        if ($result) {
            return (new BaseResponse())->setMessage('Berhasil Mengatur Harga')->setData(DesignerResource::make($request->user()))->build();
        } else {
            return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
        }
    }

    public function update_design_style(DesignStyleRequest $request)
    {
        $validated = $request->validated();
        $current_design_style = $request->user('designer')->styles;
        $current_design_style_arr = [];

        $i = 0;
        foreach ($current_design_style as $curr) {
            $current_design_style_arr[$i] = $curr->design_style;
            if (!in_array($curr->design_style, $validated['design_style'])) {
                $curr->delete();
            }

            $i++;
        }

        foreach ($validated['design_style'] as $design_style) {
            if (!in_array($design_style, $current_design_style_arr)) {
                DesignerStyle::create(['designer_id' => $request->user()->id, 'design_style' => $design_style]);
            }
        }

        $request->user()->update(['main_design_style' => $validated['main_design_style']]);

        return (new BaseResponse())->setMessage('Berhasil Mengatur Gaya Desain')->setData(DesignerResource::make($request->user()))->build();
    }
}
