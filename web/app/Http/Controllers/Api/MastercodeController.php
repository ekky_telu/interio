<?php

namespace App\Http\Controllers\Api;

use App\Models\Mastercode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MastercodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Mastercode::when($keyword = $request->get("parent_code"), function ($query) use ($keyword) {
            $query->where('parent_code', $keyword);
        })->get();

        return response($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Mastercode  $mastercode
     * @return \Illuminate\Http\Response
     */
    public function show(Mastercode $mastercode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Mastercode  $mastercode
     * @return \Illuminate\Http\Response
     */
    public function edit(Mastercode $mastercode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Mastercode  $mastercode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mastercode $mastercode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Mastercode  $mastercode
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mastercode $mastercode)
    {
        //
    }
}
