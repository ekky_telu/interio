<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ErrorResponse;
use App\Http\Requests\UserRequest;
use App\Http\Resources\BaseResponse;
use App\Http\Resources\User\DesignerResource;
use App\Http\Resources\User\UserResource;
use App\Models\Designer;
use App\Models\DesignerStyle;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $validated = $request->validated();

        $validated['password'] = bcrypt($validated['password']);
        if ($validated['role'] == 'client') {
            $result = User::create($validated);

            if ($result) {
                return (new BaseResponse())->setMessage('Berhasil Register')->setData(UserResource::make($result))->build();
            } else {
                return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
            }

        } else {
            DB::beginTransaction();
            try {
                $result = Designer::create($validated);
                if (!empty($validated['design_style'])) {
                    foreach ($validated['design_style'] as $design_style) {
                        DesignerStyle::create(['designer_id' => $result->id, 'design_style' => $design_style]);
                    }

                    $result->update(['main_design_style' => $validated['design_style'][0]]);
                }

                DB::commit();

                return (new BaseResponse())->setMessage('Berhasil Register')->setData(DesignerResource::make($result))->build();
            } catch (\Exception $exception) {
                DB::rollBack();

                return (new ErrorResponse())->errorResponse("Terjadi Kesalahan pada Sistem", 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return UserResource::make($user)->setStatus(Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $result = $user->update();
        if (!$result){
            return response('Gagal Simpan', 500);
        }else{
            return response('Berhasil Simpan', 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function check_username(Request $request)
    {
        if ($request->get('role') == 'designer') {
            if (Designer::where('username', $request->get('username'))->count() > 0) {
                return (new ErrorResponse())->errorResponse("Username Telah Terdaftar", 422);
            } else {
                return (new BaseResponse())->setMessage('Username Belum Terdaftar')->build();
            }
        } else {
            if (User::where('username', $request->get('username'))->count() > 0) {
                return (new ErrorResponse())->errorResponse("Username Telah Terdaftar", 422);
            } else {
                return (new BaseResponse())->setMessage('Username Belum Terdaftar')->build();
            }
        }
    }

    public function check_email(Request $request)
    {
        if ($request->get('role') == 'designer') {
            if (Designer::where('email', $request->get('email'))->count() > 0) {
                return (new ErrorResponse())->errorResponse("Email Telah Terdaftar", 422);
            } else {
                return (new BaseResponse())->setMessage('Email Belum Terdaftar')->build();
            }
        } else {
            if (User::where('email', $request->get('email'))->count() > 0) {
                return (new ErrorResponse())->errorResponse("Email Telah Terdaftar", 422);
            } else {
                return (new BaseResponse())->setMessage('Email Belum Terdaftar')->build();
            }
        }
    }
}
