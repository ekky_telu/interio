<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendDesignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transaction_id' => 'required',
            'result_description' => 'required',
            'photos' => 'required|array|min:1',
            'photos.*' => 'mimes:jpeg,jpg,png'
        ];
    }
}
