<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DesignStyleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'design_style' => 'required|array|min:1',
            'design_style.*' => 'string',
            'main_design_style' => 'required|string'
        ];
    }
}
