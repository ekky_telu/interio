<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required|string',
            'property_type' => 'required|string',
            'room_length' => 'required|integer',
            'room_width' => 'required|integer',
            'room_height' => 'required|integer',
            'design_style' => 'required|string',
            'designer_id' => 'required|string|exists:designers,id',
        ];
    }
}
