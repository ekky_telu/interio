<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('role') == 'designer') {
            if ($this->getMethod() == 'PUT') {
                return [
                    'username' => 'required|string|max:255|exists:designers,username',
                    'email' => 'required|string|email|max:255|exists:designers,email',
                    'first_name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'phone' => 'required|string',
                    'province' => 'nullable|string',
                    'city' => 'nullable|string',
                    'address' => 'nullable|string',
                    'role' => 'required|in:client,designer',
                ];
            } else {
                return [
                    'username' => 'required|string|max:255|unique:designers,username',
                    'email' => 'required|string|email|max:255|unique:designers,email',
                    'password' => 'required|string|confirmed',
                    'password_confirmation' => 'required|string',
                    'first_name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'phone' => 'required|string',
                    'province' => 'nullable|string',
                    'city' => 'nullable|string',
                    'address' => 'nullable|string',
                    'role' => 'required|in:client,designer',
//                    'design_style' => 'array',
                    'design_style.*' => 'string|nullable'
                ];
            }
        } else {
            if ($this->getMethod() == 'PUT') {
                return [
                    'username' => 'required|string|max:255|exists:users,username',
                    'email' => 'required|string|email|max:255|exists:users,email',
                    'first_name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'phone' => 'required|string',
                    'province' => 'nullable|string',
                    'city' => 'nullable|string',
                    'address' => 'nullable|string',
                    'role' => 'required|in:client,designer'
                ];
            } else {
                return [
                    'username' => 'required|string|max:255|unique:users,username',
                    'email' => 'required|string|email|max:255|unique:users,email',
                    'password' => 'required|string|confirmed',
                    'password_confirmation' => 'required|string',
                    'first_name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'phone' => 'required|string',
                    'province' => 'nullable|string',
                    'city' => 'nullable|string',
                    'address' => 'nullable|string',
                    'role' => 'required|in:client,designer'
                ];
            }
        }
    }

    function messages()
    {
        return [
            'username.unique' => 'Username Telah Terdaftar',
            'username.exists' => 'Username Tidak Terdaftar',
            'email.unique' => 'Email Telah Terdaftar',
            'email.exists' => 'Email Tidak Terdaftar',
            'email.email' => 'Format Email Salah',
            'password.confirmed' => 'Password Tidak Sama dengan Konfirmasi Password',
            'required' => 'Harus Diisi'
        ];
    }
}
