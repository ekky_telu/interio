<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->getMethod() == 'PUT') {
            return [
                'designer_id' => 'required|exists:designers,id',
                'title' => 'required|string',
                'location' => 'required|string',
                'property_type' => 'required|string|exists:mastercode,code',
                'room_length' => 'required|integer',
                'room_width' => 'required|integer',
                'room_height' => 'required|integer',
                'design_style' => 'required|string',
                'duration' => 'required|integer',
                'description' => 'nullable|string'
            ];
        } else {
            return [
                'designer_id' => 'required|exists:designers,id',
                'title' => 'required|string',
                'location' => 'required|string',
                'property_type' => 'required|string|exists:mastercode,code',
                'room_length' => 'required|integer',
                'room_width' => 'required|integer',
                'room_height' => 'required|integer',
                'design_style' => 'required|string',
                'duration' => 'required|integer',
                'description' => 'nullable|string',
                'photos' => 'required|array|min:1',
                'photos.*' => 'mimes:jpeg,jpg,png'
            ];
        }
    }
}
