<?php

namespace App\Http\Middleware;

use App\Exceptions\ErrorResponse;
use App\Exceptions\InvalidToken;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            return route('login');
        } else {
            return (new ErrorResponse())->make(new InvalidToken(),
                Response::HTTP_UNAUTHORIZED
            );
        }
    }
}
