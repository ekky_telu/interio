<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use App\Exceptions\ErrorResponse;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
//        if ($request->expectsJson()) {
//            if ($exception instanceof ModelNotFoundException) {
//                return response([
//                    'success' => false,
//                    'message' => 'Data Tidak Ditemukan',
//                    'data' => []
//                ], 404);
//            }
//
//            if ($exception instanceof MethodNotAllowedHttpException) {
//                return response([
//                    'success' => false,
//                    'message' => 'Access Denied or Method Not Allowed',
//                    'data' => []
//                ], 405);
//            }
//        }
//
//        return parent::render($request, $exception);

        $rendered = parent::render($request, $exception);

        return (new ErrorResponse())->make($exception, $rendered->getStatusCode());
    }

//    protected function invalidJson($request, ValidationException $exception)
//    {
//        return response()->json([
//            'success' => false,
//            'message' => $exception->getMessage(),
//            'errors' => $exception->errors()
//        ], $exception->status);
//    }
}
