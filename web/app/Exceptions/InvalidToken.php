<?php

namespace App\Exceptions;

use RuntimeException;

class InvalidToken extends RuntimeException
{
    //
}
