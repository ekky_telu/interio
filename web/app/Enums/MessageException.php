<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self notFound()
 * @method static self unauthorized()
 * @method static self invalidToken()
 * @package App\Enum
 */

final class MessageException extends Enum
{
    const MAP_VALUE = [
        'notFound' => 'Tidak Ditemukan',
        'unauthorized' => 'Unauthorized',
        'invalidToken' => 'Invalid Token'
    ];
}
