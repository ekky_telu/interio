<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use Uuid;

    protected $table = 'projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'designer_id', 'title', 'location', 'property_type', 'room_length', 'room_width', 'room_height', 'design_style', 'duration', 'description'
    ];

    public function designer()
    {
        return $this->belongsTo(Designer::class);
    }

    public function photos()
    {
        return $this->hasMany(ProjectPhoto::class);
    }

    public function property_type_detail()
    {
        return $this->hasOne(Mastercode::class, 'code', 'property_type');
    }

    public function design_style_detail()
    {
        return $this->hasOne(Mastercode::class, 'code', 'design_style');
    }
}
