<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use Uuid;

    protected $table = 'transaction';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'designer_id', 'description', 'location', 'estimated_duration', 'duration', 'total_room', 'total_size', 'total_price', 'status', 'payment_method', 'payment_account_no', 'payment_account_name', 'payment_amount', 'payment_status', 'progress_count'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function designer()
    {
        return $this->belongsTo(Designer::class);
    }

    public function detail()
    {
        return $this->hasMany(TransactionDetail::class);
    }

    public function status_detail()
    {
        return $this->hasOne(Mastercode::class, 'code', 'status');
    }
}
