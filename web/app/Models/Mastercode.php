<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Mastercode extends Model
{
    protected $table = 'mastercode';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_code', 'code', 'description'
    ];

    public function design_styles()
    {
        return $this->hasMany(DesignerStyle::class);
    }
}
