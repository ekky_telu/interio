<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class DesignerStyle extends Model
{
    use Uuid;

    protected $table = 'designer_styles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'designer_id', 'design_style'
    ];

    public function designer()
    {
        return $this->belongsTo(Designer::class);
    }

    public function detail()
    {
        return $this->hasOne(Mastercode::class, 'code', 'design_style');
    }
}
