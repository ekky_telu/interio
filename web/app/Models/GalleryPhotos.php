<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class GalleryPhotos extends Model
{
    use Uuid;

    protected $table = 'gallery_photos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gallery_id', 'title', 'image', 'description', 'main_image'
    ];

    public function gallery()
    {
        return $this->belongsTo(Gallery::class);
    }
}
