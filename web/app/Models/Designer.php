<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Designer extends Authenticatable
{
    use Notifiable, Uuid, HasApiTokens;

    protected $table = 'designers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'first_name', 'last_name', 'email', 'password', 'phone', 'province', 'city', 'address', 'price', 'status', 'main_design_style'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_verified_at'
    ];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function galleries()
    {
        return $this->hasMany(Gallery::class);
    }

    public function styles()
    {
        return $this->hasMany(DesignerStyle::class);
    }

    public function main_style()
    {
        return $this->hasOne(Mastercode::class, 'code', 'main_design_style');
    }
}
