<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use Uuid;

    protected $table = 'galleries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'designer_id', 'title', 'property_type', 'room_length', 'room_width', 'room_height', 'design_style', 'description'
    ];

    public function designer()
    {
        return $this->belongsTo(Designer::class);
    }

    public function photos()
    {
        return $this->hasMany(GalleryPhotos::class);
    }

    public function property_type_detail()
    {
        return $this->hasOne(Mastercode::class, 'code', 'property_type');
    }

    public function design_style_detail()
    {
        return $this->hasOne(Mastercode::class, 'code', 'design_style');
    }
}
