<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class ProjectPhoto extends Model
{
    use Uuid;

    protected $table = 'project_photos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id', 'title', 'image', 'description', 'main_image'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
