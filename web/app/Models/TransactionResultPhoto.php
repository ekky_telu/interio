<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class TransactionResultPhoto extends Model
{
    use Uuid;

    protected $table = 'transaction_result_photos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_detail_id', 'title', 'image', 'description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function transaction_detail()
    {
        return $this->belongsTo(TransactionDetail::class);
    }
}
