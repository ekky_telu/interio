<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    use Uuid;

    protected $table = 'transaction_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_id', 'property_type', 'room_length', 'room_width', 'room_height', 'design_style', 'status', 'result_description'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function property_type_detail()
    {
        return $this->hasOne(Mastercode::class, 'code', 'property_type');
    }

    public function design_style_detail()
    {
        return $this->hasOne(Mastercode::class, 'code', 'design_style');
    }

    public function status_detail()
    {
        return $this->hasOne(Mastercode::class, 'code', 'status');
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function result_photo()
    {
        return $this->hasMany(TransactionResultPhoto::class);
    }
}
